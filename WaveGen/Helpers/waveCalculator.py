# Standard imports
from typing import Optional, Union, List, Dict, Tuple
import logging

# 3rd party imports
from AMASignal import Generator, Signal
import numpy as np
import pyqtgraph as QtG
import PyQt5.QtCore as QtC
import PyQt5.QtWidgets as QtW
from PyQt5 import Qt
import matplotlib as mpl
import i18n

# Custom imports
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen import configuration

# Configure the language
i18n.load_path.append("locales")
i18n.set("fallback", "en")
_ = i18n.t


class WaveCalculator:
    upperVoltageCutoff = 5
    lowerVoltageCutoff = -5

    @classmethod
    def calculateWavesAndPlot(
        cls,
        waveConfigs: Union[List[WaveConfigurationTable], Tuple[WaveConfigurationTable]],
        amplitude: float,
        offset: Union[float, Dict[configuration.ADOutputChannels, float]],
        periods: Union[float, Dict[configuration.ADOutputChannels, float]],
        enabled: Union[bool, Dict[configuration.ADOutputChannels, bool]],
        sampleCount: int,
        individualPlots: bool = False,
        masterPlot: bool = True,
        clipVoltage: bool = True,
        plotCanvas: Optional[Union[mpl.figure.Figure, QtG.PlotWidget]] = None,
    ) -> Dict[configuration.ADOutputChannels, Signal]:
        logger = logging.getLogger(cls.__class__.__name__)

        if len(waveConfigs) == 0:
            logger.info("I have no signals to process...")

            if plotCanvas is not None:
                # Clear the plot
                logger.info("Clearing plot")
                WaveCalculator.clearPlot(
                    plotCanvas, channels=list(configuration.ADOutputChannels)
                )

            # Done, nothing to calculate
            return {}

        # Get the smallest frequency
        freq = np.array([wave.frequency for wave in waveConfigs])
        minFreq = np.amin(freq)
        periodList = freq / minFreq * periods

        # Create a master signal
        masterSignals: Dict[configuration.ADOutputChannels, Signal] = {}

        if plotCanvas is not None:
            # Clear the plot
            WaveCalculator.clearPlot(
                plotCanvas=plotCanvas, channels=list(configuration.ADOutputChannels)
            )

        # Check what signal
        for i, wave in enumerate(waveConfigs):
            # get some basics
            waveChannel = wave.channel
            waveEnabled = wave.enabled

            # Check if enabled
            if isinstance(enabled, bool) and not enabled:
                break
            elif isinstance(enabled, dict):
                if not enabled.get(waveChannel, False):
                    continue

            # Check if we should calculate it. If not enabled, check if we have a plotting canvas
            if (
                plotCanvas is not None and not waveEnabled and individualPlots
            ) or waveEnabled:
                # Calculate the wave
                aSignal = WaveCalculator.calculateSignalForWave(
                    wave=wave,
                    sampleCount=sampleCount,
                    periods=periodList[i],
                    voltageClipping=False,
                )

                # Check if enabled
                if waveEnabled:
                    # Add this to the master
                    if waveChannel in masterSignals:
                        masterSignals[waveChannel].signal += aSignal.signal

                    else:
                        masterSignals[waveChannel] = aSignal.copy()

                # Check if we have single plots and a canvas
                if individualPlots and plotCanvas is not None:
                    # Plot it
                    WaveCalculator.plotWaves(
                        signal=aSignal,
                        waveEnabled=waveEnabled,
                        channel=waveChannel,
                        plotCanvas=plotCanvas,
                    )

        if individualPlots:
            return {}

        # loop each master signal
        output = {}
        for channel, signal in masterSignals.items():
            # Check if enabled
            if isinstance(enabled, bool) and not enabled:
                break
            elif isinstance(enabled, dict):
                if not enabled.get(channel, False):
                    continue

            # Apply offset and amplitude
            if isinstance(amplitude, float):
                signal.signal *= amplitude
            else:
                signal.signal *= amplitude.get(channel, 1)

            if isinstance(offset, float):
                signal.signal += offset
            else:
                signal.signal += offset.get(channel, 0)

            if clipVoltage:
                # Clip it
                signal.signal = signal.signal.clip(
                    WaveCalculator.lowerVoltageCutoff, WaveCalculator.upperVoltageCutoff
                )

            # Check if we should plot the master
            if masterPlot and plotCanvas is not None:
                # plot it
                WaveCalculator.plotWaves(
                    signal=signal,
                    waveEnabled=True,
                    channel=channel,
                    plotCanvas=plotCanvas,
                )

            # Prepare the return, it's always the master signal
            output[channel] = signal

        # Return
        return output

    @classmethod
    def calculateSignalForWave(
        cls,
        wave: WaveConfigurationTable,
        sampleCount: int,
        periods: float,
        voltageClipping: bool = True,
    ) -> Signal:
        # Get the data
        signalType = wave.waveType
        frequency = wave.frequency
        amplitude = wave.amplitude
        phase = wave.phase
        offset = wave.offset
        dutyCycle = wave.dutyCycle

        # Get the logger
        logger = logging.getLogger(cls.__class__.__name__)

        # Check the signal type
        sig: Optional[Signal] = None
        if signalType == configuration.WaveTypes.Sinus:
            # Sinus
            sig = Generator.Sin(
                f=frequency,
                amplitude=amplitude,
                phase_deg=phase,
                offset=offset,
                n=periods,
                n_sp=sampleCount,
            ).signal

        elif signalType == configuration.WaveTypes.Cosine:
            # Cosine
            sig = Generator.Cos(
                f=frequency,
                amplitude=amplitude,
                phase_deg=phase,
                offset=offset,
                n=periods,
                n_sp=sampleCount,
            ).signal

        elif signalType == configuration.WaveTypes.Triangle:
            # Triangle
            sig = Generator.Triangle(
                f=frequency,
                amplitude=amplitude,
                phase_deg=phase,
                offset=offset,
                n=periods,
                n_sp=sampleCount,
            ).signal

        elif signalType == configuration.WaveTypes.Sawtooth:
            # Sawtooth
            sig = Generator.Sawtooth(
                f=frequency,
                amplitude=amplitude,
                phase_deg=phase,
                offset=offset,
                n=periods,
                n_sp=sampleCount,
            ).signal

        elif signalType == configuration.WaveTypes.Square:
            # Square
            sig = Generator.Square(
                f=frequency,
                amplitude=amplitude,
                phase_deg=phase,
                offset=offset,
                n=periods,
                n_sp=sampleCount,
            ).signal

        elif signalType == configuration.WaveTypes.PWM:
            # PWM
            sig = Generator.PWM(
                f=frequency,
                amplitude=amplitude,
                phase_deg=phase,
                offset=offset,
                dutyCycle=dutyCycle / 100,
                n=periods,
                n_sp=sampleCount,
            ).signal

        elif signalType == configuration.WaveTypes.DC:
            # DC
            sig = Generator.DC(
                f=frequency, phase_deg=phase, offset=offset, n=periods, n_sp=sampleCount
            ).signal

        else:
            # Invalid signal
            logger.critical(f"{signalType} is not implemented!")
            raise Exception(f"{signalType} is not implemented!")

        if voltageClipping:
            # Clip the signal
            sig.signal = sig.signal.clip(
                WaveCalculator.lowerVoltageCutoff, WaveCalculator.upperVoltageCutoff
            )

        return sig

    @staticmethod
    def clearPlot(
        plotCanvas: Union[mpl.figure.Figure, QtG.PlotWidget],
        channels: Optional[configuration.ADOutputChannels] = None,
    ):
        if isinstance(plotCanvas, mpl.figure.Figure):
            plotCanvas.axes.clear()

        elif isinstance(plotCanvas, QtG.PlotWidget):
            plotCanvas.clear()
            plotCanvas.enableAutoRange()

            # Add a legend
            legend = plotCanvas.addLegend()

            # Bug fix for pyqtgraph: We need to manually add the legend to the item list...
            # Only took me 2h to find this
            plotCanvas.plotItem.items.append(legend)

            plotCanvas.showGrid(x=True, y=True, alpha=0.7)

    @staticmethod
    def plotWaves(
        signal: Signal,
        waveEnabled: bool,
        channel: configuration.ADOutputChannels,
        plotCanvas: Union[mpl.figure.Figure, QtG.PlotWidget],
    ):
        # Get the colour
        colour = list(configuration.ChannelPlotColours)[channel.value].value

        if isinstance(plotCanvas, mpl.figure.Figure):
            # Get the state
            dashes = "-" if waveEnabled else "--"

            # Labels
            plotCanvas.axes.set_ylabel(_("plotting.voltage_axis"))
            plotCanvas.axes.set_xlabel(_("plotting.time_axis"))

            # Plot it
            plotCanvas.axes.plot(
                signal.time, signal.signal, dashes, color=colour, label=channel.name
            )
            plotCanvas.fig.tight_layout()
            plotCanvas.axes.legend()
            plotCanvas.draw_idle()

        else:
            # Get the state
            dashes = QtC.Qt.SolidLine if waveEnabled else QtC.Qt.DashLine

            # Plot
            plotCanvas.setLabel("left", _("plotting.voltage_axis"))
            plotCanvas.setLabel("bottom", _("plotting.time_axis"))
            plotCanvas.plot(
                signal.time,
                signal.signal,
                pen=QtG.mkPen(Qt.QColor(colour), style=dashes),
                name=channel.name,
            )
            plotCanvas.replot()
