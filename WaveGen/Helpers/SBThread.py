# Standard imports
from typing import Optional, List, Dict, Callable
import time

# 3rd party
import PyQt5.QtCore as QtC
import PyQt5.QtWidgets as QtW


class SBThread(QtC.QObject):
    class NothingReturned:
        pass

    # signals
    runSignal = QtC.pyqtSignal(object, object, object, str, bool)
    completedSignal = QtC.pyqtSignal(str, object)

    # Other
    __self = None

    def __init__(self):
        if SBThread.__self is None:
            # Init the object for the signals
            super().__init__()

            # Setup the singleton
            SBThread.__self = self
            SBThread.__self.tasks = {}
            SBThread.__self.currentTaskID = 0
            SBThread.__self.runSignal.connect(self.__targetRunner)

    @staticmethod
    def runOnMainThread(
        target: Callable,
        args: Optional[List[any]] = None,
        blocking=False,
        kwargs: Optional[Dict[str, any]] = None,
    ) -> Optional[any]:
        if SBThread.__self is None:
            raise Exception("SBThread has not been initialized!")

        # Get a new task ID
        taskID = str(SBThread.__self.currentTaskID)
        SBThread.__self.currentTaskID += 1

        if blocking:
            # Set it as an empty response
            SBThread.__self.tasks[taskID] = SBThread.NothingReturned

        if args is None:
            args = tuple()

        if kwargs is None:
            kwargs = {}

        # send out a signal
        SBThread.__self.runSignal.emit(target, args, kwargs, taskID, blocking)

        # Check if blocking and return the signal
        if blocking:
            # Let the app process the events
            QtW.QApplication.processEvents()

            # Loop
            while True:
                if SBThread.__self.tasks[taskID] != SBThread.NothingReturned:
                    # Get the values back
                    val = SBThread.__self.tasks[taskID]
                    del SBThread.__self.tasks[taskID]
                    return val

                else:
                    # Wait
                    time.sleep(0.0001)

    def __targetRunner(
        self,
        target: Callable,
        args: List[any],
        kwargs: Dict[str, any],
        taskID,
        sendCallback: bool,
    ):
        # run the target method, we are now on the main thread
        # Try and get the result
        result = target(*args, **kwargs)

        # Check if we want a callback
        if sendCallback:
            # Prevent an infinite loop
            if result == SBThread.NothingReturned:
                result = None

            # set & emit
            self.tasks[taskID] = result
            self.completedSignal.emit(taskID, result)
