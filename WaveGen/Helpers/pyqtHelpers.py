# Native imports
from typing import Optional
import math

# 3rd Party imports
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import numpy as np

# Custom imports
from WaveGen.Helpers.translation import _

class QHLine(QtW.QFrame):
    def __init__(self, colour: Optional[str] = None):
        super(QHLine, self).__init__()
        self.setFrameShape(QtW.QFrame.HLine)

        if not colour:
            self.setFrameShadow(QtW.QFrame.Sunken)

        else:
            self.setStyleSheet(f"background-color: {colour};")
            self.setStyleSheet(f"color: {colour};")


class QVLine(QtW.QFrame):
    def __init__(self, colour: Optional[str] = None):
        super(QVLine, self).__init__()
        self.setFrameShape(QtW.QFrame.VLine)

        if not colour:
            self.setFrameShadow(QtW.QFrame.Sunken)

        else:
            self.setStyleSheet(f"background-color: {colour};")
            self.setStyleSheet(f"color: {colour};")


class QSpacer(QtW.QWidget):
    def __init__(self, width: Optional[int] = None, height: Optional[int] = None):
        super().__init__()

        self.setSizePolicy(Qt.QSizePolicy.Expanding, Qt.QSizePolicy.Preferred)

        if width is not None:
            self.setFixedWidth(width)

        if height is not None:
            self.setFixedHeight(height)


class QBaseNSpinBox(QtW.QSpinBox):
    def __init__(self, base: int, parent=None):
        super().__init__(parent=parent)

        self.base = base
        self._oldValue = None
        self.userEditing = False
        self._changedValue = False

        # Disable most events
        self.setKeyboardTracking(False)

        # Listen for changes
        self.valueChanged.connect(self._valueChanged)
        self.editingFinished.connect(self._editingFinished)

    def _editingFinished(self):
        self.calcNewValue(self.value(), True)

    def setValue(self, value: int):
        super().setValue(value)

        if self._oldValue is None:
            self._oldValue = value

    def _valueChanged(self, i: int):
        self.calcNewValue(i)

    def calcNewValue(self, i: int, nearest=False):
        if self._changedValue:
            self._changedValue = False
            return

        # Get the exponent
        exp = math.log(i, self.base)

        # check if the exponent is an integer and we did a single step
        newValue = i
        if nearest:
            # Adjust
            self._changedValue = True

            # Calculate the new exponent
            newValue = self.base ** round(exp)
            self.setValue(newValue)
        if (
            not np.equal(np.mod(exp, 1), 0)
            and abs(self._oldValue - i) == self.singleStep()
        ):
            # Adjust
            self._changedValue = True

            # Calculate the new exponent
            newExp = math.ceil(exp) if self._oldValue < i else math.floor(exp)
            newValue = self.base ** newExp
            self.setValue(newValue)

        self._oldValue = newValue


class QTitleLabel(QtW.QLabel):
    def __init__(self, title, colour: Optional[str] = None, parent=None):
        super().__init__(title, parent)

        # Set it bold
        self.setStyleSheet("font-weight: bold;")

        if colour:
            self.setStyleSheet(f"color: {colour};")

        # disable vertical expanding
        self.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Fixed)


class QFormLayoutRight(QtW.QFormLayout):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setLabelAlignment(QtC.Qt.AlignRight)


class QLabelItalic(QtW.QLabel):
    def __init__(self, text: str, parent=None) -> None:
        super().__init__(text=text, parent=parent)

        self.setStyleSheet("font-style: italic;")


class QAspectRatioWidget(QtW.QWidget):
    """
    https://stackoverflow.com/a/49851646/557087
    """

    def __init__(self, widget, parent=None):
        super().__init__(parent)
        self.aspect_ratio = widget.size().width() / widget.size().height()
        self.setLayout(QtW.QBoxLayout(QtW.QBoxLayout.LeftToRight, self))
        #  add spacer, then widget, then spacer
        self.layout().addItem(QtW.QSpacerItem(0, 0))
        self.layout().addWidget(widget)
        self.layout().addItem(QtW.QSpacerItem(0, 0))

    def resizeEvent(self, e):
        w = e.size().width()
        h = e.size().height()

        if w / h > self.aspect_ratio:  # too wide
            self.layout().setDirection(QtW.QBoxLayout.LeftToRight)
            widget_stretch = h * self.aspect_ratio
            outer_stretch = (w - widget_stretch) / 2 + 0.5
        else:  # too tall
            self.layout().setDirection(QtW.QBoxLayout.TopToBottom)
            widget_stretch = w / self.aspect_ratio
            outer_stretch = (h - widget_stretch) / 2 + 0.5

        self.layout().setStretch(0, outer_stretch)
        self.layout().setStretch(1, widget_stretch)
        self.layout().setStretch(2, outer_stretch)

class LoaderWindow(QtW.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setWindowFlags(QtC.Qt.Window | QtC.Qt.CustomizeWindowHint | QtC.Qt.WindowStaysOnTopHint)

        self.setFixedSize(110, 80)

        widget = QtW.QWidget()
        layout = QtW.QVBoxLayout()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        self.loaderLabel = QtW.QLabel()
        self.loaderMovie = Qt.QMovie("img/loader.gif")
        self.loaderLabel.setMovie(self.loaderMovie)
        layout.addWidget(self.loaderLabel)

        exportingLabel = QtW.QLabel(_("mainWindow.exporting"))
        exportingLabel.setAlignment(QtC.Qt.AlignCenter)
        layout.addWidget(exportingLabel)
        
        self.loaderMovie.start()