# 3rd party
import i18n

# load the module
i18n.load_path.append("locales")
i18n.set("fallback", "en")
_ = i18n.t
