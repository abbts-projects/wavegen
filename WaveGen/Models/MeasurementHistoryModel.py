# Standard imports
import logging
from typing import List

# 3rd party imports
import dwf
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta

# Custom imports
from WaveGen.Helpers.translation import _
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.measurementHistoryTable import MeasurementHistoryTable


class MeasurementHistoryModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = logging.getLogger(self.__class__.__name__)
        self.history: List[MeasurementHistoryTable] = []

    def updateHistory(self):
        # Get the session / data
        DBSession = DBConnection.getSession()

        try:
            self.history = (
                DBSession.query(MeasurementHistoryTable)
                .order_by(MeasurementHistoryTable.creationDate)
                .all()
            )

        except Exception as e:
            self.logger.info(f"Failed to get Measurement History Table: {e}")

        DBSession.close()

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        # Get the row
        row = index.row()

        if role == QtC.Qt.DisplayRole:
            return QtC.QVariant(self.history[row].name)

        elif role == QtC.Qt.DecorationRole:
            if row == 0:
                return QtC.QVariant(qta.icon("mdi.texture", options=[{"color": "white"}]))
            else:
                return QtC.QVariant(qta.icon("mdi.history", options=[{"color": "white"}]))

    def rowCount(self, index):
        return len(self.history)
