# Standard imports
import logging

# 3rd party imports
import dwf
import PyQt5.QtCore as QtC
from PyQt5 import Qt

# Custom imports
from WaveGen.Helpers.translation import _


class ADControlTriggerTypeModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = logging.getLogger(self.__class__.__name__)

        # Set the methods
        self.trigTypes = list(dwf.DwfAnalogIn.TRIGTYPE)

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        if role == QtC.Qt.DisplayRole:
            # Get the row
            row = index.row()
            trig = self.trigTypes[row]

            return QtC.QVariant(_(f"ADControl.TriggerType_{trig.name}"))

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.trigTypes)
