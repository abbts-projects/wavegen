# Standard imports
import logging
from typing import Optional, List
from enum import Enum, auto

# 3rd party imports
import PyQt5.QtCore as QtC
from PyQt5 import Qt

# Custom imports
from WaveGen.Helpers.translation import _
from WaveGen import configuration


class FFTWindowModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = logging.getLogger(self.__class__.__name__)

        # Set the methods
        self.windows = list(configuration.Windows)

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        if role == QtC.Qt.DisplayRole:
            # Get the row
            row = index.row()
            window = self.windows[row]

            return QtC.QVariant(_(f"ADControl.FFT_W_{window.name}"))

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.windows)
