# Standard imports
import logging
from typing import Optional, List

# 3rd party imports
import dwf
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta

# Custom imports
from WaveGen.Helpers.translation import _
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable


class WaveConfigurationModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.waves: List[WaveConfigurationTable] = []

        self.measurementID: Optional[int] = None
        self.logger = logging.getLogger(self.__class__.__name__)

    def updateWaves(self):
        # Get the session / data
        DBSession = DBConnection.getSession()

        try:
            self.waves = (
                DBSession.query(WaveConfigurationTable)
                .order_by(WaveConfigurationTable.order)
                .filter_by(measurementID=self.measurementID)
                .all()
            )

        except Exception as e:
            self.logger.info(f"Failed to fetch the wave config table: {e}")

        DBSession.close()

    def setMeasurementID(self, measID: int):
        self.logger.info("Setting new measurement ID")
        self.measurementID = measID

        # Update
        self.updateWaves()

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        # Get the row
        row = index.row()

        if role == QtC.Qt.DisplayRole:
            return QtC.QVariant(self.waves[row].name)

        elif role == QtC.Qt.DecorationRole:
            return QtC.QVariant(qta.icon("mdi.waves", options=[{"color": "white"}]))

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.waves)
