# Standard imports
import logging

# 3rd party imports
import dwf
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta

# Custom imports
from WaveGen.Helpers.translation import _
from WaveGen import configuration


class ADControlRecordingModeModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = logging.getLogger(self.__class__.__name__)

        # Set the methods
        self.recMethods = list(configuration.RecordingMethods)

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        # Get the row
        row = index.row()
        method = self.recMethods[row]

        if role == QtC.Qt.DisplayRole:
            return QtC.QVariant(_(f"ADControl.{method.name}"))

        elif role == QtC.Qt.DecorationRole:
            img = ""

            if method == configuration.RecordingMethods.SingleShot:
                img = "mdi.arrow-right"
            elif method == configuration.RecordingMethods.Continuous:
                img = "mdi.repeat"

            return QtC.QVariant(qta.icon(img, options=[{"color": "white"}]))

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.recMethods)
