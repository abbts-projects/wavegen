# Standard imports
import logging

# 3rd party imports
import dwf
import PyQt5.QtCore as QtC
from PyQt5 import Qt

# Custom imports
from WaveGen.Helpers.translation import _


class ADControlSourceModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = logging.getLogger(self.__class__.__name__)

        # Set the methods
        self.sources = list(dwf.DwfAnalogIn.TRIGSRC)

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        if role == QtC.Qt.DisplayRole:
            # Get the row
            row = index.row()
            source = self.sources[row]

            return QtC.QVariant(_(f"ADControl.TriggerSource_{source.name}"))

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.sources)
