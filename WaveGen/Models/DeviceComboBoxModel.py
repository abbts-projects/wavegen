# Standard imports
import logging
from typing import Optional, List

# 3rd party imports
import PyQt5.QtCore as QtC
from PyQt5 import Qt

# Custom imports
from WaveGen.Helpers.translation import _
from WaveGen import configuration


class DeviceComboBoxModel(QtC.QAbstractListModel):
    def __init__(self, devices: Optional[List[str]] = None):
        super().__init__()
        self.devices = devices or []

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        row = index.row()

        if role == QtC.Qt.DisplayRole:
            if len(self.devices) > row:
                device = self.devices[row]
                return QtC.QVariant(f"{device.serial} ({device.name})")

            else:
                return QtC.QVariant()

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.devices)
