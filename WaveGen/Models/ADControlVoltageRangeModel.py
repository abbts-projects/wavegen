# Standard imports
import logging

# 3rd party imports
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import AnalogWaveforms

# Custom imports
from WaveGen.Helpers.translation import _


class ADControlVoltageRangeModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = logging.getLogger(self.__class__.__name__)

        # Set the methods
        self.voltageRanges = list(AnalogWaveforms.VoltageRange)

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        # Get the row
        row = index.row()
        vRange = self.voltageRanges[row]

        if role == QtC.Qt.DisplayRole:
            return QtC.QVariant(_(f"ADControl.{vRange.name}"))

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.voltageRanges)
