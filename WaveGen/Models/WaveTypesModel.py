# Standard imports
import logging
from typing import Optional, List

# 3rd party imports
import dwf
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta

# Custom imports
from WaveGen.Helpers.translation import _
from WaveGen import configuration


class WaveTypesModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.waveTypes = [(wave, wave.name) for wave in configuration.WaveTypes]

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        if role == QtC.Qt.DisplayRole:
            row = index.row()

            return QtC.QVariant(_(f"configuration.{self.waveTypes[row][1]}"))

        elif role == QtC.Qt.DecorationRole:
            return QtC.QVariant(qta.icon("mdi.waves", options=[{"color": "white"}]))

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.waveTypes)
