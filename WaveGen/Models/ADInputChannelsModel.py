# Standard imports
import logging

# 3rd party imports
import PyQt5.QtCore as QtC
from PyQt5 import Qt

# Custom imports
from WaveGen.Helpers.translation import _
from WaveGen import configuration


class ADInputChannelsModel(QtC.QAbstractListModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = logging.getLogger(self.__class__.__name__)

        # Set the methods
        self.channels = list(configuration.ADInputChannels)

    def data(self, index: Qt.QModelIndex, role: QtC.Qt.ItemDataRole) -> QtC.QVariant:
        if role == QtC.Qt.DisplayRole:
            # Get the row
            row = index.row()
            channel = self.channels[row]

            return QtC.QVariant(channel.name)

    def rowCount(self, index: Qt.QModelIndex):
        return len(self.channels)
