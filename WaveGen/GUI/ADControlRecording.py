# Native imports
from typing import Optional, List, Tuple, Dict, Union
import logging
from enum import Enum, auto
import threading

# 3rd Party
import pyqtgraph as QtG
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import WaveForms, Generator, Signal, AnalogWaveforms
import qtawesome as qta
import dwf
import numpy as np

# Custom
from WaveGen.GUI.wavePlot import WavePlotController
from WaveGen.GUI.ADXYPlot import ADXYPlotController
from WaveGen.GUI.ADFFTPlot import ADFFTPlotController
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.DB.ADConfigurationTable import ADConfigurationTable
from WaveGen.DB.measurementHistoryTable import MeasurementTable
from WaveGen.Helpers.waveCalculator import WaveCalculator
from WaveGen.Helpers.translation import _
from WaveGen.Helpers.SBThread import SBThread
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import configuration
from WaveGen import Models


class ADControlRecordingWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setObjectName(self.__class__.__name__)

        self.layout = QtW.QVBoxLayout()

        # Add a title
        self.layout.addWidget(QtH.QTitleLabel(_("ADControl.Recording"), "#aaa"))

        self.recordingModeComboBox = QtW.QComboBox()
        self.settingsGroupBox = QtW.QGroupBox()

        # Specific settings
        settingsLayout = QtH.QFormLayoutRight()

        self.recordingBaseSpinBox = QtW.QDoubleSpinBox()
        self.recordingBaseSpinBox.setRange(1e-6, 12e6)
        self.recordingBaseSpinBox.setAccelerated(True)
        self.recordingBaseSpinBox.setDecimals(6)
        self.recordingBaseSpinBox.setSuffix(" ms/Div")
        self.recordingBaseSpinBox.setValue(1)

        self.recordingSamplesSpinBox = QtH.QBaseNSpinBox(2)
        self.recordingSamplesSpinBox.setRange(2 ** 5, 2 ** 13)
        self.recordingSamplesSpinBox.setAccelerated(True)
        self.recordingSamplesSpinBox.setValue(self.recordingSamplesSpinBox.maximum())

        self.recordingFrequencySpinBox = QtW.QDoubleSpinBox()
        self.recordingFrequencySpinBox.setRange(50e-3, 100e6)
        self.recordingFrequencySpinBox.setAccelerated(True)
        self.recordingFrequencySpinBox.setDecimals(4)
        self.recordingFrequencySpinBox.setSuffix(" Hz")

        settingsLayout.addRow(
            QtW.QLabel(_("ADControl.Base")), self.recordingBaseSpinBox
        )
        settingsLayout.addRow(
            QtW.QLabel(_("ADControl.Samples")), self.recordingSamplesSpinBox
        )
        settingsLayout.addRow(
            QtW.QLabel(_("ADControl.Frequency")), self.recordingFrequencySpinBox
        )

        # Set a default layout
        self.settingsGroupBox.setLayout(settingsLayout)

        self.layout.addWidget(self.recordingModeComboBox)
        self.layout.addWidget(self.settingsGroupBox)

        self.setLayout(self.layout)


class ADControlRecordingController(QtC.QObject):
    # class properties
    waveforms: Optional[WaveForms] = None
    isConnected: bool = False

    # Signals
    recordingValuesChangedSignal = QtC.pyqtSignal()
    channelValuesChangedSignal = QtC.pyqtSignal()
    plotLiveDataSig = QtC.pyqtSignal(object)

    def __init__(
        self,
        ADControl: "ADControl",
        mainConfig: "MainConfigurationController",
        historyController: "MeasurementHistoryController",
        parent=None,
    ):
        super().__init__(parent)

        # New logger
        self.logger = logging.getLogger(self.__class__.__name__)

        self.DBSession = DBConnection.getSession()

        # keep inputs
        self.ADControl = ADControl
        self.mainConfig = mainConfig
        self.historyController = historyController

        # Other
        self.selfChangeFreqBase = False
        self.measuredSignals: Dict[configuration.ADOutputChannels, Signal] = {}
        self.stopMeasurementEvent = threading.Event()
        self.isRecording = False
        self.currentHistoryID: Optional[int] = None

        # new plot controller
        self.measuredSignalPlotController = WavePlotController(
            _("ADControl.MeasuredSignal")
        )

        # Set the plot labels
        pltWi = self.measuredSignalPlotController.widget.plotWidget
        pltWi.setLabel("left", _("plotting.voltage_axis"))
        pltWi.setLabel("bottom", _("plotting.time_axis"))

        # New instance
        configuration.waveforms = WaveForms()

        # Init the widget
        self.widget = ADControlRecordingWidget()

        # Set the recording modes model
        self.recModeModel = Models.ADControlRecordingModeModel()
        self.widget.recordingModeComboBox.setModel(self.recModeModel)

        # Init the XY View
        self.XYPlotController = ADXYPlotController(self.ADControl)
        ADControl.mainWindow.addDockWidget(
            QtC.Qt.LeftDockWidgetArea, self.XYPlotController.dockableWidget
        )
        self.XYPlotController.dockableWidget.setFloating(True)
        self.hideXYPlot()

        # Init the FFT View
        self.FFTPlotController = ADFFTPlotController(self.ADControl)
        ADControl.mainWindow.addDockWidget(
            QtC.Qt.LeftDockWidgetArea, self.FFTPlotController.dockableWidget
        )
        self.FFTPlotController.dockableWidget.setFloating(True)
        self.hideFFTPlot()

        # Saving for later
        self.recordedSignals: Dict[int, Signal] = {}
        self.adjustedSignals: Dict[int, Signal] = {}

        # Connect
        self.connect()

        # Let's say, the base changed
        self.recordingBaseChanged()

    ## XY Plotting
    def showXYPlot(self):
        self.logger.info("Showing XY Plotter")
        self.XYPlotController.setHidden(False)

        # Plot the data
        if self.recordedSignals:
            self.checkAndPlotXY(newSignal=self.recordedSignals)

    def hideXYPlot(self):
        self.logger.info("Hiding XY Plotter")
        self.XYPlotController.setHidden(True)

    def checkAndPlotXY(self, newSignal: Dict[int, Signal]):
        # check if we have a X-Y plot
        if not self.XYPlotController.hidden:
            # Update the plot
            signals = list(newSignal.values())

            # Check if we have enough signals
            if len(signals) >= 2:
                self.XYPlotController.plotFromSignals(
                    signal1=signals[0].signal,
                    signal2=signals[1].signal,  # TODO: Better way of passing the values
                )

            else:
                self.XYPlotController.clearPlot()

    ## FFT Plotting
    def showFFTPlot(self):
        self.logger.info("Showing FFT Plotter")
        self.FFTPlotController.setHidden(False)

        # Plot the data
        if self.recordedSignals:
            self.checkAndPlotFFT(newSignal=self.recordedSignals)

    def hideFFTPlot(self):
        self.logger.info("Hiding FFT Plotter")
        self.FFTPlotController.setHidden(True)

    def checkAndPlotFFT(self, newSignal: Dict[int, Signal], clearUnused=False):
        # check if we have a FFT plot
        if not self.FFTPlotController.hidden:
            # Update the plot
            signals = list(newSignal.values())
            self.FFTPlotController.plotFromSignals(
                signals=newSignal, clearUnused=clearUnused
            )

    ## DB
    def databaseChanged(self):
        # Close and get a new session
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # update
        self.updateGUIValues()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Update
        self.currentHistoryID = currentID
        measurements = (
            self.DBSession.query(MeasurementTable)
            .filter_by(measurementID=self.currentHistoryID)
            .all()
        )

        # Fetch the measurements
        self.recordedSignals = self.historyController.fetchMeasurements()

        # update
        self.updateGUIValues()
        self.plotMeasurements()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Curves are copied in another place :)

    ## APP
    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        # inform
        self.FFTPlotController.projectAboutToClose()
        self.XYPlotController.projectAboutToClose()

        # Check if we are recording
        if self.isRecording:
            # Stop!
            self.runMeasurements()

        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        # Commit
        self.DBSession.commit()

    ## GUI
    def connect(self):
        # Connect all changes
        self.widget.recordingSamplesSpinBox.valueChanged.connect(
            self.sampleCountChanged
        )
        self.widget.recordingBaseSpinBox.valueChanged.connect(self.recordingBaseChanged)
        self.widget.recordingFrequencySpinBox.valueChanged.connect(
            self.frequencyChanged
        )
        self.widget.recordingModeComboBox.currentIndexChanged.connect(
            self.recModeChanged
        )

        self.plotLiveDataSig.connect(self.syncLivePlot)

    def updateGUIValues(self):
        self.logger.info("Updating GUI values")

        if self.ADControl.currentADConfig is None:
            self.logger.info("We don't have a valid ADConfig!")
            return

        # self.recordingBaseSpinBox.setValue()       # No need to set, as this will be calculated!
        self.widget.recordingSamplesSpinBox.setValue(
            self.ADControl.currentADConfig.recordingSamples
        )
        self.widget.recordingFrequencySpinBox.setValue(
            self.ADControl.currentADConfig.recordingFrequency
        )

        # Combo box update
        self.setRecordingMode(self.ADControl.currentADConfig.recordingMethod)

        # Update the plots
        self.XYPlotController.updateGUIValues()
        self.FFTPlotController.updateGUIValues()

    def setRecordingMode(self, mode: configuration.RecordingMethods):
        self.logger.info("Updating recording mode")

        newIndex = self.recModeModel.recMethods.index(mode)
        self.widget.recordingModeComboBox.setCurrentIndex(newIndex)

    def getRecordingBase(self) -> float:
        return self.widget.recordingBaseSpinBox.value() * 1000

    def setRecordingBase(self, newBase) -> None:
        self.widget.recordingBaseSpinBox.setValue(newBase / 1000)

    def recMode(self) -> configuration.RecordingMethods:
        cIndex = self.widget.recordingModeComboBox.currentIndex()
        return self.recModeModel.recMethods[cIndex]

    def recModeChanged(self):
        self.logger.info("Updating recording mode")

        # Update the model
        if self.ADControl.currentADConfig:
            self.ADControl.currentADConfig.recordingMethod = self.recMode()

        self.recordingValuesChanged()

    def sampleCountChanged(self):
        if not self.selfChangeFreqBase:
            self.logger.info("Sample count changed")

            # Update
            self.updateFrequrencyOrBase(updateFrequency=False)

        else:
            self.selfChangeFreqBase = False

        # Update the model
        if self.ADControl.currentADConfig:
            self.ADControl.currentADConfig.recordingSamples = (
                self.widget.recordingSamplesSpinBox.value()
            )

        # Emit a signal
        self.recordingValuesChanged()

    def recordingBaseChanged(self):
        if not self.selfChangeFreqBase:
            self.logger.info("Recording base changed")

            # Update
            self.updateFrequrencyOrBase(updateFrequency=True)

        else:
            self.selfChangeFreqBase = False

        # Emit a signal
        self.recordingValuesChanged()

    def frequencyChanged(self):
        if not self.selfChangeFreqBase:
            self.logger.info("Frequency changed changed")

            # Update
            self.updateFrequrencyOrBase(updateFrequency=False)

        else:
            self.selfChangeFreqBase = False

        # Update the model
        if self.ADControl.currentADConfig:
            self.ADControl.currentADConfig.recordingFrequency = (
                self.widget.recordingFrequencySpinBox.value()
            )

        # Emit a signal
        self.recordingValuesChanged()

    def updateFrequrencyOrBase(self, updateFrequency):
        self.selfChangeFreqBase = True

        # Update!
        maxFreq = self.widget.recordingFrequencySpinBox.maximum()
        frequency = self.widget.recordingFrequencySpinBox.value()
        baseValue = self.getRecordingBase()
        sampleCount = self.widget.recordingSamplesSpinBox.value()
        sampleCount = int(str(sampleCount)[0]) * 1e8

        if updateFrequency:
            self.widget.recordingFrequencySpinBox.setValue(
                min((1 / baseValue * sampleCount), maxFreq)
            )

        else:
            self.setRecordingBase(frequency / sampleCount * 1000000)

    def recordingValuesChanged(self):
        self.logger.info("recording values changed")
        self.recordingValuesChangedSignal.emit()

    def channelValuesChanged(self):
        self.logger.info("channel values changed")

        # Re plot
        self.plotMeasurements()

        # Signal
        self.channelValuesChangedSignal.emit()

    def calculateSignal(self) -> Dict[configuration.ADOutputChannels, Signal]:
        # Get all waves from the DB
        waves = (
            self.DBSession.query(WaveConfigurationTable)
            .filter_by(measurementID=self.currentHistoryID)
            .all()
        )

        # TODO: Combine this code with the wave configuration!
        # Prepare
        offsets: Dict[configuration.ADOutputChannels, float] = {}
        amplitudes: Dict[configuration.ADOutputChannels, float] = {}
        enabled: Dict[configuration.ADOutputChannels, float] = {}

        mainOffset = self.mainConfig.currentConfiguration.offset
        mainAmplitude = self.mainConfig.currentConfiguration.amplitude

        # Loop each config.
        config = self.ADControl.outputController.fetchCHConfig(
            historyID=self.currentHistoryID
        )

        if config is None:
            self.logger.info("We don't have a channel config!")
            return {}

        for output in config:
            offsets[output.channel] = mainOffset + output.offset
            amplitudes[output.channel] = mainAmplitude * output.amplitude
            enabled[output.channel] = output.enabled

        # re-calculate the wave
        sigs = WaveCalculator.calculateWavesAndPlot(
            waveConfigs=waves,
            amplitude=amplitudes,
            offset=offsets,
            enabled=enabled,
            periods=self.mainConfig.currentConfiguration.periods,
            sampleCount=self.mainConfig.currentConfiguration.sampleCount,
            individualPlots=self.mainConfig.currentConfiguration.individual,
            masterPlot=True,
            clipVoltage=self.mainConfig.currentConfiguration.clipVoltage,
            plotCanvas=None,
        )

        return sigs

    def setRecIcon(self, isRecording: bool):
        self.logger.info("Updating recording icon")
        self.mainConfig.setRecordingIcon(isRecording)

    def runMeasurements(self):
        # Change the recording icon
        self.setRecIcon(isRecording=(not self.isRecording))

        # Check the recording state
        if self.isRecording:
            # Stop the recording!
            if self.stopMeasurementEvent:
                self.stopMeasurementEvent.set()

            # Stop the live ad thread!
            if configuration.waveforms.analog.liveDataStopEvent:
                configuration.waveforms.analog.liveDataStopEvent.set()

                # Save the live data!
                self.historyController.addMeasurementToHistory(
                    signals=self.recordedSignals,
                    currentConfig=self.mainConfig.currentConfiguration,
                )

        else:
            self.logger.info("Running a new measurement")

            # Check if we are connected
            if configuration.waveforms.device is None:
                self.logger.warning("Can't record without a device being connected!")
                return

            # Reset the plot
            self.clearPlottedSignals()

            # Check if single or continuous
            method = self.ADControl.currentADConfig.recordingMethod

            # Reset the flag
            self.stopMeasurementEvent = threading.Event()

            # Update the AD config, start the output
            chConfig = self.updateADConfig()

            # Check
            if method == configuration.RecordingMethods.SingleShot:
                # Single shot, run it in a new thread
                measurementThread = threading.Thread(
                    target=self.singleShotRecording, args=(chConfig,)
                )

                # Start it
                measurementThread.start()

            elif method == configuration.RecordingMethods.Continuous:
                # Continuous recording
                self.continuosRecording(chConfig)

            else:
                self.logger.warning(f"Invalid measurement method selected! {method}")

        # Change the rec state
        self.isRecording = not self.isRecording

    def updateADConfig(self) -> List[AnalogWaveforms.RecordChannelConfig]:
        # Get the signal
        self.calculatedSignal = self.calculateSignal()

        # Write the data
        for ch, sig in self.calculatedSignal.items():
            self.logger.info(f"Starting output on ch: {ch.name}")
            configuration.waveforms.analog.configureOutput(
                channel=ch.value,
                signal=sig,
                amplitude=None,  # Auto detect max amplitude
                offset=None,  # No offset
                start=False,  # Start at a later date
            )

        self.logger.info("Resetting AI/AO")
        # TODO: Enable reset. Causing problems?
        # configuration.waveforms.analog.resetAnalogOut()
        configuration.waveforms.analog.resetAnalogIn()

        # Get the config
        chConfig: List[AnalogWaveforms.RecordChannelConfig] = []
        for chSetup in self.ADControl.channelController.channelControllers.values():
            chConfig.append(
                AnalogWaveforms.RecordChannelConfig(
                    voltageRange=chSetup.voltageRange(),
                    channelID=chSetup.channel.value,
                    enabled=chSetup.widget.enabledCheckBox.isChecked(),
                    offset=0,
                )
            )

        if self.ADControl.currentADConfig.triggerEnable:
            self.logger.info("Setting up triggering")

            # Setup triggering
            configuration.waveforms.analog.setupTrigger(
                channel=self.ADControl.currentADConfig.triggerChannel.value,
                level=self.ADControl.currentADConfig.triggerLevel,
                triggerType=self.ADControl.currentADConfig.triggerType,
                condition=self.ADControl.currentADConfig.triggerCondition,
                source=self.ADControl.currentADConfig.triggerSource,
            )

        else:
            self.logger.info("No trigger has been set up this time")

        # Check if we should sync the output
        if self.ADControl.currentADConfig.outputSynchronizationEnabled:
            self.logger.info("Enabling output sync")

            # Prepare
            masterCH = self.ADControl.currentADConfig.outputSynchronizationMaster.value
            slaveCH = self.ADControl.currentADConfig.outputSynchronizationSlave.value

            # Sync setup
            configuration.waveforms.analog.setupOutputSynchronization(
                masterChannel=masterCH, slaveChannel=slaveCH
            )

            # Enable Output
            configuration.waveforms.analog.startOutput(channel=masterCH)

        else:
            self.logger.info("Individual signal start")

            # Start the output. This will mean, that both signals will start at the "same" time...
            for ch in self.calculatedSignal.keys():
                configuration.waveforms.analog.startOutput(channel=ch.value)

        return chConfig

    def singleShotRecording(self, chConfig: List[AnalogWaveforms.RecordChannelConfig]):
        self.logger.info("Doing a single shot measurement")

        # Let's record some data
        samples = self.widget.recordingSamplesSpinBox.value()
        freq = self.widget.recordingFrequencySpinBox.value()

        # Record
        self.logger.info("Starting the recording")
        self.recordedSignals = configuration.waveforms.analog.record(
            frequency=freq,
            samples=samples,
            channelConfig=chConfig,
            stopEvent=self.stopMeasurementEvent,
        )
        self.logger.info("Finished the recording")

        if self.recordedSignals is None:
            self.logger.warning("No data was recorded!")
            return

        # Reset recording state
        self.isRecording = False
        self.setRecIcon(isRecording=self.isRecording)

        # Adjust
        self.adjustRecordedSignals()

        # Plot it
        SBThread.runOnMainThread(
            target=self.plotMeasurements, kwargs={"reAdjustSignals": False}
        )

        # Repackage
        recordedSignalsAD: Dict[configuration.ADInputChannels, Signal] = {}
        for ch, signal in self.recordedSignals.items():
            recordedSignalsAD[configuration.ADInputChannels(ch)] = signal

        # Stop the output
        for ch in recordedSignalsAD.keys():
            self.logger.info("Stopping output")
            configuration.waveforms.analog.stopOutput(ch.value)

        # Save the measurement
        SBThread.runOnMainThread(
            target=lambda: self.historyController.addMeasurementToHistory(
                signals=recordedSignalsAD,
                currentConfig=self.mainConfig.currentConfiguration,
            )
        )

    def continuosRecording(self, chConfig: List[AnalogWaveforms.RecordChannelConfig]):
        self.logger.info("Continuous recording")

        # Let's record some data
        samples = self.widget.recordingSamplesSpinBox.value()
        freq = self.widget.recordingFrequencySpinBox.value()

        # ToDo: improve! Legend with names
        pltWi = self.measuredSignalPlotController.widget.plotWidget

        # Create a list of curves
        self.curve = [pltWi.getPlotItem().plot() for _ in configuration.ADInputChannels]

        # Set the plot labels
        pltWi.setLabel("left", _("plotting.voltage_axis"))
        pltWi.setLabel("bottom", _("plotting.voltage_axis"))

        # Set the plot colours
        for i, ch in enumerate(configuration.ChannelPlotColours):
            self.curve[i].setPen(QtG.mkPen(Qt.QColor(ch.value)))

        # Record
        self.logger.info("Starting the live view")
        configuration.waveforms.analog.liveData(
            frequency=freq,
            samples=samples,
            channelConfig=chConfig,
            onUpdate=self.liveDataArrived,
        )
        # Now there is a new thread collecting the data!

    def liveDataArrived(self, newSignal: Dict[int, Signal]):
        # self.logger.info("Received live data!")

        # Save
        self.recordedSignals = newSignal

        # Get this back on the main thread
        self.plotLiveDataSig.emit(newSignal)

    def syncLivePlot(
        self, newSignal: Dict[Union[int, configuration.ADInputChannels], Signal]
    ):
        # Plot this data!
        for i, data in newSignal.items():
            if isinstance(i, configuration.ADInputChannels):
                i = i.value

            # Clear it
            if i < len(self.curve):
                self.curve[i].clear()

            # Check if this one is enabled
            if self.ADControl.channelController.enabledForChannel(i):
                # Plot
                self.curve[i].setData(
                    x=data.time,
                    y=data.signal
                    + self.ADControl.channelController.offsetForChannel(i),
                    name=f"CH{i+1}",
                )

        # Check and plot the XY
        self.checkAndPlotXY(newSignal)

        # Check if we should FFT plot
        self.checkAndPlotFFT(newSignal=self.recordedSignals)

    def clearPlottedSignals(self):
        self.logger.info("Clearing plotted signals")

        # Clear
        self.measuredSignalPlotController.widget.plotWidget.clear()

    def adjustRecordedSignals(self):
        newSigs = {}

        if self.recordedSignals is None:
            self.logger.info("No recorded signals to display!")
            self.adjustedSignals = {}
            return

        for ch, sig in self.recordedSignals.items():
            # Check if enabled
            if not self.ADControl.channelController.enabledForChannel(ch):
                continue

            # Prepare
            signal = sig.signal.copy()

            # Add the offset
            signal += self.ADControl.channelController.offsetForChannel(ch)

            # Add it
            newSigs[ch] = Signal(sig.name, sig.time, signal)

        # Set it
        self.adjustedSignals = newSigs

    def plotMeasurements(self, reAdjustSignals=True):
        self.logger.info("Plotting Measurements!")

        if reAdjustSignals:
            self.adjustRecordedSignals()

        if self.isRecording:
            self.logger.info("Not replotting, since we are recording!")
            return

        # Clear the plot first
        self.clearPlottedSignals()

        # plot them
        for ch, sig in self.adjustedSignals.items():
            if not self.ADControl.channelController.enabledForChannel(ch):
                continue

            # Prepare
            signal = sig.signal.copy()

            # Change number to enum
            ch = configuration.ADInputChannels(ch)

            # Find the right colour
            colour = list(configuration.ChannelPlotColours)[ch.value].value

            # Do the plotting
            self.measuredSignalPlotController.widget.plotWidget.plot(
                sig.time,
                signal + self.ADControl.channelController.offsetForChannel(ch),
                pen=QtG.mkPen(Qt.QColor(colour)),
            )

        # Check if we should X-Y plot
        self.checkAndPlotXY(newSignal=self.adjustedSignals)

        # Check if we should FFT plot
        self.checkAndPlotFFT(newSignal=self.adjustedSignals, clearUnused=True)
