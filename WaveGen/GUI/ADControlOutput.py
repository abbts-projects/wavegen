# Native imports
from typing import Optional, List, Tuple, Dict, Union
import logging
from enum import Enum, auto

# 3rd Party
import pyqtgraph as QtG
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import WaveForms, Generator, Signal, AnalogWaveforms
import qtawesome as qta
import dwf

# Custom
from WaveGen.GUI.wavePlot import WavePlotController
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.Helpers.translation import _
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import configuration
from WaveGen.DB.ADConfigurationTable import (
    ADConfigurationTable,
    ADConfigurationOutputChannelTable,
)
from WaveGen import Models


class ADControlOutputWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setObjectName(self.__class__.__name__)

        self.setMinimumWidth(250)

        self.layout = QtW.QVBoxLayout()

        # Add a title
        self.layout.addWidget(QtH.QTitleLabel(_("ADControl.Output"), "#aaa"))

        # Another form for the general setup
        formLayout = QtH.QFormLayoutRight()
        self.layout.addLayout(formLayout)

        # set if the output is in sync
        self.syncEnableCheckBox = QtW.QCheckBox()
        self.syncMasterComboBox = QtW.QComboBox()
        self.syncSlaveComboBox = QtW.QComboBox()

        formLayout.addRow(
            QtW.QLabel(_("ADControl.SyncEnable")), self.syncEnableCheckBox
        )
        formLayout.addRow(
            QtW.QLabel(_("ADControl.SyncMaster")), self.syncMasterComboBox
        )
        formLayout.addRow(QtW.QLabel(_("ADControl.SyncSlave")), self.syncSlaveComboBox)

        # Set the layout
        self.setLayout(self.layout)


class ADControlOutputController(QtC.QObject):
    channelConfigChangedSignal = QtC.pyqtSignal()

    def __init__(self, ADControl: "ADControl", parent=None):
        super().__init__(parent=parent)

        self.logger = logging.getLogger(self.__class__.__name__)

        # keep inputs
        self.ADControl = ADControl

        # Prepare
        self.DBSession = DBConnection.getSession()
        self.currentHistoryID: Optional[int] = None

        # set the widget
        self.widget = ADControlOutputWidget()

        self.channelModel = Models.ADOutputChannelsModel()
        self.widget.syncMasterComboBox.setModel(self.channelModel)
        self.widget.syncSlaveComboBox.setModel(self.channelModel)

        # prepare
        self.outputChannelControllers: Dict[
            configuration.ADOutputChannels, ADControlOutputSingleController
        ] = {}

        # Add the channels
        for ch in configuration.ADOutputChannels:
            # Create it
            controller = ADControlOutputSingleController(title=ch.name, channel=ch)
            controller.valueChangedSignal.connect(self.channelConfigChanged)

            # Save it
            self.outputChannelControllers[ch] = controller

            # Add it!
            self.widget.layout.addWidget(controller.widget)

        # Connect it all up
        self.connect()

    def fetchCHConfig(
        self, historyID: Union[int, bool] = False
    ) -> Optional[List[ADConfigurationOutputChannelTable]]:
        if isinstance(historyID, bool):
            historyID = self.currentHistoryID

        try:
            return (
                self.DBSession.query(ADConfigurationOutputChannelTable)
                .filter_by(measurementID=historyID)
                .all()
            )

        except Exception as e:
            self.logger.info(f"Failed to fetch CH config: {e}")
            return None

    def connect(self):
        self.widget.syncEnableCheckBox.stateChanged.connect(self.syncEnableChanged)
        self.widget.syncMasterComboBox.currentIndexChanged.connect(self.masterCHChanged)
        self.widget.syncSlaveComboBox.currentIndexChanged.connect(self.slaveCHChanged)

    def syncEnableChanged(self):
        self.logger.info("Enabled state changed")

        # Get the state
        enabled = self.widget.syncEnableCheckBox.isChecked()

        # Save it
        if self.ADControl.currentADConfig is not None:
            self.ADControl.currentADConfig.outputSynchronizationEnabled = enabled

        # Set the enabled state
        self.widget.syncMasterComboBox.setEnabled(enabled)
        self.widget.syncSlaveComboBox.setEnabled(enabled)

        # Inform
        self.channelConfigChanged()

    def masterCHChanged(self, index: int):
        self.logger.info("master CH state changed")

        # Get the channel
        channel = configuration.ADOutputChannels(index)

        # set
        if self.ADControl.currentADConfig is not None:
            self.ADControl.currentADConfig.outputSynchronizationMaster = channel

        # Inform
        self.channelConfigChanged()

    def slaveCHChanged(self, index: int):
        self.logger.info("slave CH state changed")

        # Get the channel
        channel = configuration.ADOutputChannels(index)

        # set
        if self.ADControl.currentADConfig is not None:
            self.ADControl.currentADConfig.outputSynchronizationSlave = channel

        # Inform
        self.channelConfigChanged()

    def channelConfigChanged(self):
        self.logger.info("Channel config changed")

        # inform once again!
        self.channelConfigChangedSignal.emit()

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        # Update the sub controllers
        for _, ctrl in self.outputChannelControllers.items():
            ctrl.projectAboutToClose()

        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        # Update the sub controllers
        for _, ctrl in self.outputChannelControllers.items():
            ctrl.saveProject()

        # Commit
        self.DBSession.commit()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Update the sub controllers
        for _, ctrl in self.outputChannelControllers.items():
            ctrl.historySelectionChanged(currentID)

        # update
        self.updateGUIValues()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Update the sub controllers
        for _, ctrl in self.outputChannelControllers.items():
            ctrl.historyCopySetup(newID)

    def databaseChanged(self):
        self.logger.info("Updating DB")

        # Close the old one, open a new one
        self.DBSession.commit()
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # Update the sub controllers
        for _, ctrl in self.outputChannelControllers.items():
            ctrl.databaseChanged()

        # Update
        self.updateGUIValues()

    def updateGUIValues(self):
        self.logger.info("Updating GUI values")

        if self.ADControl.currentADConfig is None:
            self.logger.info("Na valid AD config found!")
            return

        # Update our values
        self.widget.syncEnableCheckBox.setChecked(
            self.ADControl.currentADConfig.outputSynchronizationEnabled
        )
        self.setSyncMaster(self.ADControl.currentADConfig.outputSynchronizationMaster)
        self.setSyncSlave(self.ADControl.currentADConfig.outputSynchronizationSlave)

    def setSyncMaster(self, masterCH: configuration.ADOutputChannels):
        self.logger.info("Updating master channel")

        newIndex = self.channelModel.channels.index(masterCH)
        self.widget.syncMasterComboBox.setCurrentIndex(newIndex)

    def setSyncSlave(self, slaveCH: configuration.ADOutputChannels):
        self.logger.info("Updating slave channel")

        newIndex = self.channelModel.channels.index(slaveCH)
        self.widget.syncSlaveComboBox.setCurrentIndex(newIndex)


class ADControlOutputSingleWidget(QtW.QGroupBox):
    def __init__(self, title, parent=None):
        super().__init__(title=title, parent=parent)

        self.setObjectName(self.__class__.__name__)

        self.layout = QtH.QFormLayoutRight()

        self.enabledCheckBox = QtW.QCheckBox()

        self.amplitudeSpinBox = QtW.QDoubleSpinBox()
        self.amplitudeSpinBox.setRange(-20, 20)
        self.amplitudeSpinBox.setAccelerated(True)
        self.amplitudeSpinBox.setSingleStep(0.1)
        self.amplitudeSpinBox.setSuffix("V")

        self.offsetSpinBox = QtW.QDoubleSpinBox()
        self.offsetSpinBox.setRange(-20, 20)
        self.offsetSpinBox.setAccelerated(True)
        self.offsetSpinBox.setSingleStep(0.1)
        self.offsetSpinBox.setSuffix("V")

        self.layout.addRow(
            QtW.QLabel(_("ADControl.OutputEnabled")), self.enabledCheckBox
        )
        self.layout.addRow(
            QtW.QLabel(_("ADControl.OutputAmplitude")), self.amplitudeSpinBox
        )
        self.layout.addRow(QtW.QLabel(_("ADControl.OutputOffset")), self.offsetSpinBox)

        # Set the layout
        self.setLayout(self.layout)


class ADControlOutputSingleController(QtC.QObject):
    valueChangedSignal = QtC.pyqtSignal()

    def __init__(self, title: str, channel: configuration.ADOutputChannels):
        super().__init__()

        self.logger = logging.getLogger(self.__class__.__name__)

        self.DBSession = DBConnection.getSession()

        # Save the inputs
        self.title = title
        self.channel = channel
        self.config: Optional[ADConfigurationOutputChannelTable] = None
        self.currentHistoryID: Optional[int] = None

        # set the widget
        self.widget = ADControlOutputSingleWidget(title)

        self.connect()

    def connect(self):
        self.widget.enabledCheckBox.stateChanged.connect(self.enableStateChanged)
        self.widget.amplitudeSpinBox.valueChanged.connect(self.amplitudeChanged)
        self.widget.offsetSpinBox.valueChanged.connect(self.offsetChanged)

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        self.DBSession.commit()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Update
        self.currentHistoryID = currentID
        self.config = (
            self.DBSession.query(ADConfigurationOutputChannelTable)
            .filter_by(channel=self.channel, measurementID=currentID)
            .first()
        )

        # update
        self.updateGUIValues()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Get a new copy of the config
        cfg = self.DBSession.query(ADConfigurationOutputChannelTable).get(
            self.config.id
        )

        # Create a copy
        DBConnection.copyAndAdd(self.DBSession, cfg)

        # Set the ID
        cfg.measurementID = newID

        # Commit to the changes
        self.DBSession.commit()

    def databaseChanged(self):
        self.logger.info("Database changed")

        # Close and reopen
        self.DBSession.commit()
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # get the config
        self.config = (
            self.DBSession.query(ADConfigurationOutputChannelTable)
            .filter_by(channel=self.channel, measurementID=None)
            .first()
        )

        if self.config is None:
            # Set a default
            self.config = ADConfigurationOutputChannelTable(
                channel=self.channel,
                amplitude=1.0,
                offset=0.0,
                enabled=True,
                measurementID=None,
            )

            # Save
            self.DBSession.add(self.config)
            self.DBSession.commit()

        # Update
        self.updateGUIValues()

    def updateGUIValues(self):
        self.logger.info("Updating GUI values")

        # Check for a valid config
        if self.config is None:
            self.logger.info("Not updating, invalid config")
            return

        # Update
        self.setOffset(self.config.offset)
        self.setAmplitude(self.config.amplitude)
        self.setEnabled(self.config.enabled)

    def enableStateChanged(self):
        self.logger.info("enable changed")

        # Check if we can update the config
        if self.config is not None:
            self.config.enabled = self.widget.enabledCheckBox.isChecked()

        # inform
        self.valueChanged()

    def amplitudeChanged(self):
        self.logger.info("Amplitude changed")

        # Check if we can update the config
        if self.config is not None:
            self.config.amplitude = self.widget.amplitudeSpinBox.value()

        # inform
        self.valueChanged()

    def offsetChanged(self):
        self.logger.info("Offset changed")

        # Check if we can update the config
        if self.config is not None:
            self.config.offset = self.widget.offsetSpinBox.value()

        # inform
        self.valueChanged()

    def valueChanged(self):
        self.logger.info("A value changed!")

        # Save the changes
        self.DBSession.commit()

        # inform everybody
        self.valueChangedSignal.emit()

    def setOffset(self, offset):
        self.logger.info("Setting Offset")
        self.widget.offsetSpinBox.setValue(offset)

    def setAmplitude(self, amplitude: float):
        self.logger.info("Setting Amplitude")
        self.widget.amplitudeSpinBox.setValue(amplitude)

    def setEnabled(self, enabled: bool):
        self.logger.info("Setting Enabled")
        self.widget.enabledCheckBox.setChecked(enabled)
