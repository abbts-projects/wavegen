# Native imports
import sys
from typing import Optional, List, Tuple
import logging
import os
import tempfile
import shutil
import filecmp
import threading

# 3rd Party
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import WaveForms, Generator, Signal

# Custom
from WaveGen.GUI.mainConfiguration import MainConfigurationController
from WaveGen.GUI.measurementHistory import MeasurementHistoryController
from WaveGen.GUI.waveConfiguration import WaveConfigurationController
from WaveGen.GUI.ADControl import ADControlController
from WaveGen import configuration
from WaveGen.Helpers.translation import _

from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.DB.configurationTable import ConfigurationTable


class MainWindow(QtW.QMainWindow):
    def __init__(self, controller: "MainWindowController", parent=None):
        super().__init__(parent)

        self.setObjectName(self.__class__.__name__)

        # Set the properties
        self.controller = controller

        # Configure the menu
        self.createMenuItems()

        # Setup of the view
        self.configureView()

        # Configure the window
        self.setMinimumSize(1300, 800)
        self.resize(1300, 800)

    def configureView(self):
        self.setCentralWidget(QtW.QWidget())
        self.centralWidget().setFixedSize(0, 0)

        # Create the main configuration across the top
        self.toolbarMainConfig = QtW.QToolBar()
        self.toolbarMainConfig.setAllowedAreas(
            QtC.Qt.TopToolBarArea | QtC.Qt.BottomToolBarArea
        )
        self.toolbarMainConfig.setObjectName("MainConfigToolbar")
        self.addToolBar(QtC.Qt.TopToolBarArea, self.toolbarMainConfig)

        # More configuration on the right side
        self.toolbarSubConfig = QtW.QToolBar()
        self.toolbarSubConfig.setAllowedAreas(
            QtC.Qt.RightToolBarArea | QtC.Qt.LeftToolBarArea
        )
        self.toolbarSubConfig.setObjectName("SubConfigToolbar")
        self.toolbarSubConfig.setOrientation(QtC.Qt.Vertical)
        self.addToolBar(QtC.Qt.RightToolBarArea, self.toolbarSubConfig)

    def createMenuItems(self):
        bar = self.menuBar()

        ########### File Menu
        fileMenu = bar.addMenu(_("mainWindow.File"))

        # Save
        self.saveAction = QtW.QAction(_("mainWindow.Save"), self)
        self.saveAction.setShortcut(Qt.QKeySequence.Save)
        fileMenu.addAction(self.saveAction)

        # Save as
        self.saveAsAction = QtW.QAction(_("mainWindow.SaveAs"), self)
        self.saveAsAction.setShortcut(Qt.QKeySequence.SaveAs)
        fileMenu.addAction(self.saveAsAction)

        # Open
        self.openAction = QtW.QAction(_("mainWindow.Open"), self)
        self.openAction.setShortcut(Qt.QKeySequence.Open)
        fileMenu.addAction(self.openAction)

        # New
        self.newAction = QtW.QAction(_("mainWindow.New"), self)
        self.newAction.setShortcut(Qt.QKeySequence.New)
        fileMenu.addAction(self.newAction)

        # close
        self.closeAction = QtW.QAction(_("mainWindow.Close"), self)
        self.closeAction.setShortcut(Qt.QKeySequence.Close)
        fileMenu.addAction(self.closeAction)

        ########### Edit Menu
        fileMenu = bar.addMenu(_("mainWindow.Edit"))

        self.undoAction = QtW.QAction(_("mainWindow.Undo"), self)
        self.undoAction.setShortcut(Qt.QKeySequence.Undo)
        self.undoAction.setEnabled(False)
        fileMenu.addAction(self.undoAction)

        self.redoAction = QtW.QAction(_("mainWindow.Redo"), self)
        self.redoAction.setShortcut(Qt.QKeySequence.Redo)
        self.undoAction.setEnabled(False)
        fileMenu.addAction(self.redoAction)

        self.cutAction = QtW.QAction(_("mainWindow.Cut"), self)
        self.cutAction.setShortcut(Qt.QKeySequence.Cut)
        fileMenu.addAction(self.cutAction)

        self.copyAction = QtW.QAction(_("mainWindow.Copy"), self)
        self.copyAction.setShortcut(Qt.QKeySequence.Copy)
        fileMenu.addAction(self.copyAction)

        self.pasteAction = QtW.QAction(_("mainWindow.Paste"), self)
        self.pasteAction.setShortcut(Qt.QKeySequence.Paste)
        fileMenu.addAction(self.pasteAction)

        ########### Wave Menu
        deviceMenu = bar.addMenu(_("mainWindow.Device"))

        self.connectDisconnectDeviceAction = QtW.QAction(
            _("mainWindow.ConnectDisconnectDevice"), self
        )
        self.connectDisconnectDeviceAction.setShortcut("ctrl+d")
        deviceMenu.addAction(self.connectDisconnectDeviceAction)

        self.reloadDevicesAction = QtW.QAction(_("mainWindow.ReloadDevices"), self)
        self.reloadDevicesAction.setShortcut("ctrl+shift+r")
        deviceMenu.addAction(self.reloadDevicesAction)

        ########### Wave Menu
        waveMenu = bar.addMenu(_("mainWindow.Wave"))

        self.addWaveAction = QtW.QAction(_("mainWindow.AddNewWave"), self)
        self.addWaveAction.setShortcut("ctrl+shift+n")
        waveMenu.addAction(self.addWaveAction)

        self.removeWaveAction = QtW.QAction(_("mainWindow.RemoveWave"), self)
        self.removeWaveAction.setShortcut("ctrl+shift+d")
        waveMenu.addAction(self.removeWaveAction)

        ########### Recordings Menu
        recordingsMenu = bar.addMenu(_("mainWindow.Recordings"))

        self.startMeasurement = QtW.QAction(_("mainWindow.StartStopMeasurement"), self)
        self.startMeasurement.setShortcut("ctrl+r")
        recordingsMenu.addAction(self.startMeasurement)

        self.exportMeasurement = QtW.QAction(_("mainWindow.Export"), self)
        self.exportMeasurement.setShortcut("ctrl+e")
        recordingsMenu.addAction(self.exportMeasurement)

        self.deleteMeasurement = QtW.QAction(_("mainWindow.DeleteMeasurement"), self)
        self.deleteMeasurement.setShortcut("ctrl+shift+k")
        recordingsMenu.addAction(self.deleteMeasurement)

        ########### Plot Menu
        plotMenu = bar.addMenu(_("mainWindow.Plot"))

        self.showFFTPlot = QtW.QAction(_("mainWindow.ShowFFT"), self)
        self.showFFTPlot.setShortcut("ctrl+alt+f")
        plotMenu.addAction(self.showFFTPlot)

        self.showXYPlot = QtW.QAction(_("mainWindow.ShowXY"), self)
        self.showXYPlot.setShortcut("ctrl+alt+x")
        plotMenu.addAction(self.showXYPlot)

        ########### View Menu
        viewMenu = bar.addMenu(_("mainWindow.View"))

        self.mainView = QtW.QAction(_("mainWindow.MainView"), self)
        self.mainView.setShortcut("ctrl+alt+n")
        self.mainView.setDisabled(True)
        viewMenu.addAction(self.mainView)

        # ADController


class MainWindowController(QtC.QObject):
    def __init__(self):
        super().__init__()

        # Get a logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # Predefine
        self.currentProjectPath = None
        self.initialBackupPath = None
        self.currentHistoryID: Optional[int] = None

        # Get the main window
        self.mainWindow = MainWindow(self)
        self.mainWindow.setWindowIcon(Qt.QIcon("img/icon.png"))

        # Init the main configuration
        self.mainConfiguration = MainConfigurationController()
        self.mainWindow.toolbarMainConfig.addWidget(self.mainConfiguration.widget)

        # Set the close event to be handled by us
        self.mainWindow.closeEvent = self.closeEvent

        # Wave history
        self.measurementHistoryController = MeasurementHistoryController()

        # Init the ADControl
        self.ADController = ADControlController(
            mainWindow=self.mainWindow,
            mainConfig=self.mainConfiguration,
            historyController=self.measurementHistoryController,
        )
        self.ADController.addControlWidgetToToolbar(self.mainWindow.toolbarSubConfig)

        # Add the custom log handler
        lh = LogHandler(self.mainWindow.statusBar())
        lh.setLevel(logging.INFO)

        # Get the global logger
        logger = logging.getLogger()
        logger.addHandler(lh)

        # Wave configurator
        self.waveConfigurationController = WaveConfigurationController(
            mainConfiguration=self.mainConfiguration,
            outputController=self.ADController.outputController,
        )

        # Update the meas history controller
        self.measurementHistoryController.waveConfigController = (
            self.waveConfigurationController
        )
        self.measurementHistoryController.ADControl = self.ADController

        self.measurementHistoryController.addWidgetToToolbar(
            self.mainWindow.toolbarSubConfig
        )
        self.waveConfigurationController.addGeneratedSignalPlotToWindow(self.mainWindow)
        self.waveConfigurationController.addConfigurationToToolbar(
            self.mainWindow.toolbarSubConfig
        )

        self.ADController.addMeasuredSignalPlotToWindow(self.mainWindow)

        # Connect
        self.connect()

        # Prepare the database
        self.newProject(dontAsk=True)

        # Log the new path
        self.logger.info(f"New database path: {self.DBPath}")

    def connect(self):
        # Menu Signals
        self.mainWindow.saveAction.triggered.connect(self.saveProject)
        self.mainWindow.saveAsAction.triggered.connect(self.saveProjectAs)

        self.mainWindow.openAction.triggered.connect(self.openProject)
        self.mainWindow.newAction.triggered.connect(self.newProject)
        # self.mainWindow.closeAction.triggered.connect(self.)
        # self.mainWindow.undoAction.triggered.connect(self.)
        # self.mainWindow.redoAction.triggered.connect(self.)
        # self.mainWindow.cutAction.triggered.connect(self.)
        # self.mainWindow.copyAction.triggered.connect(self.)
        # self.mainWindow.pasteAction.triggered.connect(self.)

        self.mainWindow.connectDisconnectDeviceAction.triggered.connect(
            self.mainConfiguration.connectCurrentDevice
        )

        self.mainWindow.reloadDevicesAction.triggered.connect(
            self.mainConfiguration.updateDeviceList
        )

        self.mainWindow.addWaveAction.triggered.connect(
            self.waveConfigurationController.addNewWave
        )
        self.mainWindow.removeWaveAction.triggered.connect(
            self.waveConfigurationController.deleteSelectedWave
        )
        self.mainWindow.startMeasurement.triggered.connect(
            self.mainConfiguration.recordButtonClicked
        )
        self.mainWindow.exportMeasurement.triggered.connect(
            self.measurementHistoryController.exportSelectedMeasurement
        )
        self.mainWindow.deleteMeasurement.triggered.connect(
            self.measurementHistoryController.deleteSelectedMeasurement
        )
        self.mainWindow.showFFTPlot.triggered.connect(
            self.ADController.recordingController.showFFTPlot
        )
        self.mainWindow.mainView.triggered.connect(self.showMainWindow)

        self.mainWindow.showXYPlot.triggered.connect(
            self.ADController.recordingController.showXYPlot
        )

        # Other signals
        self.mainConfiguration.signalDeviceChanged.connect(self.ADDeviceChanged)
        self.mainConfiguration.signalValuesChanged.connect(self.configChanged)
        self.mainConfiguration.signalStartRecording.connect(
            self.ADController.recordingController.runMeasurements
        )

        # Update the plot
        self.ADController.outputController.channelConfigChangedSignal.connect(
            self.waveConfigurationController.updateSampleWavePlot
        )

        # History changed
        self.measurementHistoryController.selectionChangedSignal.connect(
            self.historySelectionChanged
        )
        self.measurementHistoryController.createCopyForMeasurementSignal.connect(
            self.historyCopySetup
        )

    def closeEvent(self, event):
        logging.info("Asking user, if he wants to quit the application")

        # Show a question
        choice = self.saveUnsavedDataDialog()

        # do stuff
        if choice:
            logging.info("User choose to quit application")

            # Close the old one
            self.projectAboutToClose()

            event.accept()
        else:
            logging.info("User choose to keep app running")
            event.ignore()

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        self.measurementHistoryController.projectAboutToClose()
        self.waveConfigurationController.projectAboutToClose()
        self.ADController.projectAboutToClose()

    def databaseChanged(self):
        # Inform all the controllers
        self.mainConfiguration.databaseChanged()
        self.measurementHistoryController.databaseChanged()
        self.waveConfigurationController.databaseChanged()
        self.ADController.databaseChanged()

        # Update the title
        self.updateWindowTitle()

        # Update the signal Plot
        self.waveConfigurationController.updateSampleWavePlot()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection changed")

        # Inform
        self.currentHistoryID = currentID
        self.mainConfiguration.historySelectionChanged(currentID)
        self.waveConfigurationController.historySelectionChanged(currentID)
        self.ADController.historySelectionChanged(currentID)

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        self.mainConfiguration.historyCopySetup(newID)
        self.waveConfigurationController.historyCopySetup(newID)
        self.ADController.historyCopySetup(newID)

    def waveConfigurationChanged(self):
        self.logger.info("Wave configuration changed!")

        # Update the plot
        self.updateSampleWavePlot()

    def updateWindowTitle(self):
        # Get the path
        path = ""
        if self.currentProjectPath is not None:
            path = f" - {self.currentProjectPath}"

        # Set the title
        self.mainWindow.setWindowTitle(f"WaveGen{path}")

    def showMainWindow(self):
        # Update the title
        self.updateWindowTitle()

        # Show the window
        self.mainWindow.show()

    def saveProject(self):
        self.logger.info("User wants to save project!")

        # Inform everybody
        self.measurementHistoryController.saveProject()
        self.waveConfigurationController.saveProject()
        self.ADController.saveProject()

        if self.currentProjectPath is None:
            self.logger.info("Saving project as, no path set")
            self.saveProjectAs()

        else:
            self.logger.info("Saving project")

            # Now save it
            self.saveDatabase()

        self.logger.info("Saving project completed")

    def saveProjectAs(self):
        self.logger.info("Saving project as")

        # Get the save path
        path, __ = Qt.QFileDialog.getSaveFileName(
            self.mainWindow,
            _("mainWindow.saveProject_dialog"),
            _("mainWindow.SaveFilename"),
            "WaveGen (*.wvg)",
        )

        if path:
            # Set the path
            self.currentProjectPath = path

            # Now save it
            self.saveDatabase()

        # Update the title
        self.databaseChanged()

        self.logger.info("Saving project as completed")

    def saveDatabase(self, path: Optional[str] = None):
        # Save the window state
        savSt = self.mainWindow.saveState()
        self.mainConfiguration.setWindowSaveState(savSt.toBase64())

        # Check if we have a path
        if path is None:
            path = self.currentProjectPath

        # Copy to the new location
        shutil.copy2(self.DBPath, path)

        # Check if we have an initial file
        if self.initialBackupPath:
            os.unlink(self.initialBackupPath)
            self.initialBackupPath = None

    def openProject(self):
        self.logger.info("Should open project")

        # Check if we can discard the data
        if self.saveUnsavedDataDialog():
            # Get the path
            path, __ = Qt.QFileDialog.getOpenFileName(
                self.mainWindow,
                _("mainWindow.selectProject_dialog"),
                "",
                "WaveGen (*.wvg)",
            )

            # Close the old one
            self.projectAboutToClose()

            if path:
                # open it
                self.openDatabase(path)

        # Set the window state
        windowState = self.mainConfiguration.getWindowSaveState()

        if windowState is not None:
            self.mainWindow.restoreState(QtC.QByteArray.fromBase64(windowState))

    def openDatabase(self, path: str):
        # Get a temporary file
        self.DBPath = tempfile.NamedTemporaryFile().name

        # Copy the file
        shutil.copy2(path, self.DBPath)
        self.currentProjectPath = path

        # Set the path in the config
        configuration.projectPath = path

        # Set the new path
        DBConnection.setEnginePath(self.DBPath)

        # Update the title
        self.databaseChanged()

    @property
    def hasUnsavedData(self) -> bool:
        # Check if we have a save path
        if self.currentProjectPath is None:
            # No path set, check if we have a backup file
            if self.initialBackupPath is None:
                # Nope? ask to save (better save than sorry)
                return True
            else:
                try:
                    # Compare, if the initial backup is the same
                    return not filecmp.cmp(self.DBPath, self.initialBackupPath)
                except Exception as e:
                    self.logger.warning(f"Failed to compare files: {e}")
                    return True

        # Compare the files
        return not filecmp.cmp(self.DBPath, self.currentProjectPath)

    def saveUnsavedDataDialog(self) -> bool:
        # Just accept it
        choice = QtW.QMessageBox.No

        # Check if we have unsaved data
        if self.hasUnsavedData:
            # Ask user
            choice = QtW.QMessageBox.warning(
                self.mainWindow,
                _("mainWindow.saveDialogTitle"),
                _("mainWindow.saveDialogBody"),
                QtW.QMessageBox.Yes | QtW.QMessageBox.No,
                QtW.QMessageBox.Yes,
            )

            if choice == QtW.QMessageBox.Yes:
                self.saveProject()

                return True

        return choice == QtW.QMessageBox.No

    def newProject(self, dontAsk=False):
        self.logger.info("Starting new project")

        # Check if we can discard the data
        if dontAsk or self.saveUnsavedDataDialog():
            # Close the old one
            self.projectAboutToClose()

            # Get a temporary file
            self.DBPath = tempfile.NamedTemporaryFile().name
            self.currentProjectPath = None
            self.initialBackupPath = f"{self.DBPath}-backup"

            # Set the new path
            DBConnection.setEnginePath(self.DBPath)
            self.createDBBaseTables()

            # Copy the backup, wait a bit for everything to settle in...
            # Yeah, there should be a better way
            copyThread = threading.Timer(
                0.5, lambda: shutil.copy2(self.DBPath, self.initialBackupPath)
            )
            copyThread.start()

            # Update the title
            self.databaseChanged()

    def ADDeviceChanged(self, device: WaveForms.Device):
        self.logger.info(f"Device: {device}")

        # Update the AD info controller
        # self.ADInformationController.connectionStateChanged()
        # TODO: Move to AD controller

    def configChanged(self):
        self.logger.info(f"Configuration changed!")

        # Replot the sample wave
        self.waveConfigurationController.updateSampleWavePlot()

    def createDBBaseTables(self):
        self.logger.info("Creating new Database")

        # Create the DB
        DBConnection.Base.metadata.create_all(DBConnection.engine)


class LogHandler(logging.StreamHandler):
    def __init__(self, statusbar: QtW.QStatusBar):
        super().__init__()

        # Set the status bar
        self.statusbar = statusbar

    def emit(self, record: logging.LogRecord):
        self.statusbar.showMessage(record.msg)
