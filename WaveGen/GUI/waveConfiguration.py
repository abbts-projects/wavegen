# Native imports
import sys
import logging
from typing import Optional, Dict

# 3rd party imports
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta

# Custom
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.GUI.waveConfigurator import WaveConfiguratorController
from WaveGen.GUI.wavePlot import WavePlotController
from WaveGen.Helpers.pyqtHelpers import QSpacer, QHLine, QTitleLabel
from WaveGen.Helpers.waveCalculator import WaveCalculator
from WaveGen.Helpers.translation import _
from WaveGen import Models
from WaveGen import configuration


class WaveConfigurationWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setMinimumWidth(250)

        # Create a layout
        self.layout = QtW.QVBoxLayout()
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

        # Separate
        self.layout.addWidget(QHLine("#222"))
        self.layout.addWidget(QTitleLabel(_("waveConfiguration.title"), "#aaa"))

        # Add the list view
        self.waveList = QtW.QListView()
        self.layout.addWidget(self.waveList)

        # Create the toolbar at the bottom
        toolbar = QtW.QToolBar()
        toolbar.setIconSize(QtC.QSize(15, 15))
        self.layout.addWidget(toolbar)

        # Spacer to force everything to the right
        toolbar.addWidget(QSpacer())

        # Create button
        plusIcon = qta.icon("mdi.plus", options=[{"color": "white"}])
        self.addWaveAction = QtW.QAction(plusIcon, "", self)
        toolbar.addAction(self.addWaveAction)
        toolbar.addSeparator()

        trashIcon = qta.icon("mdi.trash-can-outline", options=[{"color": "white"}])
        self.deleteWaveAction = QtW.QAction(trashIcon, "", self)
        toolbar.addAction(self.deleteWaveAction)

        # Set the layout
        self.setLayout(self.layout)


class WaveConfigurationController(QtC.QObject):
    waveConfigurationChangedSignal = QtC.pyqtSignal()

    def __init__(
        self,
        mainConfiguration: "mainConfigurationController",
        outputController: "ADControlOutput",
    ):
        super().__init__()

        # Get a logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # Prepare
        self.DBSession = DBConnection.getSession()
        self.wcc: Optional[WaveConfiguratorController] = None
        self.waveConfigs: Dict[int, WaveConfiguratorController] = {}
        self.currentHistoryID: Optional[int] = None

        # Save the params
        self.mainConfiguration = mainConfiguration
        self.outputController = outputController

        # Get the widget running
        self.widget = WaveConfigurationWidget()

        # Connect the signals
        self.connect()

        # Create the plot
        self.generatedSignalPlotController = WavePlotController(
            _("waveConfiguration.generatedSignalWidget")
        )

        # Set the plot labels
        pltWi = self.generatedSignalPlotController.widget.plotWidget
        pltWi.setLabel("left", _("plotting.voltage_axis"))
        pltWi.setLabel("bottom", _("plotting.time_axis"))

        # Update
        self.waveConfigModel = Models.WaveConfigurationModel()
        self.widget.waveList.setModel(self.waveConfigModel)

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        # Commit
        self.DBSession.commit()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Update the model
        self.currentHistoryID = currentID
        self.waveConfigModel.setMeasurementID(currentID)

        # Update GUI
        self.waveConfigurationChanged()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # get a list of waves
        waves = (
            self.DBSession.query(WaveConfigurationTable)
            .filter_by(measurementID=self.currentHistoryID)
            .all()
        )

        for wave in waves:
            # Get a new copy of the wave
            wvg = self.DBSession.query(WaveConfigurationTable).get(wave.id)

            # Create a copy
            DBConnection.copyAndAdd(self.DBSession, wvg)

            # Set the ID
            wvg.measurementID = newID

        # Commit to the changes
        self.DBSession.commit()

    def addGeneratedSignalPlotToWindow(self, window):
        # Generated Signal plot
        window.addDockWidget(
            QtC.Qt.LeftDockWidgetArea, self.generatedSignalPlotController.dockableWidget
        )

    def addConfigurationToToolbar(self, toolbar: QtW.QToolBar):
        # and ourselves
        toolbar.addWidget(self.widget)

    def connect(self):
        self.widget.addWaveAction.triggered.connect(self.addNewWave)
        self.widget.deleteWaveAction.triggered.connect(self.deleteSelectedWave)
        self.widget.waveList.doubleClicked.connect(self.waveDoubleClick)

    def databaseChanged(self):
        self.logger.info("Reloading database")

        # Close the old one, open a new one
        self.DBSession.commit()
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # Reload data
        self.waveConfigurationChanged()

    def addNewWave(self):
        self.logger.info("Clicked add new wave")

        if not self.wcc:
            self.wcc = WaveConfiguratorController(self.widget)
            self.wcc.waveUpdatedSignal.connect(self.waveConfigurationChanged)
            self.wcc.windowClosedSignal.connect(self.waveConfiguratorWindowClosed)
            self.wcc.showWindow()

    def waveConfiguratorWindowClosed(self, waveID: Optional[int]):
        self.logger.info("Config window was closed")

        # Check the wave ID: None -> new config, int -> existing wave
        if waveID is None:
            # Reset
            self.wcc = None
        else:
            # Check if the id exists
            if waveID in list(self.waveConfigs.keys()):
                # Delete it
                del self.waveConfigs[waveID]

    def deleteSelectedWave(self):
        self.logger.info("clicked to delete wave")

        # get the current index
        currentIndex = self.widget.waveList.currentIndex().row()

        if currentIndex == -1:
            self.logger.info("can't delete nothing!")
            return

        if currentIndex >= len(self.waveConfigModel.waves):
            self.logger.info("Selected index is out of range, can't delete!")
            return

        # We have something
        entry = self.waveConfigModel.waves[currentIndex]

        if entry is None:
            self.logger.info(
                "The currently selected entry does not exist anymore, can't delete"
            )
            return

        # connect to the DB & delete
        self.DBSession.query(WaveConfigurationTable).filter_by(id=entry.id).delete()
        self.DBSession.commit()

        # update
        self.waveConfigurationChanged()

    def waveConfigurationChanged(self, wave: Optional[WaveConfigurationTable] = None):
        self.logger.info("Wave configuration changed")

        # Update
        self.waveConfigModel.updateWaves()
        self.waveConfigModel.layoutChanged.emit()

        # Check how many waves we have
        self.widget.deleteWaveAction.setEnabled(len(self.waveConfigModel.waves) > 0)

        # plot
        self.updateSampleWavePlot()

        # Send out a new signal
        self.waveConfigurationChangedSignal.emit()

    def waveDoubleClick(self, index: QtC.QModelIndex):
        self.logger.info("Double clicked a wave row")

        # get the row
        row = index.row()

        # Get the DB Entry
        dbEntry = self.waveConfigModel.waves[row]

        # Create a new configurator
        if dbEntry.id not in list(self.waveConfigs.keys()):
            configurator = WaveConfiguratorController(self.widget, dbEntry.id)
            configurator.waveUpdatedSignal.connect(self.waveConfigurationChanged)
            configurator.windowClosedSignal.connect(self.waveConfiguratorWindowClosed)
            configurator.showWindow()

            # Save it
            self.waveConfigs[dbEntry.id] = configurator

        else:
            # Show the window
            self.waveConfigs[dbEntry.id].showWindow()

    def updateSampleWavePlot(self):
        self.logger.info("Updating sample wave plot")

        try:
            # Get all waves from the DB
            DBSession = DBConnection.getSession()
            waves = (
                DBSession.query(WaveConfigurationTable)
                .filter_by(measurementID=self.currentHistoryID)
                .all()
            )
            DBSession.close()

        except Exception as e:
            self.logger.info(
                f"Failed to update sample wave plot, failed to connect to DB: {e}"
            )
            return

        # Prepare
        offsets: Dict[configuration.ADOutputChannels, float] = {}
        amplitudes: Dict[configuration.ADOutputChannels, float] = {}
        enabled: Dict[configuration.ADOutputChannels, float] = {}

        mainOffset = self.mainConfiguration.currentConfiguration.offset
        mainAmplitude = self.mainConfiguration.currentConfiguration.amplitude

        # Get the config
        chConfig = self.outputController.fetchCHConfig(historyID=self.currentHistoryID)

        if chConfig is None:
            self.logger.warning("Failed to fetch the CH config!")
            return

        # Loop each config
        for output in chConfig:
            offsets[output.channel] = mainOffset + output.offset
            amplitudes[output.channel] = mainAmplitude * output.amplitude
            enabled[output.channel] = output.enabled

        # Plot :)
        if self.mainConfiguration.currentConfiguration is not None:
            WaveCalculator.calculateWavesAndPlot(
                waveConfigs=waves,
                amplitude=amplitudes,
                offset=offsets,
                enabled=enabled,
                periods=self.mainConfiguration.currentConfiguration.periods,
                sampleCount=self.mainConfiguration.currentConfiguration.sampleCount,
                individualPlots=self.mainConfiguration.currentConfiguration.individual,
                masterPlot=True,
                clipVoltage=self.mainConfiguration.currentConfiguration.clipVoltage,
                plotCanvas=self.generatedSignalPlotController.widget.plotWidget,
            )
        else:
            self.logger.info(
                "Can't do main plot, as there is no valid configuration set"
            )

    def copyConfigurationToMeasurementID(self, measID: int):
        pass
        # TODO

    def getConfigurationForID(self, measID: int):
        pass
        # TODO
