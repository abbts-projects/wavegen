# Native imports
import sys
import logging
from typing import List, Optional
from datetime import datetime
import threading
import time

# 3rd party imports
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import WaveForms
import qtawesome as qta

# Custom
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.configurationTable import ConfigurationTable
from WaveGen.DB.ADConfigurationTable import ADConfigurationTable
from WaveGen.Helpers.pyqtHelpers import QVLine, QSpacer
from WaveGen.Helpers.SBThread import SBThread
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen.Helpers.translation import _
from WaveGen import configuration


class ADControlInformationWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        # Start a new layout
        self.layout = QtH.QFormLayoutRight()

        # Shared style!
        labelStyleSheet = "font-weight: bold;"

        # DWF

        # Serial NR, name, DWF version, resolution, ....
        self.dwfLabel = QtH.QLabelItalic("")
        self.serialNrLabel = QtH.QLabelItalic("")
        self.deviceNameLabel = QtH.QLabelItalic("")

        self.layout.addRow(QtH.QTitleLabel(_("ADControl.InfoGeneral")), QtW.QLabel())
        self.layout.addRow(
            QtW.QLabel(_("ADControl.InfoAppV")), QtH.QLabelItalic(configuration.version)
        )
        self.layout.addRow(QtW.QLabel(_("ADControl.InfoDWF")), self.dwfLabel)
        self.layout.addRow(QtW.QLabel(_("ADControl.InfoSerialNr")), self.serialNrLabel)
        self.layout.addRow(
            QtW.QLabel(_("ADControl.InfoDeviceName")), self.deviceNameLabel
        )
        self.layout.addRow(QtH.QSpacer(height=5), QtH.QSpacer(height=5))

        # USB
        self.usbVoltageValueLabel = QtH.QLabelItalic("")
        self.usbCurrentValueLabel = QtH.QLabelItalic("")

        self.layout.addRow(QtH.QTitleLabel(_("ADControl.InfoUSB")), QtW.QLabel())
        self.layout.addRow(
            QtW.QLabel(_("ADControl.InfoVoltage")), self.usbVoltageValueLabel
        )
        self.layout.addRow(
            QtW.QLabel(_("ADControl.InfoCurrent")), self.usbCurrentValueLabel
        )
        self.layout.addRow(QtH.QSpacer(height=5), QtH.QSpacer(height=5))

        # Aux
        self.auxVoltageValueLabel = QtH.QLabelItalic("")
        self.auxCurrentValueLabel = QtH.QLabelItalic("")

        self.layout.addRow(QtH.QTitleLabel(_("ADControl.InfoAux")), QtW.QLabel())
        self.layout.addRow(
            QtW.QLabel(_("ADControl.InfoVoltage")), self.auxVoltageValueLabel
        )
        self.layout.addRow(
            QtW.QLabel(_("ADControl.InfoCurrent")), self.auxCurrentValueLabel
        )
        self.layout.addRow(QtH.QSpacer(height=5), QtH.QSpacer(height=5))

        # Temperature
        self.temperatureValueLabel = QtH.QLabelItalic("")
        self.layout.addRow(
            QtH.QTitleLabel(_("ADControl.InfoTemperature")), QtW.QLabel()
        )
        self.layout.addRow(
            QtW.QLabel(_("ADControl.InfoTempInternal")), self.temperatureValueLabel
        )

        # Set the layout
        self.setLayout(self.layout)


class ADControlInformationController(QtC.QObject):
    def __init__(self, ADControl: "ADControl", parent=None):
        super().__init__(parent=parent)

        # Get a logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # Save the inputs
        self.ADControl = ADControl

        # Prepare
        self.updateTimeThread: Optional[threading.Thread] = None

        # Init the views
        self.widget = ADControlInformationWidget()

        # Parameters
        self.updateInterval = 2  # In seconds
        self.stopParameterUpdates: Optional[threading.Event] = None

        # Reset
        self.resetGUIValues()

        # Connect
        self.connect()

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        # Stop the timer (if we have one)
        self.stopTimer()

    def saveProject(self):
        self.logger.info("Saving project")

        # Nothing here

    def updateGUIValues(self):
        self.logger.info("Updating GUI")

    def closeEvent(self, event):
        self.logger.info("Closing AD information window")

        # Cancel the update timer
        self.stopTimer()

    def showEvent(self, event):
        self.logger.info("Showing AD Information window")

        # Start the timer
        self.startTimer()

    def startTimer(self):
        self.logger.info("Setup AD Information starting timer")

        # Setup the timer
        if self.updateTimeThread is None or not self.updateTimeThread.isAlive():
            self.updateTimeThread = threading.Thread(target=self.updateParameters)
            self.stopParameterUpdates = threading.Event()

            # Start
            self.updateTimeThread.start()

    def stopTimer(self):
        self.logger.info("Stopping AD Information timer")

        # Cancel the timer, if we have one
        if self.updateTimeThread is not None and self.updateTimeThread.isAlive():
            self.stopParameterUpdates.set()
            self.updateTimeThread = None

    def connect(self):
        pass

    def connectionStateChanged(self):
        # Check the new state
        if not configuration.waveforms.device:
            # Stop the timer
            self.stopTimer()

            self.resetGUIValues()

        else:
            # Start the timer
            self.startTimer()

            # Update AD info
            self.widget.dwfLabel.setText(configuration.waveforms.getVersion())
            self.widget.serialNrLabel.setText(configuration.waveforms.device.serial)
            self.widget.deviceNameLabel.setText(configuration.waveforms.device.name)

    def resetGUIValues(self):
        self.logger.info("Resetting AD Info GUI values")
        self.widget.dwfLabel.setText("---")
        self.widget.serialNrLabel.setText("---")
        self.widget.deviceNameLabel.setText("---")

        self.widget.usbVoltageValueLabel.setText(f"{0.0:.2f}")
        self.widget.usbCurrentValueLabel.setText(f"{0.0:.2f}")
        self.widget.auxVoltageValueLabel.setText(f"{0.0:.2f}")
        self.widget.auxCurrentValueLabel.setText(f"{0.0:.2f}")
        self.widget.temperatureValueLabel.setText(f"{0.0:.2f}")

    def updateParameters(self):
        while not self.stopParameterUpdates.wait(self.updateInterval):
            self.logger.debug("Updating AD Values...")

            if not configuration.waveforms.device:
                self.logger.info("Not connected to a device!")
                return

            # Get the new values
            try:
                values = configuration.waveforms.readDeviceStatistics()

            except WaveForms.DWFError as e:
                self.logger.info(f"Encountered DWF error: {e}")
                return

            # update
            SBThread.runOnMainThread(
                target=lambda: (
                    self.widget.usbVoltageValueLabel.setText(
                        f"{values.usbVoltage:.2f}"
                    ),
                    self.widget.usbCurrentValueLabel.setText(
                        f"{values.usbCurrent:.2f}"
                    ),
                    self.widget.auxVoltageValueLabel.setText(
                        f"{values.auxVoltage:.2f}"
                    ),
                    self.widget.auxCurrentValueLabel.setText(
                        f"{values.auxCurrent:.2f}"
                    ),
                    self.widget.temperatureValueLabel.setText(
                        f"{values.temperature:.2f}"
                    ),
                )
            )
