# Native imports
from typing import Optional, List, Tuple, Dict
import logging
from enum import Enum, auto

# 3rd Party
import pyqtgraph as QtG
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import WaveForms, Generator, Signal, AnalogWaveforms
import qtawesome as qta
import dwf

# Custom
from WaveGen.GUI.wavePlot import WavePlotController
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.DB.ADConfigurationTable import ADConfigurationTable
from WaveGen.Helpers.translation import _
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import configuration
from WaveGen import Models


class ADControlTriggerWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setObjectName(self.__class__.__name__)

        self.setMinimumWidth(250)

        self.layout = QtH.QFormLayoutRight()

        # Add a title
        self.layout.addRow(
            QtH.QTitleLabel(_("ADControl.Trigger"), "#aaa"), QtW.QLabel("")
        )

        # setup the triggering
        self.enableCheckBox = QtW.QCheckBox()
        self.channelComboBox = QtW.QComboBox()
        self.triggerTypeComboBox = QtW.QComboBox()
        self.conditionComboBox = QtW.QComboBox()
        self.sourceComboBox = QtW.QComboBox()
        self.levelSpinBox = QtW.QDoubleSpinBox()
        self.levelSpinBox.setRange(-25, 25)
        self.levelSpinBox.setAccelerated(True)
        self.levelSpinBox.setSuffix("V")
        self.levelSpinBox.setSingleStep(0.1)

        # Add them to the GUI
        self.layout.addRow(
            QtW.QLabel(_("ADControl.TriggerEnable")), self.enableCheckBox
        )
        self.layout.addRow(
            QtW.QLabel(_("ADControl.TriggerChannel")), self.channelComboBox
        )
        self.layout.addRow(QtW.QLabel(_("ADControl.TriggerLevel")), self.levelSpinBox)
        self.layout.addRow(
            QtW.QLabel(_("ADControl.TriggerType")), self.triggerTypeComboBox
        )
        self.layout.addRow(
            QtW.QLabel(_("ADControl.TriggerCondition")), self.conditionComboBox
        )
        self.layout.addRow(
            QtW.QLabel(_("ADControl.TriggerSource")), self.sourceComboBox
        )

        # Set the layout
        self.setLayout(self.layout)


class ADControlTriggerController(QtC.QObject):
    def __init__(self, ADControl: "ADControl", parent=None):
        super().__init__(parent=parent)

        self.logger = logging.getLogger(self.__class__.__name__)

        self.DBSession = DBConnection.getSession()

        # Save the inputs
        self.ADControl = ADControl

        # set the widget
        self.widget = ADControlTriggerWidget()

        # connect the models
        self.channelModel = Models.ADInputChannelsModel()
        self.triggerModel = Models.ADControlTriggerTypeModel()
        self.conditionModel = Models.ADControlConditionModel()
        self.sourceModel = Models.ADControlSourceModel()

        self.widget.channelComboBox.setModel(self.channelModel)
        self.widget.triggerTypeComboBox.setModel(self.triggerModel)
        self.widget.conditionComboBox.setModel(self.conditionModel)
        self.widget.sourceComboBox.setModel(self.sourceModel)

        self.connect()

    ## APP
    def projectAboutToClose(self):
        self.logger.info("Project about to close!")
        # TODO

        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        # Commit
        self.DBSession.commit()

    ## DB
    def databaseChanged(self):
        self.logger.info("Database changed")

        # Close and open a new one
        self.DBSession.commit()
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Update: Gets it's data directly from the ADControl!

        # update GUI
        self.updateGUIValues()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Nothing to do here!

    ## Other
    def connect(self):
        self.widget.enableCheckBox.stateChanged.connect(self.enableChanged)
        self.widget.channelComboBox.currentIndexChanged.connect(self.channelChanged)
        self.widget.levelSpinBox.valueChanged.connect(self.levelChanged)
        self.widget.triggerTypeComboBox.currentIndexChanged.connect(
            self.triggerTypeChanged
        )
        self.widget.conditionComboBox.currentIndexChanged.connect(self.conditionChanged)
        self.widget.sourceComboBox.currentIndexChanged.connect(self.sourceChanged)

    def enableChanged(self):
        self.logger.info("Enable changed!")
        self.ADControl.currentADConfig.triggerEnable = (
            self.widget.enableCheckBox.isChecked()
        )
        self.valuesChanged()

    def levelChanged(self):
        self.logger.info("Level changed!")
        self.ADControl.currentADConfig.triggerLevel = self.widget.levelSpinBox.value()
        self.valuesChanged()

    def channelChanged(self):
        self.logger.info("Channel changed!")
        self.ADControl.currentADConfig.triggerChannel = self.channel()
        self.valuesChanged()

    def triggerTypeChanged(self):
        self.logger.info("Trigger changed!")
        self.ADControl.currentADConfig.triggerType = self.triggerType()
        self.valuesChanged()

    def conditionChanged(self):
        self.logger.info("Condition changed!")
        self.ADControl.currentADConfig.triggerCondition = self.condition()
        self.valuesChanged()

    def sourceChanged(self):
        self.logger.info("source changed!")
        self.ADControl.currentADConfig.triggerSource = self.source()
        self.valuesChanged()

    def valuesChanged(self):
        self.logger.info("Values changed!")

    def updateGUIValues(self):
        self.logger.info("Updating GUI values")

        if self.ADControl.currentADConfig is None:
            self.logger.info("We don't have a valid ADConfig!")
            return

        # Set the combo boxes
        self.setChannel(self.ADControl.currentADConfig.triggerChannel)
        self.setTriggerType(self.ADControl.currentADConfig.triggerType)
        self.setCondition(self.ADControl.currentADConfig.triggerCondition)
        self.setSource(self.ADControl.currentADConfig.triggerSource)

        # Update the voltage level & trigger enable
        self.widget.levelSpinBox.setValue(self.ADControl.currentADConfig.triggerLevel)
        self.widget.enableCheckBox.setChecked(
            self.ADControl.currentADConfig.triggerEnable
        )

    def setChannel(self, channel: configuration.ADInputChannels):
        self.logger.info("Setting new Channel")

        # Get the new index and set it
        newIndex = self.channelModel.channels.index(channel)
        self.widget.channelComboBox.setCurrentIndex(newIndex)

    def setTriggerType(self, triggerType: dwf.DwfAnalogIn.TRIGTYPE):
        self.logger.info("Setting new TriggerType")

        # Get the new index and set it
        newIndex = self.triggerModel.trigTypes.index(triggerType)
        self.widget.triggerTypeComboBox.setCurrentIndex(newIndex)

    def setCondition(self, condition: dwf.DwfAnalogIn.TRIGCOND):
        self.logger.info("Setting new Condition")

        # Get the new index and set it
        newIndex = self.conditionModel.conditions.index(condition)
        self.widget.conditionComboBox.setCurrentIndex(newIndex)

    def setSource(self, source: dwf.DwfAnalogIn.TRIGSRC):
        self.logger.info("Setting new Source")

        # Get the new index and set it
        newIndex = self.sourceModel.sources.index(source)
        self.widget.sourceComboBox.setCurrentIndex(newIndex)

    def channel(self) -> configuration.ADInputChannels:
        self.logger.info("Getting current channel")
        cIndex = self.widget.channelComboBox.currentIndex()
        return self.channelModel.channels[cIndex]

    def triggerType(self) -> dwf.DwfAnalogIn.TRIGTYPE:
        self.logger.info("Getting current trigger type")
        cIndex = self.widget.triggerTypeComboBox.currentIndex()
        return self.triggerModel.trigTypes[cIndex]

    def condition(self) -> dwf.DwfAnalogIn.TRIGCOND:
        self.logger.info("Getting current condition")
        cIndex = self.widget.conditionComboBox.currentIndex()
        return self.conditionModel.conditions[cIndex]

    def source(self) -> dwf.DwfAnalogIn.TRIGSRC:
        self.logger.info("Getting current source")
        cIndex = self.widget.sourceComboBox.currentIndex()
        return self.sourceModel.sources[cIndex]
