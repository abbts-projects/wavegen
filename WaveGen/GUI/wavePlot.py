# Standard imports
import sys
import logging


# 3rd party imports
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import pyqtgraph as QtG


# Custom imports
from WaveGen.Helpers.translation import _


class WavePlotDockWidget(QtW.QDockWidget):
    def __init__(self, title: str, widget: QtW.QWidget, parent=None):
        super().__init__(title, parent=parent)

        self.setObjectName(f"{self.__class__.__name__}-{title}")

        # Disable closing
        self.setFeatures(self.features() & ~QtW.QDockWidget.DockWidgetClosable)

        # Set the content
        self.setWidget(widget)


class WavePlotWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        # Create the layout
        self.layout = QtW.QHBoxLayout()
        self.setLayout(self.layout)

        # Create the plot
        self.plotWidget = QtG.PlotWidget(None)
        self.plotWidget.showGrid(x=True, y=True, alpha=0.7)
        self.layout.addWidget(self.plotWidget)


class WavePlotController(QtC.QObject):
    def __init__(self, title: str):
        super().__init__()

        # Get a logger
        self.logger = logging.getLogger(f"{self.__class__.__name__}-{title}")

        # Get the widget running
        self.widget = WavePlotWidget()
        self.dockableWidget = WavePlotDockWidget(title, self.widget)
