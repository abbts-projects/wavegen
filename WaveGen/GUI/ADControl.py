# Native imports
from typing import Optional, List, Tuple, Dict
import logging
from enum import Enum, auto

# 3rd Party
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from AMASignal import WaveForms
import qtawesome as qta
import dwf

# Custom
from WaveGen.GUI.ADControlRecChannel import ADControlRecChannelController
from WaveGen.GUI.ADControlOutput import ADControlOutputController
from WaveGen.GUI.ADControlRecording import ADControlRecordingController
from WaveGen.GUI.ADControlTrigger import ADControlTriggerController
from WaveGen.GUI.ADControlInformation import ADControlInformationController
from WaveGen.Helpers.translation import _
from WaveGen import configuration

from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.ADConfigurationTable import ADConfigurationTable


class ADControlWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        # Create a new layout
        layout = QtW.QVBoxLayout()

        # Create the tab widget
        self.tabWidget = QtW.QTabWidget()
        layout.addWidget(self.tabWidget)

        # set the layout
        self.setLayout(layout)


class ADControlController(QtC.QObject):
    def __init__(
        self,
        mainWindow: QtW.QMainWindow,
        mainConfig: "MainConfiguration",
        historyController: "MeasurementHistoryController",
    ):
        super().__init__()

        self.logger = logging.getLogger(self.__class__.__name__)

        # New db session
        self.DBSession = DBConnection.getSession()
        self.currentHistoryID: Optional[int] = None

        # Load the data from the DB
        self.currentADConfig: Optional[ADConfigurationTable] = None

        # Create a waveforms instance
        configuration.waveforms = WaveForms()

        # Init the view
        self.widget = ADControlWidget()

        # Save the params
        self.mainWindow = mainWindow
        self.mainConfig = mainConfig
        self.historyController = historyController

        # Init all the controllers
        self.channelController = ADControlRecChannelController(ADControl=self)
        self.outputController = ADControlOutputController(ADControl=self)
        self.recordingController = ADControlRecordingController(
            ADControl=self,
            mainConfig=mainConfig,
            historyController=historyController
        )
        self.triggerController = ADControlTriggerController(ADControl=self)
        self.informationController = ADControlInformationController(ADControl=self)

        # add them to the tab widget
        self.widget.tabWidget.addTab(
            self.channelController.widget,
            qta.icon("mdi.radio", options=[{"color": "white"}]),
            "",
        )
        self.widget.tabWidget.addTab(
            self.recordingController.widget,
            qta.icon("mdi.record-circle", options=[{"color": "white"}]),
            "",
        )
        self.widget.tabWidget.addTab(
            self.outputController.widget,
            qta.icon("mdi.application-export", options=[{"color": "white"}]),
            "",
        )
        self.widget.tabWidget.addTab(
            self.triggerController.widget,
            qta.icon("mdi.mine", options=[{"color": "white"}]),
            "",
        )
        self.widget.tabWidget.addTab(
            self.informationController.widget,
            qta.icon("mdi.information-outline", options=[{"color": "white"}]),
            "",
        )

        self.connect()

    def connect(self):
        self.channelController.valuesChangedSignal.connect(
            self.recordingController.plotMeasurements
        )
        self.mainConfig.signalDeviceConnectionChanged.connect(
            self.informationController.connectionStateChanged
        )

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        # Tell the others
        self.channelController.projectAboutToClose()
        self.outputController.projectAboutToClose()
        self.recordingController.projectAboutToClose()
        self.triggerController.projectAboutToClose()
        self.informationController.projectAboutToClose()

        # Save everything
        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        self.channelController.saveProject()
        self.outputController.saveProject()
        self.recordingController.saveProject()
        self.triggerController.saveProject()
        self.informationController.saveProject()

        # Commit
        self.DBSession.commit()

    def databaseChanged(self):
        self.logger.info("Updating DB")

        # Close the old one, open a new one
        self.DBSession.commit()
        self.DBSession.close()

        self.DBSession = DBConnection.getSession()

        # Get the first one
        self.currentADConfig = self.DBSession.query(ADConfigurationTable).first()

        if not self.currentADConfig:
            # Set a default config
            self.currentADConfig = ADConfigurationTable(
                recordingMethod=configuration.RecordingMethods.SingleShot,
                recordingSamples=8192,
                recordingFrequency=5e3,
                outputSynchronizationEnabled=True,
                outputSynchronizationMaster=configuration.ADOutputChannels.W1,
                outputSynchronizationSlave=configuration.ADOutputChannels.W2,
                triggerEnable=True,
                triggerChannel=configuration.ADInputChannels.CH1,
                triggerLevel=0.5,
                triggerType=dwf.DwfAnalogIn.TRIGTYPE.EDGE,
                triggerCondition=dwf.DwfAnalogIn.TRIGCOND.RISING_POSITIVE,
                triggerSource=dwf.DwfAnalogIn.TRIGSRC.DETECTOR_ANALOG_IN,
                xyXCH=configuration.ADInputChannels.CH1,
                xyYCH=configuration.ADInputChannels.CH2,
                fftWindow=configuration.Windows.none,
                fftLimit=500,
                measurementID=None,
            )

            # Save
            self.DBSession.add(self.currentADConfig)
            self.DBSession.commit()

        # Update the rec channel and output
        self.outputController.databaseChanged()
        self.channelController.databaseChanged()
        self.recordingController.databaseChanged()
        self.triggerController.databaseChanged()

        # Update
        self.updateGUIValues()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Load it
        self.currentHistoryID = currentID
        self.currentADConfig = (
            self.DBSession.query(ADConfigurationTable)
            .filter_by(measurementID=currentID)
            .first()
        )

        # Inform the others
        self.recordingController.historySelectionChanged(currentID)
        self.outputController.historySelectionChanged(currentID)
        self.channelController.historySelectionChanged(currentID)
        self.triggerController.historySelectionChanged(currentID)

        # update
        self.updateGUIValues()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Get a new copy of the config
        cfg = self.DBSession.query(ADConfigurationTable).get(self.currentADConfig.id)

        # Create a copy
        DBConnection.copyAndAdd(self.DBSession, cfg)

        # Set the ID
        cfg.measurementID = newID

        # Commit to the changes
        self.DBSession.commit()

        # Inform the others
        self.outputController.historyCopySetup(newID)
        self.channelController.historyCopySetup(newID)
        self.recordingController.historyCopySetup(newID)
        self.triggerController.historyCopySetup(newID)

    def updateGUIValues(self):
        self.logger.info("Reloading GUI values")

        # Reload
        self.recordingController.updateGUIValues()
        self.triggerController.updateGUIValues()
        self.informationController.updateGUIValues()

        # The following are already done by the db update
        # self.channelController.updateGUIValues()
        # self.outputController.updateGUIValues()

    def addMeasuredSignalPlotToWindow(self, window):
        self.logger.info("Adding measured plot to window")

        # Measured Signal
        window.addDockWidget(
            QtC.Qt.LeftDockWidgetArea,
            self.recordingController.measuredSignalPlotController.dockableWidget,
        )

    def addControlWidgetToToolbar(self, toolbar: QtW.QToolBar):
        self.logger.info("Adding control widget to toolbar")

        # Add it to the toolbar
        toolbar.addWidget(self.widget)

    def copyConfigurationToMeasurementID(self, measID: int):
        self.logger.info(f"Copying configuration to measurement id {measID}")
        # TODO
