# Native imports
import sys
import logging
from typing import List, Optional
from datetime import datetime
import threading
import time

# 3rd party imports
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import WaveForms
import qtawesome as qta

# Custom
from WaveGen.GUI.ADControl import ADControlController
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.configurationTable import ConfigurationTable
from WaveGen.Helpers.SBThread import SBThread
from WaveGen.Helpers.translation import _
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import configuration
from WaveGen import Models


class MainConfigurationWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setObjectName(self.__class__.__name__)

        # Start a new layout
        self.layout = QtW.QHBoxLayout()

        # Device
        self.deviceLabel = QtW.QLabel(_("mainConfiguration.Device_label"))
        self.deviceComboBox = QtW.QComboBox()
        self.deviceComboBox.setFixedWidth(150)
        self.layout.addWidget(self.deviceLabel)
        self.layout.addWidget(self.deviceComboBox)

        self.connectIcon = qta.icon("mdi.power-plug", options=[{"color": "white"}])
        self.disconnectIcon = qta.icon(
            "mdi.power-plug-off", options=[{"color": "white"}]
        )
        self.connectDeviceButton = QtW.QPushButton(self.connectIcon, "")
        self.layout.addWidget(self.connectDeviceButton)

        refreshIcon = qta.icon("mdi.refresh", options=[{"color": "white"}])
        self.refreshDevicesButton = QtW.QPushButton(refreshIcon, "")
        self.layout.addWidget(self.refreshDevicesButton)

        self.layout.addWidget(QtH.QVLine(colour="darkgray"))

        # Samples
        self.samplesLabel = QtW.QLabel(_("mainConfiguration.Samples_label"))
        self.samplesSpinBox = QtH.QBaseNSpinBox(2)
        self.samplesSpinBox.setRange(2, 2 ** 13)  # TODO: FDwfDigitalOutDataInfo
        self.samplesSpinBox.setAccelerated(True)

        self.layout.addWidget(self.samplesLabel)
        self.layout.addWidget(self.samplesSpinBox)

        # Amplitude
        self.amplitudeLabel = QtW.QLabel(_("mainConfiguration.Amplitude_label"))
        self.amplitudeSpinBox = QtW.QDoubleSpinBox()
        self.amplitudeSpinBox.setRange(-20, +20)
        self.amplitudeSpinBox.setSingleStep(0.1)
        self.amplitudeSpinBox.setSuffix("V")
        self.amplitudeSpinBox.setAccelerated(True)

        self.layout.addWidget(self.amplitudeLabel)
        self.layout.addWidget(self.amplitudeSpinBox)

        # Offset
        self.offsetLabel = QtW.QLabel(_("mainConfiguration.Offset_label"))
        self.offsetSpinBox = QtW.QDoubleSpinBox()
        self.offsetSpinBox.setRange(-20, +20)
        self.offsetSpinBox.setSingleStep(0.1)
        self.offsetSpinBox.setSuffix("V")
        self.offsetSpinBox.setAccelerated(True)

        self.layout.addWidget(self.offsetLabel)
        self.layout.addWidget(self.offsetSpinBox)

        # Periods
        self.periodsLabel = QtW.QLabel(_("mainConfiguration.Periods_label"))
        self.periodsSpinBox = QtW.QDoubleSpinBox()
        self.periodsSpinBox.setRange(1, 100)
        self.periodsSpinBox.setAccelerated(True)

        self.layout.addWidget(self.periodsLabel)
        self.layout.addWidget(self.periodsSpinBox)

        # Individual
        self.individualLabel = QtW.QLabel(_("mainConfiguration.Individual_label"))
        self.individualCheckBox = QtW.QCheckBox()

        self.layout.addWidget(self.individualLabel)
        self.layout.addWidget(self.individualCheckBox)

        # Clip signal
        self.clipVoltageLabel = QtW.QLabel(_("mainConfiguration.ClipVoltage_label"))
        self.clipVoltageCheckBox = QtW.QCheckBox()
        self.clipVoltageCheckBox.setChecked(True)

        self.layout.addWidget(self.clipVoltageLabel)
        self.layout.addWidget(self.clipVoltageCheckBox)

        self.layout.addWidget(QtH.QSpacer())

        # Start playing
        self.recordIcon = qta.icon("mdi.record", options=[{"color": "white"}])
        self.stopRecordingIcon = qta.icon(
            "mdi.stop-circle-outline", options=[{"color": "white"}]
        )
        self.recordSignalButton = QtW.QPushButton(
            self.recordIcon, _("mainConfiguration.RecordSignal")
        )
        self.recordSignalButton.setEnabled(False)
        self.layout.addWidget(self.recordSignalButton)

        # Set the layout
        self.setLayout(self.layout)


class MainConfigurationController(QtC.QObject):
    signalDeviceChanged = QtC.pyqtSignal(object)  # the new device
    signalDeviceConnectionChanged = QtC.pyqtSignal()
    signalStartRecording = QtC.pyqtSignal()
    signalStopRecording = QtC.pyqtSignal()
    signalValuesChanged = QtC.pyqtSignal()

    def __init__(self):
        super().__init__()

        # Get a logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # get the DB session
        self.DBSession = DBConnection.getSession()

        # Prepare
        self.connectDeviceThread: threading.Thread = None

        # Init the widget, start connecting
        self.widget = MainConfigurationWidget()
        self.devicesModel = Models.DeviceComboBoxModel()
        self.widget.deviceComboBox.setModel(self.devicesModel)

        # Prepare
        self.currentDevice: Optional[WaveForms.Device] = None
        self.currentConfiguration: Optional[ConfigurationTable] = None
        self.currentlyUpdatingUI = False
        self.currentHistoryID: Optional[int] = None

        # Connect
        self.connect()

        self.widget.showEvent = self.viewShowing

    def viewShowing(self, event):
        # Update the list
        self.updateDeviceList()

    def connect(self):
        self.widget.refreshDevicesButton.clicked.connect(self.updateDeviceList)
        self.widget.deviceComboBox.currentIndexChanged.connect(
            self.deviceSelectionChanged
        )
        self.widget.connectDeviceButton.clicked.connect(self.connectButtonClicked)
        self.widget.recordSignalButton.clicked.connect(self.recordButtonClicked)

        self.widget.individualCheckBox.stateChanged.connect(self.inputsChanged)
        self.widget.clipVoltageCheckBox.stateChanged.connect(self.inputsChanged)
        self.widget.samplesSpinBox.valueChanged.connect(self.sampleCountChanged)
        self.widget.amplitudeSpinBox.valueChanged.connect(self.inputsChanged)
        self.widget.offsetSpinBox.valueChanged.connect(self.inputsChanged)
        self.widget.periodsSpinBox.valueChanged.connect(self.inputsChanged)

    def databaseChanged(self):
        self.logger.info("Reloading database")

        # Close the old one, open a new one
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # Get the first row!
        self.currentConfiguration = self.DBSession.query(ConfigurationTable).first()

        # Check if we have an entry
        if self.currentConfiguration is None:
            # Create a new one
            self.currentConfiguration = ConfigurationTable(
                sampleCount=4094,
                amplitude=1.0,
                offset=0.0,
                periods=2.0,
                individual=False,
                creationDate=datetime.now(),
                version=configuration.version,
                lastDeviceSN="",
                dwfVersion="",
                windowSaveState=None,
            )

            # Save
            self.DBSession.add(self.currentConfiguration)
            self.DBSession.commit()

        # Update
        self.updateUIFromConfiguration()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Get the new item
        self.currentHistoryID = currentID
        self.currentConfiguration = (
            self.DBSession.query(ConfigurationTable)
            .filter_by(measurementID=currentID)
            .first()
        )

        # Update
        self.updateUIFromConfiguration()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Get a new copy of the config
        cfg = self.DBSession.query(ConfigurationTable).get(self.currentConfiguration.id)

        # Create a copy
        DBConnection.copyAndAdd(self.DBSession, cfg)

        # Set the ID
        cfg.measurementID = newID

        # Commit to the changes
        self.DBSession.commit()

    def updateUIFromConfiguration(self):
        self.logger.info("Updating GUI values from configuration")

        if self.currentConfiguration is None:
            self.logger.info("No valid config found!")
            return

        # Update the UI
        self.currentlyUpdatingUI = True
        self.widget.samplesSpinBox.setValue(self.currentConfiguration.sampleCount)
        self.widget.amplitudeSpinBox.setValue(self.currentConfiguration.amplitude)
        self.widget.offsetSpinBox.setValue(self.currentConfiguration.offset)
        self.widget.periodsSpinBox.setValue(self.currentConfiguration.periods)
        self.widget.individualCheckBox.setChecked(self.currentConfiguration.individual)
        self.currentlyUpdatingUI = False

    def sampleCountChanged(self):
        self.logger.info("Sample count changed")

        # Make sure, that we increase in 2^n steps!

        # inputs changed
        self.inputsChanged()

    def inputsChanged(self):
        self.logger.info("Inputs changed!")

        if self.currentlyUpdatingUI:
            self.logger.info("Inputs changed due to automated updates!")
            return

        # Update the DB entries
        if self.currentConfiguration is not None:
            self.currentConfiguration.sampleCount = self.widget.samplesSpinBox.value()
            self.currentConfiguration.amplitude = self.widget.amplitudeSpinBox.value()
            self.currentConfiguration.offset = self.widget.offsetSpinBox.value()
            self.currentConfiguration.periods = self.widget.periodsSpinBox.value()
            self.currentConfiguration.individual = (
                self.widget.individualCheckBox.isChecked()
            )
            self.currentConfiguration.clipVoltage = (
                self.widget.clipVoltageCheckBox.isChecked()
            )
            self.DBSession.commit()
        else:
            self.logger.info("Could not update configuration, as it does not exist!")

        # inform
        self.signalValuesChanged.emit()

    def updateDeviceList(self):
        self.logger.info("Updating AD device list")

        # Create a new Waveforms instance
        devices = configuration.waveforms.listAllDevices()

        # Set it
        self.devicesModel.devices = devices
        self.devicesModel.layoutChanged.emit()

        # Select it
        if self.widget.deviceComboBox.currentIndex() == -1 and len(devices) > 0:
            self.widget.deviceComboBox.setCurrentIndex(0)

        self.logger.info("Completed updating AD device list")

    def deviceSelectionChanged(self):
        # Get the index
        index = self.widget.deviceComboBox.currentIndex()

        self.logger.info(f"Device selection changed, index: {index}")

        # Check if something is selected
        if index > -1:
            # Get the device
            self.currentDevice = self.devicesModel.devices[index]

            # Emit a signal
            self.logger.info(f"Device selection changed, device: {self.currentDevice}")
            self.signalDeviceChanged.emit(self.currentDevice)

    def connectButtonClicked(self):
        self.logger.info("Connect to device button clicked")

        # connect
        self.connectDeviceThread = threading.Thread(target=self.connectCurrentDevice)
        self.connectDeviceThread.start()

    def connectCurrentDevice(self):
        if configuration.waveforms.device:
            # Disconnect
            self.logger.info("Trying to disconnect from AD device")

            # Reset
            configuration.waveforms.device = None

            # Change the button, Disable recording
            SBThread.runOnMainThread(
                target=lambda: (
                    self.widget.connectDeviceButton.setIcon(self.widget.connectIcon),
                    self.widget.recordSignalButton.setEnabled(False),
                )
            )

        else:
            if self.currentDevice is None:
                self.logger.info(f"No device has been selected!")
                return

            # Connect
            self.logger.info(f"Trying to connect to AD device {self.currentDevice}")

            try:
                configuration.waveforms.device = self.currentDevice

            except WaveForms.DWFError as e:
                self.logger.info(f"Failed to connect to AD: {e}")

                # inform the user
                SBThread.runOnMainThread(
                    target=QtW.QMessageBox.warning,
                    args=(
                        self.widget,
                        _("mainConfiguration.ADConnectionFailedTitle"),
                        _("mainConfiguration.ADConnectionFailedBody").format(
                            self.currentDevice.serial, e
                        ),
                        QtW.QMessageBox.Ok,
                    ),
                )
                return

            # Change it in the DB
            self.currentConfiguration.lastDeviceSN = self.currentDevice.serial
            self.currentConfiguration.dwfVersion = configuration.waveforms.getVersion()
            self.logger.info(
                f"Current dwf version: {self.currentConfiguration.dwfVersion}"
            )

            # Commit on the main thread, update ui
            SBThread.runOnMainThread(
                target=lambda: (
                    self.DBSession.commit,
                    self.widget.connectDeviceButton.setIcon(self.widget.disconnectIcon),
                    self.widget.recordSignalButton.setEnabled(True),
                )
            )

        # Signal
        self.signalDeviceConnectionChanged.emit()

    def recordButtonClicked(self):
        self.logger.info("Recording button clicked")

        # Start recording
        self.signalStartRecording.emit()

    def setWindowSaveState(self, newState: str):
        """
        Save the window state
        
        Parameters
        ----------
        newState : str
            The base64 encoded window state
        """
        self.logger.info("Saving window state...")

        self.currentConfiguration.windowSaveState = newState
        self.DBSession.commit()

    def getWindowSaveState(self) -> str:
        self.logger.info("getting window state...")

        return self.currentConfiguration.windowSaveState

    def setRecordingIcon(self, isRecording: bool):
        self.logger.info("Updating recording icon")

        # Get the icon
        icn = self.widget.stopRecordingIcon if isRecording else self.widget.recordIcon

        # Set it
        self.widget.recordSignalButton.setIcon(icn)
