# Native imports
import sys
from typing import Optional, List, Tuple, cast
import logging
import os
from datetime import datetime
from enum import Enum, auto
import re
import threading

# 3rd Party
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import pyqtgraph as QtG
from AMASignal import WaveForms, Generator, Signal
import qtawesome as qta
from sqlalchemy.orm import Session
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Custom
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import configuration
from WaveGen.Helpers.translation import _
from WaveGen import Models
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.DB.measurementHistoryTable import MeasurementHistoryTable, MeasurementTable
from WaveGen.DB.configurationTable import ConfigurationTable
from WaveGen.DB.ADConfigurationTable import ADConfigurationTable
from WaveGen.Helpers.SBThread import SBThread


class MeasurementExportWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        # Start a new layout
        layout = QtW.QVBoxLayout()
        self.setLayout(layout)

        # Save Path
        saveLayout = QtW.QGridLayout()
        layout.addLayout(saveLayout)

        self.savePathLineEdit = QtW.QLineEdit()

        self.changeFolderButton = QtW.QPushButton("...")
        self.changeFolderButton.setMaximumWidth(20)

        outFolderLabel = QtW.QLabel(_("measurementExporter.OutputFolder"))
        outFolderLabel.setFixedHeight(10)

        saveLayout.addWidget(outFolderLabel, 0, 0)
        saveLayout.addWidget(self.savePathLineEdit, 1, 0)
        saveLayout.addWidget(self.changeFolderButton, 1, 1)

        # Export options for CSV
        csvGroupBox = QtW.QGroupBox(_("measurementExporter.Excel"))
        csvLayout = QtW.QFormLayout()
        csvGroupBox.setLayout(csvLayout)
        layout.addWidget(csvGroupBox)

        # Create the Check boxes for CSV
        self.CSVGeneratedCheckBox = QtW.QCheckBox()
        self.CSVMeasurementCheckBox = QtW.QCheckBox()
        self.CSVFFTCheckBox = QtW.QCheckBox()

        # Set checked
        self.CSVGeneratedCheckBox.setChecked(True)
        self.CSVMeasurementCheckBox.setChecked(True)
        self.CSVFFTCheckBox.setChecked(True)

        # Add them
        csvLayout.addRow(
            QtW.QLabel(_("measurementExporter.Generated")),
            self.CSVGeneratedCheckBox
        )
        csvLayout.addRow(
            QtW.QLabel(_("measurementExporter.Measurements")),
            self.CSVMeasurementCheckBox,
        )
        csvLayout.addRow(QtW.QLabel(_("measurementExporter.FFT")), self.CSVFFTCheckBox)

        # Export options for Plots
        plotGroupBox = QtW.QGroupBox(_("measurementExporter.Plotting"))
        plotLayout = QtW.QFormLayout()
        plotGroupBox.setLayout(plotLayout)
        layout.addWidget(plotGroupBox)

        # Create the Check boxes for the plots
        self.plotGeneratedCheckBox = QtW.QCheckBox()
        self.plotMeasurementCheckBox = QtW.QCheckBox()
        self.plotFFTCheckBox = QtW.QCheckBox()
        self.plotXYCheckBox = QtW.QCheckBox()

        # Set the state
        self.plotGeneratedCheckBox.setChecked(True)
        self.plotMeasurementCheckBox.setChecked(True)
        self.plotFFTCheckBox.setChecked(True)
        self.plotXYCheckBox.setChecked(True)

        # Add to the UI
        plotLayout.addRow(
            QtW.QLabel(_("measurementExporter.Generated")),
            self.plotGeneratedCheckBox
        )
        plotLayout.addRow(
            QtW.QLabel(_("measurementExporter.Measurements")),
            self.plotMeasurementCheckBox,
        )
        plotLayout.addRow(
            QtW.QLabel(_("measurementExporter.FFT")), self.plotFFTCheckBox
        )
        plotLayout.addRow(QtW.QLabel(_("measurementExporter.XY")), self.plotXYCheckBox)

        # Export and cancel
        actionLayout = QtW.QHBoxLayout()
        layout.addWidget(QtH.QSpacer())
        layout.addLayout(actionLayout)

        self.cancelButton = QtW.QPushButton(_("measurementExporter.Cancel"))
        self.exportButton = QtW.QPushButton(_("measurementExporter.Export"))

        actionLayout.addWidget(QtH.QSpacer())
        actionLayout.addWidget(self.cancelButton)
        actionLayout.addWidget(self.exportButton)


class MeasurementExportMainWindow(QtW.QMainWindow):
    def __init__(self, waveConfig: MeasurementExportWidget, parent=None):
        super().__init__(parent)

        self.setObjectName(self.__class__.__name__)

        # Setup of the view
        self.layout = QtW.QHBoxLayout()
        self.wid = QtW.QWidget(self)
        self.setCentralWidget(self.wid)
        self.wid.setLayout(self.layout)

        # Add the widget
        self.layout.addWidget(waveConfig)

        # Configure the window
        self.setWindowTitle(_("measurementExporter.title"))
        self.resize(400, 450)


class MeasurementExportController(QtC.QObject):
    windowClosingSignal = QtC.pyqtSignal()

    def __init__(
        self,
        measurementID: int,
        ADControl: "ADControlController",
        parent: QtW.QWidget
    ):
        super().__init__()

        # Get a logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # prepare
        self.updatingWaveConfiguration = False

        # Set the current ID
        self.measurementID = measurementID
        self.ADControl = ADControl

        # Create a new DB session
        self.DBSession = DBConnection.getSession()

        # Set the properties
        self.measurementEntry = self.DBSession.query(MeasurementHistoryTable).get(measurementID)

        # Create the widgets
        self.configurationsWidget = MeasurementExportWidget()

        # Get the main window
        self.measExporter = MeasurementExportMainWindow(
            self.configurationsWidget, parent=parent
        )

        # Forward the event
        self.measExporter.closeEvent = self.windowClosing

        # Set the current path
        if configuration.projectPath is not None:
            # Set it
            self.configurationsWidget.savePathLineEdit.setText(
                os.path.dirname(configuration.projectPath)
            )

        self.connect()

    def windowClosing(self, event: QtC.QEvent):
        self.logger.info("Window closing")

        # Forward
        self.windowClosingSignal.emit()

    def connect(self):
        cw = self.configurationsWidget

        # Clicks
        cw.cancelButton.clicked.connect(self.cancel)
        cw.exportButton.clicked.connect(self.exportData)
        cw.changeFolderButton.clicked.connect(self.choosePath)

        # Event filtering
        cw.savePathLineEdit.installEventFilter(self)
        cw.changeFolderButton.installEventFilter(self)
        cw.CSVGeneratedCheckBox.installEventFilter(self)
        cw.CSVMeasurementCheckBox.installEventFilter(self)
        cw.CSVFFTCheckBox.installEventFilter(self)
        cw.plotGeneratedCheckBox.installEventFilter(self)
        cw.plotMeasurementCheckBox.installEventFilter(self)
        cw.plotFFTCheckBox.installEventFilter(self)
        cw.plotXYCheckBox.installEventFilter(self)
        cw.cancelButton.installEventFilter(self)
        cw.exportButton.installEventFilter(self)

    def eventFilter(self, obj: QtC.QObject, event: QtC.QEvent):
        if event.type() == QtC.QEvent.KeyPress:
            # Change the type
            event = cast(Qt.QKeyEvent, event)

            # Check if we pressed the return key
            if event.key() == QtC.Qt.Key_Enter or event.key() == QtC.Qt.Key_Return:
                # Check if cancel or apply
                if obj == self.configurationsWidget.cancelButton:
                    self.cancel()

                elif obj == self.configurationsWidget.exportButton:
                    self.exportData()

                elif obj == self.configurationsWidget.changeFolderButton:
                    self.choosePath()

                else:
                    # Submit!
                    self.exportData()

                # Done
                return True
        # Not us
        return False

    def choosePath(self):
        # Get the path
        path = Qt.QFileDialog.getExistingDirectory(
            self.measExporter,
            _("measurementExporter.folderSelection"),
            self.configurationsWidget.savePathLineEdit.text()
        )

        # Check if we have a path
        if len(path) > 0:
            self.configurationsWidget.savePathLineEdit.setText(path)

    def exportData(self):
        self.logger.info("Export Data")

        # Check, if we have a valid path
        path = self.configurationsWidget.savePathLineEdit.text()

        # Check length
        if len(path) == 0:
            QtW.QMessageBox.warning(
                self.measExporter,
                _("measurementExporter.invalidPath"),
                _("measurementExporter.invalidPathBody"),
                QtW.QMessageBox.Ok,
            )
            return

        # Disable the buttons
        self.configurationsWidget.cancelButton.setEnabled(False)
        self.configurationsWidget.exportButton.setEnabled(False)

        # Show an export window
        self.lw = QtH.LoaderWindow()
        self.lw.show()

        # Hide the window
        self.measExporter.close()

        # Start a new thread
        self.exportThread = threading.Thread(target=self.doExport, args=(path,))
        self.exportThread.start()

    def doExport(self, path:str):
        # Now, what options are checked
        CSVGenerated, CSVMeasurement, CSVFFT =  SBThread.runOnMainThread(target=lambda:(
            self.configurationsWidget.CSVGeneratedCheckBox.isChecked(),
            self.configurationsWidget.CSVMeasurementCheckBox.isChecked(),
            self.configurationsWidget.CSVFFTCheckBox.isChecked(),
        ), blocking=True)

        plotGenerated, plotMeasurement, plotFFT, plotXY =  SBThread.runOnMainThread(target=lambda:(
            self.configurationsWidget.plotGeneratedCheckBox.isChecked(),
            self.configurationsWidget.plotMeasurementCheckBox.isChecked(),
            self.configurationsWidget.plotFFTCheckBox.isChecked(),
            self.configurationsWidget.plotXYCheckBox.isChecked(),
        ), blocking=True)

        # Shortcut
        recCtrl = self.ADControl.recordingController

        # Fetch some data
        signalData = SBThread.runOnMainThread(target=recCtrl.calculateSignal, blocking=True)
        measuredData = recCtrl.adjustedSignals

        entryID = SBThread.runOnMainThread(target=lambda: self.measurementEntry.id, blocking=True)

        # Get the name
        name = _("measurementExporter.Untitled")
        if entryID > 1:
            name = SBThread.runOnMainThread(target=lambda: self.measurementEntry.name, blocking=True)
            
            name = re.sub(r'[\\/*?:"<>|]', "", name)

        # Create a plots folder
        plotFolderName = f"{name}_{datetime.now().strftime('%Y-%m-%d')}"
        saveFolder = os.path.join(path, plotFolderName)
        os.makedirs(saveFolder, exist_ok=True)

        # Init an excel writer
        writer = pd.ExcelWriter(
            os.path.join(saveFolder, "signals.xlsx"),
            engine='xlsxwriter'
        )

        # CSV
        if CSVGenerated:
            self.logger.info("Exporting CSV of generated signals")

            # Generate a DF and save it
            signalDf = pd.DataFrame()
            if len(signalData) > 0:
                
                # Convert to CH number
                saveDict = {}
                for ch, signal in signalData.items():
                    saveDict[f"Time [s]"] = signal.time
                    saveDict[f"CH {ch.value} [V]"] = signal.signal

                signalDf = pd.DataFrame(saveDict)

            signalDf.to_excel(writer, sheet_name=_("measurementExporter.Generated"))

        if CSVMeasurement:
            self.logger.info("Exporting CSV of measurement")

            # Generate a DF and save it
            signalDf = pd.DataFrame()
            if len(measuredData) > 0:
                # Convert to CH number
                saveDict = {}
                for ch, signal in measuredData.items():
                    saveDict[f"Time [s]"] = signal.time
                    saveDict[f"CH {ch.value} [V]"] = signal.signal

                signalDf = pd.DataFrame(saveDict)

            signalDf.to_excel(writer, sheet_name=_("measurementExporter.Measurements"))

        if CSVFFT:
            self.logger.info("Exporting CSV of FFT")

            # Calc and get signals
            SBThread.runOnMainThread(
                target=recCtrl.FFTPlotController.plotFromSignals,
                kwargs={
                    "signals": measuredData,
                    "clearUnused": False
                },
                blocking=True
            )
            fftSigs = recCtrl.FFTPlotController.fftSignals

            # Generate a DF and save it
            signalDf = pd.DataFrame()
            if len(fftSigs) > 0:
                # Convert to CH number
                saveDict = {}
                for ch, signal in fftSigs.items():
                    saveDict[f"Frequency [Hz]"] = signal.time
                    saveDict[f"Amplitude CH {ch.value} [V]"] = signal.signal

                signalDf = pd.DataFrame(saveDict)

            signalDf.to_excel(writer, sheet_name=_("measurementExporter.FFT"))

        # Save the excel
        writer.save()

        # Plotting
        if plotGenerated:
            self.logger.info("Plotting Generated signals")

            # Create a new plot
            fig, ax = plt.subplots(1, 1, figsize=(15, 8))

            # Plot each signal
            for ch, signal in signalData.items():
                sns.lineplot(x=signal.time, y=signal.signal, ax=ax, label=ch.name);
            
            fig.suptitle(_("measurementExporter.Generated"), fontsize=20)
            ax.set_ylabel(_("plotting.voltage_axis"))
            ax.set_xlabel(_("plotting.time_axis"))
            ax.legend()
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])

            # Save it
            fig.savefig(os.path.join(saveFolder, f"{_('measurementExporter.Generated')}.jpg"), dpi=200)

        if plotMeasurement:
            self.logger.info("Plotting measurement")

            # Create a new plot
            fig, ax = plt.subplots(1, 1, figsize=(15, 8))

            # Plot each signal
            for ch, signal in measuredData.items():
                sns.lineplot(x=signal.time, y=signal.signal, ax=ax, label=ch.name);
            
            fig.suptitle(_("measurementExporter.Measurements"), fontsize=20)
            ax.set_ylabel(_("plotting.voltage_axis"))
            ax.set_xlabel(_("plotting.time_axis"))
            ax.legend()
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])

            # Save it
            fig.savefig(os.path.join(saveFolder, f"{_('measurementExporter.Measurements')}.jpg"), dpi=200)

        if plotFFT:
            self.logger.info("Plotting FFT")

            # Calc and get signals
            SBThread.runOnMainThread(
                target=recCtrl.FFTPlotController.plotFromSignals,
                kwargs={
                    "signals": measuredData,
                    "clearUnused": False
                },
                blocking=True
            )
            fftSigs = recCtrl.FFTPlotController.fftSignals

            # Create a new plot
            fig, ax = plt.subplots(1, 1, figsize=(15, 8))

            # Plot each signal
            for ch, signal in fftSigs.items():
                sns.lineplot(x=signal.time, y=signal.signal, ax=ax, label=ch.name);
            
            fig.suptitle(_("measurementExporter.FFT"), fontsize=20)
            ax.set_ylabel(_("plotting.voltage_axis"))
            ax.set_xlabel(_("plotting.frequency_axis"))
            ax.legend()
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])

            # Save it
            fig.savefig(os.path.join(saveFolder, f"{_('measurementExporter.FFT')}.jpg"), dpi=200)

        if plotXY:
            self.logger.info("Plotting XY")

            # Calc and get signals
            signals = list(measuredData.values())
            if len(signals) < 2:
                self.logger.warning("Can't XY plot, not enough signals!")

            else:
                recCtrl.XYPlotController.plotFromSignals(signal1=signals[0].signal, signal2=signals[1].signal)
                XYSig = recCtrl.XYPlotController.XYSignal

                # Create a new plot
                fig, ax = plt.subplots(1, 1, figsize=(15, 8))

                # Plot each signal
                sns.scatterplot(x=XYSig.time, y=XYSig.signal, ax=ax);
                
                fig.suptitle(_("measurementExporter.XY"), fontsize=20)
                ax.set_ylabel(_("plotting.voltage_axis"))
                ax.set_xlabel(_("plotting.voltage_axis"))
                fig.tight_layout(rect=[0, 0.03, 1, 0.95])

                # Save it
                fig.savefig(os.path.join(saveFolder, f"{_('measurementExporter.XY')}.jpg"), dpi=200)
        
        # Close the window
        SBThread.runOnMainThread(target=lambda:(
            #self.measExporter.close(),
            self.lw.close(),
        ))

    def cancel(self):
        self.logger.info("Cancelling")

        # Close
        self.measExporter.close()

    def showWindow(self):
        self.logger.info("Showing exporter window")

        # Update the plot
        self.measExporter.show()
