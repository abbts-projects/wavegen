# Native imports
from typing import Optional, List, Tuple, Dict, Callable, Union
import logging
from enum import Enum, auto

# 3rd Party
import pyqtgraph as QtG
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta
import numpy as np
from AMASignal import Signal, FFT

# Custom
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.Helpers.translation import _
from WaveGen import configuration
from WaveGen import Models
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import Models


class ADFFTPlotDockWidget(QtW.QDockWidget):
    def __init__(self, title: str, widget: QtW.QWidget, parent=None):
        super().__init__(title, parent=parent)

        self.setObjectName(f"{self.__class__.__name__}-{title}")

        # Set the content
        self.setWidget(widget)


class ADFFTPlotWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setObjectName(self.__class__.__name__)

        # New layout
        layout = QtW.QVBoxLayout()
        self.setLayout(layout)

        # Add the setup
        setupLayout = QtW.QHBoxLayout()

        self.FFTWindowComboBox = QtW.QComboBox()
        self.FrequrncyLimitSpinBox = QtW.QDoubleSpinBox()
        self.FrequrncyLimitSpinBox.setRange(0.01, 1e20)
        self.FrequrncyLimitSpinBox.setAccelerated(True)
        self.FrequrncyLimitSpinBox.setSingleStep(100)
        self.FrequrncyLimitSpinBox.setSuffix("Hz")

        setupLayout.addWidget(QtW.QLabel(_("ADControl.FFT_Window")))
        setupLayout.addWidget(self.FFTWindowComboBox)
        setupLayout.addWidget(QtH.QSpacer(5, 5))

        setupLayout.addWidget(QtW.QLabel(_("ADControl.FFT_Limit")))
        setupLayout.addWidget(self.FrequrncyLimitSpinBox)
        setupLayout.addWidget(QtH.QSpacer(height=5))

        layout.addLayout(setupLayout)

        # Add the plot widget
        self.plotWidget = QtG.PlotWidget(None)
        self.plotWidget.showGrid(x=True, y=True, alpha=0.7)
        layout.addWidget(self.plotWidget)


class ADFFTPlotController(QtC.QObject):
    def __init__(self, ADControl: "ADControlController", parent=None):
        super().__init__(parent)

        self.ADControl = ADControl

        self.widget = ADFFTPlotWidget()
        self.dockableWidget = ADFFTPlotDockWidget(
            _("ADControl.FFTPlotTitle"), self.widget
        )

        # New logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # Prepare
        self.linePlots: Dict[int, QtG.PlotItem] = {}
        self.signals: Dict[int, Signal] = {}
        colours = list(configuration.ChannelPlotColours)

        # Loop
        for ch in configuration.ADInputChannels:
            # New plot item
            pi = self.widget.plotWidget.getPlotItem().plot()
            pi.setPen(QtG.mkPen(Qt.QColor(colours[ch.value].value)))

            # Set the plot labels
            self.widget.plotWidget.setLabel("left", _("plotting.voltage_axis"))
            self.widget.plotWidget.setLabel("bottom", _("plotting.frequency_axis"))

            # Save
            self.linePlots[ch.value] = pi

        self.fftSignals: Dict[configuration.ADInputChannels, Signal] = {}

        # Other properties
        self.hidden = True
        self.fft = FFT()

        # Set the combobox model
        self.windowModel = Models.FFTWindowModel()
        self.widget.FFTWindowComboBox.setModel(self.windowModel)

        # Update
        self.windowChanged()

        # Set the limit
        self.currentFreqLimit = 0

        # Connect
        self.connect()

    def updateGUIValues(self):
        self.logger.info("Updating GUI values")

        # Update
        self.setWindow(self.ADControl.currentADConfig.fftWindow)
        self.setFrequency(self.ADControl.currentADConfig.fftLimit)

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        # Save
        self.ADControl.DBSession.commit()

    def connect(self):
        self.widget.FFTWindowComboBox.currentIndexChanged.connect(
            self.windowChanged
        )
        self.widget.FrequrncyLimitSpinBox.valueChanged.connect(
            self.frequencyChanged
        )

    def setHidden(self, hidden: bool):
        self.logger.info("Setting hidden state!")

        # Change and remember
        self.dockableWidget.setHidden(hidden)
        self.hidden = hidden

    def windowChanged(self):
        self.logger.info("Window changed!")

        # Get the index / window
        cIndex = self.widget.FFTWindowComboBox.currentIndex()
        self.fftWindow = list(configuration.Windows)[cIndex]

        self.windowFunction = self.getWindowFunction(self.fftWindow)

        # Save
        if self.ADControl.currentADConfig:
            self.ADControl.currentADConfig.fftWindow = self.fftWindow

        # Re plot
        self.replot()

    def setFrequency(self, frequency: float):
        self.logger.info("Setting new frequency")

        # Update
        self.widget.FrequrncyLimitSpinBox.setValue(frequency)

    def setWindow(self, window: configuration.Windows):
        self.logger.info("Setting new window type")

        # Get the index and set
        index = list(configuration.Windows).index(window)
        self.widget.FFTWindowComboBox.setCurrentIndex(index)

    def getWindowFunction(
        self, window: configuration.Windows
    ) -> Callable[[int], np.ndarray]:
        if window == configuration.Windows.none:
            return np.ones

        elif window == configuration.Windows.hanning:
            return np.hanning

        elif window == configuration.Windows.hamming:
            return np.hamming

        elif window == configuration.Windows.blackman:
            return np.blackman

        elif window == configuration.Windows.bartlett:
            return np.bartlett

        else:
            raise Exception(f"Invalid window function: {window}")

    def frequencyChanged(self):
        self.logger.info("Frequency changed")

        # Get the value
        self.currentFreqLimit = self.widget.FrequrncyLimitSpinBox.value()

        # Save
        if self.ADControl.currentADConfig:
            self.ADControl.currentADConfig.fftLimit = self.currentFreqLimit

        # Re plot
        self.replot()

    def replot(self):
        self.plotFromSignals(signals=self.signals)

    def plotFromSignals(
        self,
        signals: Dict[Union[int, configuration.ADInputChannels], Signal],
        clearUnused=False,
    ):
        # TODO: This should probably be in a new thread?!

        # Save
        self.signals = signals

        if clearUnused:
            # Check if we need to clear any other plots

            for plt in self.linePlots.values():
                plt.clear()
                plt.setData(x=[], y=[])

        # Reset
        self.fftSignals = {}

        # Loop each signal
        for ch, signal in signals.items():
            # Check if we have a AD channel type
            if isinstance(ch, int):
                ch = configuration.ADInputChannels(ch)

            # Calc fft
            fftSignal = self.fft.getConditionedFFT(
                signal,
                window=self.windowFunction,
                fLimit=self.currentFreqLimit
            )

            # Save
            self.fftSignals[ch] = fftSignal

            # plot fft
            self.linePlots[ch.value].clear()
            self.linePlots[ch.value].setData(
                x=fftSignal.time,
                y=fftSignal.signal,
                name=f"CH{ch.value+1}"
            )
