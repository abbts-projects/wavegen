# Native imports
from typing import Optional, List, Tuple, Dict
import logging
from enum import Enum, auto

# 3rd Party
import pyqtgraph as QtG
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta
import numpy as np
from AMASignal import Signal

# Custom
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.Helpers.translation import _
from WaveGen import configuration
from WaveGen import Models
from WaveGen.Helpers import pyqtHelpers as QtH


class ADXYPlotDockWidget(QtW.QDockWidget):
    def __init__(self, title: str, widget: QtW.QWidget, parent=None):
        super().__init__(title, parent=parent)

        self.setObjectName(f"{self.__class__.__name__}-{title}")

        aw = QtH.QAspectRatioWidget(widget)

        # Set the content
        self.setWidget(aw)


class ADXYPlotWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setObjectName(self.__class__.__name__)

        # New layout
        layout = QtW.QVBoxLayout()
        self.setLayout(layout)

        # Add the setup
        setupLayout = QtW.QHBoxLayout()

        self.channelXComboBox = QtW.QComboBox()
        self.channelYComboBox = QtW.QComboBox()

        setupLayout.addWidget(QtW.QLabel(_("ADControl.X_CH")))
        setupLayout.addWidget(self.channelXComboBox)
        setupLayout.addWidget(QtH.QSpacer(5, 5))

        setupLayout.addWidget(QtW.QLabel(_("ADControl.Y_CH")))
        setupLayout.addWidget(self.channelYComboBox)
        setupLayout.addWidget(QtH.QSpacer(height=5))

        layout.addLayout(setupLayout)

        # Add the plot widget
        self.plotWidget = QtG.PlotWidget(None)
        self.plotWidget.showGrid(x=True, y=True, alpha=0.7)
        layout.addWidget(self.plotWidget)

        p = QtW.QSizePolicy(QtW.QSizePolicy.Preferred, QtW.QSizePolicy.Minimum)
        p.setHeightForWidth(True)
        self.plotWidget.setSizePolicy(p)


class ADXYPlotController(QtC.QObject):
    def __init__(self, ADControl: "ADControlController", parent=None):
        super().__init__(parent)

        self.ADControl = ADControl

        self.widget = ADXYPlotWidget()
        self.dockableWidget = ADXYPlotDockWidget(
            _("ADControl.XYPlotTitle"), self.widget
        )

        # New logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # Prepare
        self.scatterPlot = QtG.ScatterPlotItem()
        self.scatterPlot.setPen(None)
        self.scatterPlot.setSize(1.5)
        self.scatterPlot.setBrush(Qt.QColor(configuration.ChannelPlotColours.CH1.value))

        # add the new plot
        self.widget.plotWidget.getPlotItem().addItem(self.scatterPlot)

        # Set the plot labels
        self.widget.plotWidget.setLabel("left", _("plotting.voltage_axis"))
        self.widget.plotWidget.setLabel("bottom", _("plotting.voltage_axis"))

        self.channelsModel = Models.ADInputChannelsModel()

        self.widget.channelXComboBox.setModel(self.channelsModel)
        self.widget.channelYComboBox.setModel(self.channelsModel)

        # Other properties
        self.hidden = True

        # Set the default channels in preparation (overwritten by db!)
        self.xChannel = None
        self.yChannel = None

        # Prep
        self.signal1: Optional[np.ndarray] = None
        self.signal2: Optional[np.ndarray] = None
        self.XYSignal: Optional[Signal] = None

        # Connect
        self.connect()

    def updateGUIValues(self):
        self.logger.info("Updating GUI values")

        if self.ADControl.currentADConfig is None:
            self.logger.info("Na valid AD config found!")
            return

        # Set the XY Plot
        self.setXChannel(self.ADControl.currentADConfig.xyXCH)
        self.setYChannel(self.ADControl.currentADConfig.xyYCH)

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        # Save
        self.ADControl.DBSession.commit()

    def connect(self):
        self.widget.channelXComboBox.currentIndexChanged.connect(self.XChannelChanged)
        self.widget.channelYComboBox.currentIndexChanged.connect(self.YChannelChanged)

    def setHidden(self, hidden: bool):
        self.logger.info("Setting hidden state!")

        # Change and remember
        self.dockableWidget.setHidden(hidden)
        self.hidden = hidden

    def XChannelChanged(self, cIndex: int):
        self.logger.info("X Combo box changed")

        # Set the current x channel
        self.xChannel = self.channelsModel.channels[cIndex]

        # Save
        if self.ADControl.currentADConfig:
            self.ADControl.currentADConfig.xyXCH = self.xChannel

        # plot!
        self.replot()

    def YChannelChanged(self, cIndex: int):
        self.logger.info("Y Combo box changed")

        # Set the current y channel
        self.yChannel = self.channelsModel.channels[cIndex]

        # Save
        if self.ADControl.currentADConfig:
            self.ADControl.currentADConfig.xyYCH = self.yChannel

        # plot!
        self.replot()

    def setXChannel(self, channel: configuration.ADInputChannels):
        self.logger.info("Setting X channel combo box")

        # Get the index
        self.xChannel = channel
        index = self.channelsModel.channels.index(channel)
        self.widget.channelXComboBox.setCurrentIndex(index)

    def setYChannel(self, channel: configuration.ADInputChannels):
        self.logger.info("Setting Y channel combo box")

        # Get the index
        self.yChannel = channel
        index = self.channelsModel.channels.index(channel)
        self.widget.channelYComboBox.setCurrentIndex(index)

    def replot(self):
        self.plotFromSignals(signal1=self.signal1, signal2=self.signal2)

    def clearPlot(self):
        self.scatterPlot.clear()
        self.scatterPlot.setData(x=[], y=[])

    def plotFromSignals(self, signal1: np.ndarray, signal2: np.ndarray):
        if signal1 is None or signal2 is None:
            self.logger.warning("Don't have any signals to plot!")
            return

        # Check if the signals have the same length
        len1 = len(signal1)
        len2 = len(signal2)
        maxPoints = 1024

        # Check
        if len1 == len2:
            # Thin out
            if len1 > maxPoints:
                signal1 = signal1[:: len1 // maxPoints]

            if len2 > maxPoints:
                signal2 = signal2[:: len2 // maxPoints]

            # swapping...
            sigX = sigY = None

            # Save
            self.signal1 = signal1
            self.signal2 = signal2

            # ... X Channel
            if self.xChannel == configuration.ADInputChannels.CH1:
                sigX = signal1
            elif self.xChannel == configuration.ADInputChannels.CH2:
                sigX = signal2
            else:
                self.logger.warning(f"Invalid x channel selected: {self.xChannel}")

            # ... Y Channel
            if self.yChannel == configuration.ADInputChannels.CH1:
                sigY = signal1
            elif self.yChannel == configuration.ADInputChannels.CH2:
                sigY = signal2
            else:
                self.logger.warning(f"Invalid y channel selected: {self.xChannel}")

            # Save
            self.XYSignal = Signal("", sigX, sigY)

            # Clear and plot
            self.scatterPlot.clear()
            self.scatterPlot.setData(x=sigX, y=sigY)
