# Native imports
import sys
import logging
from datetime import datetime
import json
from typing import Optional, List, Dict, Union, cast
import zlib
from datetime import datetime

# 3rd party
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import qtawesome as qta
from AMASignal import Signal
import numpy as np

# Custom
from WaveGen.DB.measurementHistoryTable import MeasurementHistoryTable, MeasurementTable
from WaveGen.DB.configurationTable import ConfigurationTable
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.Helpers.pyqtHelpers import QSpacer, QHLine, QTitleLabel
from WaveGen import configuration
from WaveGen.Helpers.translation import _
from WaveGen import Models
from WaveGen.GUI.measurementExporter import MeasurementExportController


class MeasurementHistoryWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setMinimumWidth(250)

        # Create a layout
        self.layout = QtW.QVBoxLayout()
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

        # Separate
        self.layout.addWidget(QHLine("#222"))
        self.layout.addWidget(QTitleLabel(_("measurementHistory.title"), "#aaa"))

        # Add the list view
        self.historyList = QtW.QListView()
        self.layout.addWidget(self.historyList)

        # Create the toolbar at the bottom
        toolbar = QtW.QToolBar()
        toolbar.setIconSize(QtC.QSize(15, 15))
        self.layout.addWidget(toolbar)

        # Spacer to force everything to the right
        toolbar.addWidget(QSpacer())

        # Create buttons
        exportIcon = qta.icon("mdi.export-variant", options=[{"color": "white"}])
        self.exportAction = QtW.QAction(exportIcon, "", self)
        toolbar.addAction(self.exportAction)
        toolbar.addSeparator()

        trashIcon = qta.icon("mdi.trash-can-outline", options=[{"color": "white"}])
        self.deleteAction = QtW.QAction(trashIcon, "", self)
        toolbar.addAction(self.deleteAction)

        # Set the layout
        self.setLayout(self.layout)


class MeasurementHistoryController(QtC.QObject):
    selectionChangedSignal = QtC.pyqtSignal(object)  # History ID or none
    createCopyForMeasurementSignal = QtC.pyqtSignal(int)  # New history ID

    def __init__(
        self,
        waveConfigController: Optional["WaveConfigurationController"] = None,
        ADControl: Optional["ADControlController"] = None,
    ):
        super().__init__()

        # Get a logger
        self.logger = logging.getLogger(self.__class__.__name__)

        self.DBSession = DBConnection.getSession()
        self.exporter: Optional[MeasurementExportController] = None

        # Set some properties
        self.waveConfigController = waveConfigController
        self.ADControl = ADControl

        # Load a history item, so we can link all to this
        self.currentHistoryItem: Optional[MeasurementHistoryTable] = None

        # Get the widget running
        self.widget = MeasurementHistoryWidget()

        # Connect the signals
        self.connect()

        # Update
        self.historicalMeasurementModel = Models.MeasurementHistoryModel()
        self.widget.historyList.setModel(self.historicalMeasurementModel)

    def addWidgetToToolbar(self, toolbar: QtW.QToolBar):
        toolbar.addWidget(self.widget)

    def connect(self):
        self.widget.exportAction.triggered.connect(self.exportHistoryClicked)
        self.widget.deleteAction.triggered.connect(self.deleteHistoryClicked)
        self.widget.historyList.doubleClicked.connect(self.doubleClickedHistory)

        self.widget.historyList.selectionChanged = self.selectionChanged

    def doubleClickedHistory(self):
        self.logger.info("Double click on history item")

        # Update
        self.editHistoryName()

    def editHistoryName(self):
        self.logger.info("Editing hist entry name")

        # Get the index / ID
        selIndex = self.widget.historyList.currentIndex().row()
        selItem = self.historicalMeasurementModel.history[selIndex]

        if selIndex == 0:
            # Can't edit this one
            self.logger.info("Can't edit this entry!")
            return

        # Fetch the current item
        item = self.currentHistoryItem

        # Check if we don't match
        if item and selItem.id != item.id:
            # Loa the new entry
            item = self.DBSession.query(MeasurementHistoryTable).get(selItem.id)

        if item is None:
            self.logger.warning("Failed to fetch the history entry for editing!")
            return

        # Ask the user about the name
        text, ok = QtW.QInputDialog.getText(
            self.widget,
            _("measurementHistory.RenameMeasurement"),
            _("measurementHistory.RenameMeasurementLabel"),
            text=item.name,
        )

        if ok:
            # Set and save
            item.name = text
            self.DBSession.commit()

            # Reload
            self.reloadHistoryModelData()

    def selectionChanged(
        self, selected: QtC.QItemSelection, deselected: QtC.QItemSelection
    ):
        self.logger.info("Selection changed")
        # Get the current selection
        currentIndex = self.widget.historyList.currentIndex().row()

        # Check if it's valid
        if currentIndex == -1:
            self.logger.info("No selection made!")
            return

        # Enabled / disable the delete action
        self.widget.deleteAction.setEnabled(currentIndex > 0)

        # Update
        self.historySelectionChanged()

    def historySelectionChanged(self):
        self.logger.info("History selection changed")

        # Get the current selection
        currentIndex = self.widget.historyList.currentIndex().row()

        # Check if it's valid
        if currentIndex == -1:
            self.logger.info("No selection made!")
            return

        # Check if the index is over the limit
        if currentIndex >= len(self.historicalMeasurementModel.history):
            self.logger.info("Selected index is out of range!")
            return

        # We have something
        modelEntry = self.historicalMeasurementModel.history[currentIndex]

        # Re fetch it with our connection
        entry = self.DBSession.query(MeasurementHistoryTable).get(modelEntry.id)

        if entry is None:
            self.logger.info("The currently selected entry does not exist anymore")
            return

        # Check if we have the same id
        if entry.id == self.currentHistoryItem.id:
            # No need to update
            return

        # Set it
        self.currentHistoryItem = entry
        measID = self.currentHistoryItem.id

        # Check if this is the first entry, reset to None
        if measID == 1:
            measID = None

        # inform
        self.selectionChangedSignal.emit(measID)

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        # Commit
        self.DBSession.commit()

    def databaseChanged(self):
        self.logger.info("Reloading database")

        # Close the old one, open a new one
        self.DBSession.commit()
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # Load a history item, so we can link all to this
        self.currentHistoryItem = self.DBSession.query(MeasurementHistoryTable).first()

        # Check if we fetched something
        if self.currentHistoryItem == None:
            # Nope, create it
            self.currentHistoryItem = MeasurementHistoryTable(
                name=_(
                    "measurementHistory.InitialItem"
                ),  # TODO: This is how Microsoft does it, we can do better
                creationDate=datetime.now(),
                timeAxis=None,
                device=None,
                dwfVersion=None,
                samples=None,
                amplitude=None,
                offset=None,
                periods=None,
            )

            # Add and save
            self.DBSession.add(self.currentHistoryItem)
            self.DBSession.commit()

        # Reload
        self.reloadHistoryModelData()

    def reloadHistoryModelData(self):
        self.logger.info("Reloading history model data")

        # Reload data
        self.historicalMeasurementModel.updateHistory()
        self.historicalMeasurementModel.layoutChanged.emit()

    def exportHistoryClicked(self):
        self.logger.info("Clicked export history")

        self.exportSelectedMeasurement()

    def deleteHistoryClicked(self):
        self.logger.info("Clicked delete history")

        self.deleteSelectedMeasurement()

    def exportSelectedMeasurement(self):
        self.logger.info("Exporting selected history")

        if self.currentHistoryItem is None:
            self.logger.info("Nothing selected, can't export")

        # Check if we have an exporter
        if self.exporter is not None:
            return

        # Create a new exporter
        self.exporter = MeasurementExportController(
            self.currentHistoryItem.id,
            ADControl=self.ADControl,
            parent=self.widget
        )
        self.exporter.windowClosingSignal.connect(self.exporterClosed)
        self.exporter.showWindow()

    def exporterClosed(self):
        self.logger.info("Exporter closing")
        self.exporter = None

    def deleteSelectedMeasurement(self):
        self.logger.info("Deleting selected history")

        # get the current index
        currentIndex = self.widget.historyList.currentIndex().row()

        if currentIndex == -1:
            self.logger.info("can't delete nothing!")
            return

        if currentIndex >= len(self.historicalMeasurementModel.history):
            self.logger.info("Selected index is out of range, can't delete!")
            return

        # We have something
        entry = self.historicalMeasurementModel.history[currentIndex]

        if entry is None:
            self.logger.info(
                "The currently selected entry does not exist anymore, can't delete"
            )
            return

        # Check if this is the first entry
        if entry.id == 0:
            self.logger.info("Can't delete this measurement")
            return

        # Check if this is the currently selected one
        if self.currentHistoryItem and entry.id == self.currentHistoryItem.id:
            # Select the previous entry
            self.selectMeasurement(
                self.historicalMeasurementModel.history.index(entry) - 1
            )

        # connect to the DB & delete
        self.DBSession.query(MeasurementHistoryTable).filter_by(id=entry.id).delete()
        self.DBSession.commit()

        # update
        self.reloadHistoryModelData()

    def addMeasurementToHistory(
        self,
        signals: Dict[Union[configuration.ADOutputChannels, int], Signal],
        currentConfig: ConfigurationTable,
    ):
        self.logger.info("Adding measurement to history")

        # Get a new DB Session
        DBSession = DBConnection.getSession()

        # Check if the channel is an int
        for ch, sig in signals.items():
            # Check
            if isinstance(ch, int):
                # It's an int, convert!
                signals[configuration.ADInputChannels(ch)] = sig

                # And delete the old one
                del signals[ch]

        # Compress the time
        jsonTime = json.dumps(list(signals.values())[0].time.tolist())
        compressedTime = zlib.compress(jsonTime.encode("utf-8"))

        # Create the history entry
        measHist = MeasurementHistoryTable(
            name=_(f"Measurement {datetime.now().strftime('%H:%M:%S - %y/%m/%d')}"),
            creationDate=datetime.now(),
            timeAxis=compressedTime,
            device=currentConfig.lastDeviceSN,
            dwfVersion=currentConfig.dwfVersion,
            samples=currentConfig.sampleCount,
            amplitude=currentConfig.amplitude,
            offset=currentConfig.offset,
            periods=currentConfig.periods,
        )

        # Create it
        DBSession.add(measHist)
        DBSession.commit()

        # Add all the signals
        for channel, signal in signals.items():
            jsonSignal = json.dumps(signal.signal.tolist())
            compressedSignal = zlib.compress(jsonSignal.encode("utf-8"))

            # Create the measurement
            meas = MeasurementTable(
                measurementID=measHist.id, channel=channel, dataAxis=compressedSignal
            )

            DBSession.add(meas)

        # Inform
        self.createCopyForMeasurementSignal.emit(measHist.id)

        # Commit
        DBSession.commit()
        DBSession.close()

        # reload the history view
        self.reloadHistoryModelData()

    def selectMeasurement(self, measID: int):
        self.logger.info("Selecting a new measurement")

        # Update
        self.widget.historyList.setCurrentIndex(measID)

        # inform
        self.historySelectionChanged()

    def fetchMeasurements(self) -> Dict[configuration.ADInputChannels, Signal]:
        self.logger.info(f"Fetching a measurement with id {self.currentHistoryItem.id}")

        # Okay, now lets fetch the measurement data
        measurements: List[MeasurementTable]
        try:
            measurements = (
                self.DBSession.query(MeasurementTable)
                .filter_by(measurementID=self.currentHistoryItem.id)
                .all()
            )

        except Exception as e:
            self.logger.info(
                f"Failed to fetch any measurements for measID {index}: {e}"
            )
            return {}

        # Convert the time back to a usable value
        timeData = self.currentHistoryItem.timeAxis

        time: np.ndarray
        try:
            timeJSON = zlib.decompress(timeData)
            time = np.array(json.loads(timeJSON))

        except Exception as e:
            self.logger.info(f"Failed to unpack the time axis! {e}")
            return {}

        # Combine the signals with the time
        signals: Dict[configuration.ADOutputChannels, Signal] = {}
        for meas in measurements:
            # Get the data
            signalData = meas.dataAxis
            channel = meas.channel

            # Convert the data back
            signal: np.ndarray
            try:
                signalJSON = zlib.decompress(signalData)
                signal = np.array(json.loads(signalJSON))

            except Exception as e:
                self.logger.info(f"Failed to de-serialize the signal! {e}")
                continue

            # Convert to a usable signal for us
            signals[channel] = Signal("", time, signal)

        # Return the signals
        return signals
