# Native imports
import sys
from typing import Optional, List, Tuple, cast
import logging
import os
from datetime import datetime
from enum import Enum, auto

# 3rd Party
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
import pyqtgraph as QtG
from AMASignal import WaveForms, Generator, Signal
import qtawesome as qta
from sqlalchemy.orm import Session

# Custom
from WaveGen.Helpers.pyqtHelpers import QSpacer
from WaveGen.Helpers.waveCalculator import WaveCalculator
from WaveGen import configuration
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.Helpers.translation import _
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import Models


class WaveConfigurationsWidget(QtW.QWidget):
    def __init__(self):
        super().__init__()

        # Start a new layout
        self.layout = QtW.QHBoxLayout()

        # Start the actual configuration
        configLayout = self.setupConfigView()
        self.layout.addLayout(configLayout)

        # Now the buttons
        buttonLayout = QtW.QVBoxLayout()
        self.saveButton = QtW.QPushButton(_("waveConfigurator.Save_Button"))
        self.saveButton.setFixedWidth(80)
        buttonLayout.addWidget(self.saveButton)

        self.applyButton = QtW.QPushButton(_("waveConfigurator.Apply_Button"))
        self.applyButton.setFixedWidth(80)
        buttonLayout.addWidget(self.applyButton)

        self.cancelButton = QtW.QPushButton(_("waveConfigurator.Cancel_Button"))
        self.cancelButton.setFixedWidth(80)
        buttonLayout.addWidget(self.cancelButton)

        # Get the buttons to the top
        buttonLayout.addWidget(QSpacer(80))

        self.layout.addLayout(buttonLayout)

        # Set the layout
        self.setLayout(self.layout)

    def setupConfigView(self) -> QtW.QLayout:
        # Create the layout
        configLayout = QtW.QVBoxLayout()
        enableLayout = QtW.QHBoxLayout()

        # Name
        nameLayout = QtW.QHBoxLayout()
        configLayout.addLayout(nameLayout)
        nameLayout.addWidget(QtW.QLabel(_("waveConfigurator.name_label")))
        self.nameLineEdit = QtW.QLineEdit()
        nameLayout.addWidget(self.nameLineEdit)
        nameLayout.addWidget(QSpacer(200, 5))

        # Type
        enableLayout.addWidget(QtW.QLabel(_("waveConfigurator.type_label")))
        self.waveformTypeComboBox = QtW.QComboBox()
        enableLayout.addWidget(self.waveformTypeComboBox)

        # Add a bit of space
        enableLayout.addWidget(QSpacer(5, 5))

        # Channel selection
        enableLayout.addWidget(QtW.QLabel(_("waveConfigurator.channel_label")))
        self.channelComboBox = QtW.QComboBox()
        enableLayout.addWidget(self.channelComboBox)

        # Add a bit of space
        enableLayout.addWidget(QSpacer(5, 5))

        # Enable check box
        enableLayout.addWidget(QtW.QLabel(_("waveConfigurator.EnableWave_label")))
        self.enableWaveCheckBox = QtW.QCheckBox()
        self.enableWaveCheckBox.setChecked(True)
        enableLayout.addWidget(self.enableWaveCheckBox)

        # Push everything to the left
        enableLayout.addWidget(QSpacer(height=5))
        configLayout.addLayout(enableLayout)

        # Create a group box
        self.configGroupBox = QtW.QGroupBox()
        configLayout.addWidget(self.configGroupBox)

        # Configure the layout for the config elements
        optionColumn1 = QtH.QFormLayoutRight()
        optionColumn2 = QtH.QFormLayoutRight()
        configOptionsLayout = QtW.QHBoxLayout()
        configOptionsLayout.addLayout(optionColumn1)
        configOptionsLayout.addWidget(QSpacer(20))
        configOptionsLayout.addLayout(optionColumn2)
        self.configGroupBox.setLayout(configOptionsLayout)

        # Frequency
        self.frequencySpinBox = QtW.QDoubleSpinBox()
        self.frequencySpinBox.setRange(0.0001, 2 ** 30)
        self.frequencySpinBox.setValue(1)
        self.frequencySpinBox.setDecimals(4)
        self.frequencySpinBox.setSingleStep(1)
        self.frequencySpinBox.setSuffix("Hz")
        self.frequencySpinBox.setAccelerated(True)
        optionColumn1.addRow(
            QtW.QLabel(_("waveConfigurator.frequency_label")), self.frequencySpinBox
        )

        # Amplitude
        self.amplitudeSpinBox = QtW.QDoubleSpinBox()
        self.amplitudeSpinBox.setRange(0, 5)
        self.amplitudeSpinBox.setSingleStep(0.1)
        self.amplitudeSpinBox.setSuffix("V")
        self.amplitudeSpinBox.setAccelerated(True)
        optionColumn1.addRow(
            QtW.QLabel(_("waveConfigurator.amplitude_label")), self.amplitudeSpinBox
        )

        # Phase
        self.phaseSpinBox = QtW.QDoubleSpinBox()
        self.phaseSpinBox.setRange(-360.0, 360.0)
        self.phaseSpinBox.setValue(0)
        self.phaseSpinBox.setDecimals(1)
        self.phaseSpinBox.setSingleStep(10)
        self.phaseSpinBox.setSuffix("°")
        self.phaseSpinBox.setAccelerated(True)
        optionColumn1.addRow(
            QtW.QLabel(_("waveConfigurator.phase_label")), self.phaseSpinBox
        )

        # Offset
        self.offsetSpinBox = QtW.QDoubleSpinBox()
        self.offsetSpinBox.setRange(-20, 20)
        self.offsetSpinBox.setValue(0)
        self.offsetSpinBox.setSingleStep(0.1)
        self.offsetSpinBox.setSuffix("V")
        self.offsetSpinBox.setAccelerated(True)
        optionColumn2.addRow(
            QtW.QLabel(_("waveConfigurator.offset_label")), self.offsetSpinBox
        )

        # Offset
        self.dutyCycleSpinBox = QtW.QDoubleSpinBox()
        self.dutyCycleSpinBox.setRange(0, 100)
        self.dutyCycleSpinBox.setValue(80)
        self.dutyCycleSpinBox.setDecimals(1)
        self.dutyCycleSpinBox.setSingleStep(5)
        self.dutyCycleSpinBox.setSuffix("%")
        self.dutyCycleSpinBox.setAccelerated(True)
        optionColumn2.addRow(
            QtW.QLabel(_("waveConfigurator.dutycycle_label")), self.dutyCycleSpinBox
        )

        # Done
        return configLayout


class WaveConfigurator(QtW.QMainWindow):
    def __init__(
        self,
        waveConfig: WaveConfigurationsWidget,
        plotView: QtG.PlotWidget,
        parent=None,
    ):
        super().__init__(parent)

        self.setObjectName(self.__class__.__name__)

        # Setup of the view
        self.layout = QtW.QHBoxLayout()
        self.wid = QtW.QWidget(self)
        self.setCentralWidget(self.wid)
        self.wid.setLayout(self.layout)

        # Create the splitter
        self.mainSplitter = QtW.QSplitter(QtC.Qt.Vertical)
        self.mainSplitter.addWidget(waveConfig)
        self.mainSplitter.addWidget(plotView)

        self.layout.addWidget(self.mainSplitter)

        # Configure the window
        self.setWindowTitle(_("waveConfigurator.title"))
        self.resize(800, 600)


class WaveConfiguratorController(QtC.QObject):
    waveUpdatedSignal = QtC.pyqtSignal(object)  # WaveConfigurationTable
    windowClosedSignal = QtC.pyqtSignal(object)  # config ID or None for new window

    def __init__(self, parent, waveConfigurationID: Optional[int] = None):
        super().__init__()

        # Get a logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # prepare
        self.updatingWaveConfiguration = False

        # Create a new DB session
        self.DBSession = DBConnection.getSession()

        # Set the properties
        self.waveConfig: WaveConfigurationTable
        self.waveConfigurationID = waveConfigurationID

        # check if we have a wave configuration
        if waveConfigurationID is None:
            # Create one
            self.logger.info("Creating new waveConfiguration")

            self.waveConfig = WaveConfigurationTable(
                name="",
                userChangedName=False,
                creationDate=datetime.now(),
                frequency=1,
                amplitude=1,
                phase=0,
                waveType=configuration.WaveTypes.Sinus,
                offset=0,
                channel=configuration.ADOutputChannels.W1,
                dutyCycle=80,
                order=None,
                enabled=True,
                measurementID=None,
            )
        else:
            # Get it by ID
            self.logger.info(
                f"Using existing waveConfiguration with {waveConfigurationID}"
            )
            self.waveConfig = self.DBSession.query(WaveConfigurationTable).get(
                waveConfigurationID
            )

        # Create the widgets
        self.parent = parent
        self.configurationsWidget = WaveConfigurationsWidget()

        # Create the models
        self.waveTypesModel = Models.WaveTypesModel()
        self.channelsModel = Models.ADOutputChannelsModel()

        # Set the models
        self.configurationsWidget.waveformTypeComboBox.setModel(self.waveTypesModel)
        self.configurationsWidget.channelComboBox.setModel(self.channelsModel)

        # Create the plot view
        self.plotWidget = QtG.PlotWidget()

        # Get the main window
        self.waveConfigurator = WaveConfigurator(
            self.configurationsWidget, self.plotWidget, parent=parent
        )

        self.userChangedWaveName = False
        self.changedWaveNameAutomatically = False

        # Connect
        self.hasConnectedEvents = False
        self.connect()

    def showWindow(self):
        self.logger.info("Showing wave configurator window")

        # Update the plot
        self.loadFromModel()

        # Update the waveform
        self.waveformChanged()

        # Show
        self.waveConfigurator.show()

        # Connect
        self.connect()

    def loadFromModel(self):
        logging.info("Loading GUI values from wave model")

        if self.waveConfig is None:
            logging.info("No wave configuration has been loaded!")
            return

        # Set the spin boxes
        self.updatingWaveConfiguration = True
        self.configurationsWidget.frequencySpinBox.setValue(self.waveConfig.frequency)
        self.configurationsWidget.amplitudeSpinBox.setValue(self.waveConfig.amplitude)
        self.configurationsWidget.phaseSpinBox.setValue(self.waveConfig.phase)
        self.configurationsWidget.offsetSpinBox.setValue(self.waveConfig.offset)
        self.configurationsWidget.dutyCycleSpinBox.setValue(self.waveConfig.dutyCycle)

        # Checkmark
        self.configurationsWidget.enableWaveCheckBox.setChecked(self.waveConfig.enabled)

        # Set the name
        self.userChangedWaveName = self.waveConfig.userChangedName
        self.changedWaveNameAutomatically = not self.userChangedWaveName
        self.configurationsWidget.nameLineEdit.setText(self.waveConfig.name)

        # Set the index of the waveform
        waves = [wave[0] for wave in self.waveTypesModel.waveTypes]
        waveformIndex = waves.index(self.waveConfig.waveType)
        self.configurationsWidget.waveformTypeComboBox.setCurrentIndex(waveformIndex)

        # Set the index of the channel
        channelIndex = self.channelsModel.channels.index(self.waveConfig.channel)
        self.configurationsWidget.channelComboBox.setCurrentIndex(channelIndex)
        self.updatingWaveConfiguration = False

        # Update
        self.inputValueChanged()

    def connect(self):
        if self.hasConnectedEvents:
            return
        self.hasConnectedEvents = True

        # Connect the buttons. We need the lambda for some reason. Won't work otherwise.
        cnfWi = self.configurationsWidget
        cnfWi.saveButton.clicked.connect(self.saveButtonClicked)
        cnfWi.applyButton.clicked.connect(self.applyButtonClicked)
        cnfWi.cancelButton.clicked.connect(self.cancelButtonClicked)
        cnfWi.waveformTypeComboBox.currentIndexChanged.connect(self.waveformChanged)
        cnfWi.channelComboBox.currentIndexChanged.connect(self.channelSelectionChanged)
        cnfWi.enableWaveCheckBox.stateChanged.connect(self.waveEnabledChanged)
        cnfWi.frequencySpinBox.valueChanged.connect(self.inputValueChanged)
        cnfWi.amplitudeSpinBox.valueChanged.connect(self.inputValueChanged)
        cnfWi.phaseSpinBox.valueChanged.connect(self.inputValueChanged)
        cnfWi.offsetSpinBox.valueChanged.connect(self.inputValueChanged)
        cnfWi.dutyCycleSpinBox.valueChanged.connect(self.inputValueChanged)
        cnfWi.nameLineEdit.textChanged.connect(self.waveNameChanged)

        # Handle events
        cnfWi.saveButton.installEventFilter(self)
        cnfWi.applyButton.installEventFilter(self)
        cnfWi.cancelButton.installEventFilter(self)
        cnfWi.waveformTypeComboBox.installEventFilter(self)
        cnfWi.channelComboBox.installEventFilter(self)
        cnfWi.enableWaveCheckBox.installEventFilter(self)
        cnfWi.frequencySpinBox.installEventFilter(self)
        cnfWi.amplitudeSpinBox.installEventFilter(self)
        cnfWi.phaseSpinBox.installEventFilter(self)
        cnfWi.offsetSpinBox.installEventFilter(self)
        cnfWi.dutyCycleSpinBox.installEventFilter(self)
        cnfWi.nameLineEdit.installEventFilter(self)

        cnfWi.hideEvent = self.windowClosed

    def windowClosed(self, event: QtC.QEvent):
        self.logger.info("window closed")

        # Get the wave id
        waveID = self.waveConfig.id

        if self.waveConfigurationID is None:
            waveID = None

        # Signal
        self.windowClosedSignal.emit(waveID)
        self.configurationsWidget.close()

    def eventFilter(self, obj: QtC.QObject, event: QtC.QEvent):
        if event.type() == QtC.QEvent.KeyPress:
            # Change the type
            event = cast(Qt.QKeyEvent, event)

            # Check if we pressed the return key
            if event.key() == QtC.Qt.Key_Enter or event.key() == QtC.Qt.Key_Return:
                # Check if cancel or apply
                if obj == self.configurationsWidget.cancelButton:
                    self.cancelButtonClicked()
                elif obj == self.configurationsWidget.applyButton:
                    self.applyButtonClicked()
                else:
                    # Submit!
                    self.saveButtonClicked()

                # Done
                return True
        # Not us
        return False

    def waveNameChanged(self):
        self.logger.info("Wave name changed!")

        # get the text
        name = self.configurationsWidget.nameLineEdit.text()

        # update the name
        self.waveConfig.name = self.configurationsWidget.nameLineEdit.text()

        if self.changedWaveNameAutomatically:
            self.logger.info("Discarding wave name change, as it was automatic!")
            self.changedWaveNameAutomatically = False
            return

        # Set it
        self.userChangedWaveName = len(name.strip()) > 0

    def saveButtonClicked(self):
        self.logger.info("Save Button Clicked")

        # Apply the changes
        self.applyChanges()

        # close the window
        self.waveConfigurator.close()

    def applyButtonClicked(self):
        self.logger.info("Apply Button Clicked")

        self.applyChanges()

    def cancelButtonClicked(self):
        self.logger.info("Cancel Button Clicked")

        # close the window
        self.waveConfigurator.close()

    def applyChanges(self):
        self.logger.info("Applying changes")

        signalIndex = self.configurationsWidget.waveformTypeComboBox.currentIndex()
        self.waveConfig.waveType = self.waveTypesModel.waveTypes[signalIndex][0]

        # Check that we have a wave
        if self.waveConfig is None:
            logging.info("Can't save changes, as no wave config exists!")
            return

        # Check we have an ID
        if self.waveConfig.id is None:
            # Add it
            self.logger.info("Adding new wave to the database!")
            self.DBSession.add(self.waveConfig)

        self.logger.info(f"Following DB entries are dirty: {self.DBSession.dirty}")

        # Commit to the changes
        self.DBSession.commit()

        # Send a signal
        self.waveUpdatedSignal.emit(self.waveConfig)

    def waveformChanged(self):
        self.logger.info("Waveform selection changed")

        # Update the configuration box
        self.updateConfigurationBoxToSelection()

        # Update the plot
        self.inputValueChanged()

    def waveEnabledChanged(self):
        # Get the new state
        state = self.configurationsWidget.enableWaveCheckBox.isChecked()

        # Log
        self.logger.info(f"Enable state changed to {state}")

        # Update the state
        self.configurationsWidget.configGroupBox.setEnabled(state)

        # Update the plot
        self.inputValueChanged()

    def channelSelectionChanged(self):
        self.logger.info("Channel selection changed!")

        # Update the plot
        self.inputValueChanged()

    def inputValueChanged(self):
        self.logger.info("An input value changed!")

        if self.updatingWaveConfiguration:
            self.logger.info("Ignoring input change, this is an automatic change!")
            return

        # Get the data
        frequency = float(self.configurationsWidget.frequencySpinBox.value())
        amplitude = float(self.configurationsWidget.amplitudeSpinBox.value())
        phase = float(self.configurationsWidget.phaseSpinBox.value())
        offset = float(self.configurationsWidget.offsetSpinBox.value())
        dutyCycle = float(self.configurationsWidget.dutyCycleSpinBox.value())

        # Check if the wave is enabled
        waveEnabled = self.configurationsWidget.enableWaveCheckBox.isChecked()

        # Get the colour
        outputChannelIndex = self.configurationsWidget.channelComboBox.currentIndex()
        selectedChannel = self.channelsModel.channels[outputChannelIndex]

        # Get the signal type
        signalIndex = self.configurationsWidget.waveformTypeComboBox.currentIndex()
        signalType = self.waveTypesModel.waveTypes[signalIndex][0]

        # Check if the user has changed the name
        if not self.userChangedWaveName:
            # Update the name
            signalName = self.configurationsWidget.waveformTypeComboBox.currentText()

            pwmSetup = (
                f", {dutyCycle:.1f}%"
                if signalType == configuration.WaveTypes.PWM
                else ""
            )

            # Update the text
            self.changedWaveNameAutomatically = True
            self.configurationsWidget.nameLineEdit.setText(
                f"{signalName} {selectedChannel.name} ({frequency:.2f}Hz, {amplitude:.2f}V, {phase:.1f}°{pwmSetup})"
            )

        # Set the values
        self.waveConfig.waveType = signalType
        self.waveConfig.frequency = frequency
        self.waveConfig.amplitude = amplitude
        self.waveConfig.phase = phase
        self.waveConfig.offset = offset
        self.waveConfig.dutyCycle = dutyCycle
        self.waveConfig.enabled = waveEnabled
        self.waveConfig.channel = selectedChannel

        # Name gets set in the change event!

        # Plot :)
        WaveCalculator.calculateWavesAndPlot(
            waveConfigs=(self.waveConfig,),
            amplitude=1.0,
            offset=0.0,
            periods=4,
            enabled=True,
            sampleCount=4096,
            individualPlots=True,
            masterPlot=False,
            plotCanvas=self.plotWidget,
        )

    def updateConfigurationBoxToSelection(self):
        self.logger.info("Updating configuration UI to selection")

        # get the selection
        signalIndex = self.configurationsWidget.waveformTypeComboBox.currentIndex()
        signalType = self.waveTypesModel.waveTypes[signalIndex][0]

        # Set the default state
        frequencyEnabled = True
        phaseEnabled = True
        amplitudeEnabled = True
        dutyCycleEnabled = False

        # Check for the special ones
        if signalType == configuration.WaveTypes.PWM:
            dutyCycleEnabled = True

        elif signalType == configuration.WaveTypes.DC:
            frequencyEnabled = False
            amplitudeEnabled = False
            phaseEnabled = False

        # Update the UI
        self.configurationsWidget.frequencySpinBox.setEnabled(frequencyEnabled)
        self.configurationsWidget.phaseSpinBox.setEnabled(phaseEnabled)
        self.configurationsWidget.amplitudeSpinBox.setEnabled(amplitudeEnabled)
        self.configurationsWidget.dutyCycleSpinBox.setEnabled(dutyCycleEnabled)
