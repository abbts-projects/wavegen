# Native imports
from typing import Optional, List, Tuple, Dict, Union
import logging
from enum import Enum, auto

# 3rd Party
import pyqtgraph as QtG
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt
from AMASignal import WaveForms, Generator, Signal, AnalogWaveforms
import qtawesome as qta
import dwf

# Custom
from WaveGen.GUI.wavePlot import WavePlotController
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.DB.waveConfigurationTable import WaveConfigurationTable
from WaveGen.DB.ADConfigurationTable import (
    ADConfigurationTable,
    ADConfigurationRecordingChannelTable,
)
from WaveGen.Helpers.translation import _
from WaveGen.Helpers import pyqtHelpers as QtH
from WaveGen import configuration
from WaveGen import Models


class ADControlRecChannelWidget(QtW.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setObjectName(self.__class__.__name__)

        self.setMinimumWidth(250)

        self.layout = QtW.QVBoxLayout()

        # Add a title
        self.layout.addWidget(QtH.QTitleLabel(_("ADControl.Channel"), "#aaa"))

        # Add the layout
        self.setLayout(self.layout)


class ADControlRecChannelController(QtC.QObject):
    # Signals
    valuesChangedSignal = QtC.pyqtSignal()

    def __init__(self, ADControl: "ADControl", parent=None):
        super().__init__(parent)

        # New logger
        self.logger = logging.getLogger(self.__class__.__name__)

        # keep inputs
        self.ADControl = ADControl

        # Prepare
        self.DBSession = DBConnection.getSession()
        self.currentCHConfig: List[ADConfigurationRecordingChannelTable] = []

        # create the widget
        self.widget = ADControlRecChannelWidget()

        # Prepare
        self.channelControllers: Dict[
            configuration.ADInputChannels, ADControlSingleRecChannelController
        ] = {}

        # create the single channels
        for ch in list(configuration.ADInputChannels):
            # create a new single channel controller
            controller = ADControlSingleRecChannelController(title=ch.name, channel=ch)

            # Add it
            self.channelControllers[ch] = controller

            # add it to the view
            self.widget.layout.addWidget(controller.widget)

        self.connect()

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")

        # Inform the sub controllers
        for _, ctrl in self.channelControllers.items():
            ctrl.projectAboutToClose()

        self.saveProject()

    def saveProject(self):
        self.logger.info("Saving project")

        # Inform the sub controllers
        for _, ctrl in self.channelControllers.items():
            ctrl.saveProject()

        # Commit
        self.DBSession.commit()

    def offsetForChannel(self, channel: Union[int, configuration.ADInputChannels]):
        if isinstance(channel, int):
            channel = configuration.ADInputChannels(channel)

        # Get the offset
        return self.channelControllers[channel].offset

    def enabledForChannel(self, channel: Union[int, configuration.ADInputChannels]):
        if isinstance(channel, int):
            channel = configuration.ADInputChannels(channel)

        # Get the offset
        return self.channelControllers[channel].enabled

    def connect(self):
        # Connect the channel setup controllers
        for controller in self.channelControllers.values():
            controller.valuesChangedSignal.connect(self.valuesChangedSignal.emit)

    def databaseChanged(self):
        self.logger.info("Updating DB")

        # Close the old one, open a new one
        self.DBSession.commit()
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # Inform the sub controllers
        for _, ctrl in self.channelControllers.items():
            ctrl.databaseChanged()

        # Update
        self.updateGUIValues()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # Inform the sub controllers
        for _, ctrl in self.channelControllers.items():
            ctrl.historySelectionChanged(currentID)

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Inform the sub controllers
        for _, ctrl in self.channelControllers.items():
            ctrl.historyCopySetup(newID)

    def updateGUIValues(self):
        self.logger.info("Updating GUI values")

        # Nothing to do here :(


class ADControlSingleRecChannelWidget(QtW.QGroupBox):
    def __init__(self, channelName: str, parent=None):
        super().__init__(channelName, parent=parent)

        self.layout = QtH.QFormLayoutRight()

        self.channelOffsetSpinBox = QtW.QDoubleSpinBox()
        self.channelOffsetSpinBox.setAccelerated(True)
        self.channelOffsetSpinBox.setSingleStep(0.1)
        self.channelOffsetSpinBox.setRange(-100, 100)
        self.channelOffsetSpinBox.setSuffix("V")

        self.channelRangeComboBox = QtW.QComboBox()

        self.enabledCheckBox = QtW.QCheckBox()
        self.enabledCheckBox.setChecked(True)

        self.layout.addRow(QtW.QLabel(_("ADControl.Enabled")), self.enabledCheckBox)
        self.layout.addRow(QtW.QLabel(_("ADControl.Offset")), self.channelOffsetSpinBox)
        self.layout.addRow(QtW.QLabel(_("ADControl.Range")), self.channelRangeComboBox)

        self.setLayout(self.layout)


class ADControlSingleRecChannelController(QtC.QObject):
    # Signals
    valuesChangedSignal = QtC.pyqtSignal()

    def __init__(
        self, title: str, channel: configuration.ADOutputChannels, parent=None
    ):
        super().__init__(parent)

        # New logger
        self.logger = logging.getLogger(self.__class__.__name__)

        self.DBSession = DBConnection.getSession()

        # Widget init
        self.widget = ADControlSingleRecChannelWidget(title)

        self.title = title
        self.channel = channel

        # Update the voltage ranges
        self.voltageRangeModel = Models.ADControlVoltageRangeModel()
        self.widget.channelRangeComboBox.setModel(self.voltageRangeModel)

        # Prepare
        self.offset = 0
        self.enabled = True
        self.config: Optional[ADConfigurationRecordingChannelTable] = None
        self.currentHistoryID: Optional[int] = None

        # Connect
        self.connect()

    def projectAboutToClose(self):
        self.logger.info("Project about to close!")
        # TODO

    def saveProject(self):
        self.logger.info("Saving project")

        self.DBSession.commit()

    def historySelectionChanged(self, currentID: Optional[int]):
        self.logger.info("History selection has changed!")

        # save changes
        self.DBSession.commit()

        # Update
        self.currentHistoryID = currentID
        self.config = (
            self.DBSession.query(ADConfigurationRecordingChannelTable)
            .filter_by(channel=self.channel, measurementID=currentID)
            .first()
        )

        # update
        self.updateGUIValues()

    def historyCopySetup(self, newID: int):
        self.logger.info("Copy setup for history")

        # Get a new copy of the config
        cfg = self.DBSession.query(ADConfigurationRecordingChannelTable).get(
            self.config.id
        )

        # Create a copy
        DBConnection.copyAndAdd(self.DBSession, cfg)

        # Set the ID
        cfg.measurementID = newID

        # Commit to the changes
        self.DBSession.commit()

    def databaseChanged(self):
        self.logger.info("Database changed")

        # Save and re-open
        self.DBSession.commit()
        self.DBSession.close()
        self.DBSession = DBConnection.getSession()

        # Load the entry
        self.config = (
            self.DBSession.query(ADConfigurationRecordingChannelTable)
            .filter_by(channel=self.channel)
            .first()
        )

        # Check if we have a config
        if self.config is None:
            # Nope, create one
            self.config = ADConfigurationRecordingChannelTable(
                channel=self.channel,
                offset=0,
                voltageRange=AnalogWaveforms.VoltageRange.PM25,
                enabled=True,
                measurementID=None,
            )

            # Save
            self.DBSession.add(self.config)
            self.DBSession.commit()

        # Update the GUI
        self.updateGUIValues()

    def updateGUIValues(self):
        self.logger.info("Updating GUI Values")

        # Check the config
        if self.config is None:
            self.logger.info("Invalid config!")
            return

        # Update
        self.setOffset(self.config.offset)
        self.selectVoltageRange(self.config.voltageRange)
        self.setEnabled(self.config.enabled)

    def connect(self):
        # Connect all changes
        self.widget.channelOffsetSpinBox.valueChanged.connect(self.offsetChanged)
        self.widget.channelRangeComboBox.currentIndexChanged.connect(
            self.voltageRangeChanged
        )
        self.widget.enabledCheckBox.stateChanged.connect(self.enabledChanged)

    def offsetChanged(self):
        self.logger.info("Offset changed")
        self.offset = self.widget.channelOffsetSpinBox.value()

        # Update the config
        if self.config is not None:
            self.config.offset = self.offset

        self.valueChanged()

    def enabledChanged(self):
        self.logger.info("Enabled changed")
        self.enabled = self.widget.enabledCheckBox.isChecked()

        # Update the config
        if self.config is not None:
            self.config.enabled = self.enabled

        self.valueChanged()

    def voltageRangeChanged(self):
        self.logger.info("Voltage range changed")
        voltageRange = self.voltageRange()

        # Update the config
        if self.config is not None:
            self.config.voltageRange = voltageRange

        self.valueChanged()

    def valueChanged(self):
        self.logger.info("A value changed!")

        # Emit a new signal
        self.valuesChangedSignal.emit()

    def voltageRange(self) -> AnalogWaveforms.VoltageRange:
        self.logger.info("getting voltage range")

        # Get the current index
        index = self.widget.channelRangeComboBox.currentIndex()
        return self.voltageRangeModel.voltageRanges[index]

    def setOffset(self, offset: float):
        self.logger.info("Setting new offset")
        self.widget.channelOffsetSpinBox.setValue(offset)

    def selectVoltageRange(self, vRange: AnalogWaveforms.VoltageRange):
        self.logger.info("Selecting new voltage range")
        # Get the new index
        newRangeIndex = self.voltageRangeModel.voltageRanges.index(vRange)

        # Set it
        self.widget.channelRangeComboBox.setCurrentIndex(newRangeIndex)

    def setEnabled(self, enabled: bool):
        self.widget.enabledCheckBox.setChecked(enabled)
