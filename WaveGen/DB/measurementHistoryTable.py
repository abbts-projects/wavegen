# 3rd Party
from sqlalchemy import (
    Column,
    Integer,
    DateTime as SQLDateTime,
    Unicode,
    ForeignKey,
    Boolean,
    Enum,
    Float,
    Binary,
)

# Custom
from WaveGen.DB.DBConnection import DBConnection
from WaveGen import configuration


class MeasurementHistoryTable(DBConnection.Base):
    __tablename__ = "MeasurementHistory"

    id = Column(Integer, primary_key=True)
    name = Column(Unicode)
    creationDate = Column(SQLDateTime)
    timeAxis = Column(Binary)
    device = Column(Unicode)
    dwfVersion = Column(Unicode)
    samples = Column(Integer)
    amplitude = Column(Float)
    offset = Column(Float)
    periods = Column(Float)

    def __repr__(self):
        return f"<User(id='{self.id}', name='{self.name}', dateTime='{self.dateTime}', timeAxis(len)='{len(self.timeAxis)}', device='{self.device}', samples='{self.samples}', amplitude='{self.amplitude}', offset='{self.offset}', periods='{self.periods}', individual='{self.individual}')>"


class MeasurementTable(DBConnection.Base):
    __tablename__ = "Measurement"

    id = Column(Integer, primary_key=True)
    measurementID = Column(Integer, ForeignKey("MeasurementHistory.id"))
    channel = Column(Enum(configuration.ADInputChannels))
    dataAxis = Column(Binary)

    def __repr__(self):
        return f"<User(id='{self.id}', measurementID='{self.measurementID}', dataAxis(len)='{len(self.dataAxis)}')>"
