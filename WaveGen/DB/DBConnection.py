# Standard imports

# 3rd party

# Custom
from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session, make_transient


class DBConnection:
    engine = create_engine("sqlite:///:memory:")
    Session = sessionmaker(bind=engine)
    metadata = MetaData(bind=engine)
    Base = declarative_base()

    @staticmethod
    def setEnginePath(path: str):
        DBConnection.engine = create_engine(f"sqlite:///{path}")
        DBConnection.Session = sessionmaker(bind=DBConnection.engine)
        DBConnection.metadata = MetaData(bind=DBConnection.engine)

    @staticmethod
    def getSession():
        return Session(bind=DBConnection.engine)

    @staticmethod
    def copyAndAdd(
        session: Session, obj: declarative_base
    ):  # TODO: is obj type correct?
        # https://stackoverflow.com/a/28871439/557087

        # Detach and copy
        session.expunge(obj)  # expunge the object from session
        make_transient(
            obj
        )  # http://docs.sqlalchemy.org/en/rel_1_1/orm/session_api.html#sqlalchemy.orm.session.make_transient

        # Reset the ID
        obj.id = None
        session.add(obj)
