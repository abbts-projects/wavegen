# 3rd Party
from sqlalchemy import (
    Column,
    Integer,
    Float,
    DateTime as SQLDateTime,
    Unicode,
    ForeignKey,
    Boolean,
)

# Custom
from WaveGen.DB.DBConnection import DBConnection


class ConfigurationTable(DBConnection.Base):
    __tablename__ = "Configuration"

    id = Column(Integer, primary_key=True)
    sampleCount = Column(Integer)
    amplitude = Column(Float)
    offset = Column(Float)
    periods = Column(Float)
    individual = Column(Boolean)
    clipVoltage = Column(Boolean)
    creationDate = Column(SQLDateTime)
    version = Column(Unicode)
    lastDeviceSN = Column(Unicode)
    dwfVersion = Column(Unicode)
    windowSaveState = Column(Unicode)
    measurementID = Column(Integer, ForeignKey("MeasurementHistory.id"))
