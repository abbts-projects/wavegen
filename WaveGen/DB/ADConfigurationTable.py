# 3rd Party
from sqlalchemy import (
    Column,
    Integer,
    Float,
    DateTime as SQLDateTime,
    Unicode,
    ForeignKey,
    Boolean,
    Enum,
)
from AMASignal import AnalogWaveforms
import dwf

# Custom
from WaveGen.DB.DBConnection import DBConnection
from WaveGen import configuration


class ADConfigurationTable(DBConnection.Base):
    __tablename__ = "ADConfiguration"

    id = Column(Integer, primary_key=True)

    recordingMethod = Column(Enum(configuration.RecordingMethods))
    recordingSamples = Column(Integer)
    recordingFrequency = Column(Float)

    outputSynchronizationEnabled = Column(Boolean)
    outputSynchronizationMaster = Column(Enum(configuration.ADOutputChannels))
    outputSynchronizationSlave = Column(Enum(configuration.ADOutputChannels))

    triggerEnable = Column(Boolean)
    triggerChannel = Column(Enum(configuration.ADInputChannels))
    triggerLevel = Column(Float)
    triggerType = Column(Enum(dwf.DwfAnalogIn.TRIGTYPE))
    triggerCondition = Column(Enum(dwf.DwfAnalogIn.TRIGCOND))
    triggerSource = Column(Enum(dwf.DwfAnalogIn.TRIGSRC))

    xyXCH = Column(Enum(configuration.ADInputChannels))
    xyYCH = Column(Enum(configuration.ADInputChannels))

    fftWindow = Column(Enum(configuration.Windows))
    fftLimit = Column(Float)

    measurementID = Column(Integer, ForeignKey("MeasurementHistory.id"))

    def __repr__(self):
        return f"""<
recordingMethod="{self.recordingMethod}"
recordingSamples="{self.recordingSamples}"
recordingFrequency="{self.recordingFrequency}"
outputSynchronizationEnabled="{self.outputSynchronizationEnabled}"
outputSynchronizationMaster="{self.outputSynchronizationMaster}"
outputSynchronizationSlave="{self.outputSynchronizationSlave}"
triggerEnable="{self.triggerEnable}"
triggerChannel="{self.triggerChannel}"
triggerLevel="{self.triggerLevel}"
triggerType="{self.triggerType}"
triggerCondition="{self.triggerCondition}"
triggerSource="{self.triggerSource}"
xyXCH="{self.xyXCH}"
xyYCH="{self.xyYCH}"
fftWindow="{self.fftWindow}"
fftLimit="{self.fftLimit}"
measurementID="{self.measurementID}">"""


class ADConfigurationRecordingChannelTable(DBConnection.Base):
    __tablename__ = "ADConfigurationRecChannel"

    id = Column(Integer, primary_key=True)
    # configID = Column(Integer, ForeignKey("ADConfiguration.id"))
    channel = Column(Enum(configuration.ADInputChannels))
    offset = Column(Float)
    voltageRange = Column(Enum(AnalogWaveforms.VoltageRange))
    enabled = Column(Boolean)
    measurementID = Column(Integer, ForeignKey("MeasurementHistory.id"))


class ADConfigurationOutputChannelTable(DBConnection.Base):
    __tablename__ = "ADConfigurationOutChannel"
    id = Column(Integer, primary_key=True)
    # configID = Column(Integer, ForeignKey("ADConfiguration.id"))
    channel = Column(Enum(configuration.ADOutputChannels))
    amplitude = Column(Float)
    offset = Column(Float)
    enabled = Column(Boolean)
    measurementID = Column(Integer, ForeignKey("MeasurementHistory.id"))
