# 3rd Party
from sqlalchemy import (
    Column,
    Integer,
    DateTime as SQLDateTime,
    Unicode,
    ForeignKey,
    Float,
    Enum,
    Boolean,
)

# Custom
from WaveGen.DB.DBConnection import DBConnection
from WaveGen import configuration


class WaveConfigurationTable(DBConnection.Base):
    __tablename__ = "WaveConfiguration"

    id = Column(Integer, primary_key=True)
    name = Column(Unicode)
    userChangedName = Column(Boolean)
    creationDate = Column(SQLDateTime)
    frequency = Column(Float)
    amplitude = Column(Integer)
    phase = Column(Float)
    waveType = Column(Enum(configuration.WaveTypes))
    offset = Column(Integer)
    channel = Column(Enum(configuration.ADOutputChannels))
    dutyCycle = Column(Float)
    order = Column(Integer, nullable=True)
    enabled = Column(Boolean)
    measurementID = Column(Integer, ForeignKey("MeasurementHistory.id"), nullable=True)

    def __repr__(self):
        return f"""<User(id='{self.id}',
name='{self.name}',
creationDate='{self.creationDate}',
frequency='{self.frequency}',
amplitude='{self.amplitude}',
phase='{self.phase}',
waveType='{self.waveType}',
offset='{self.offset}',
channel='{self.channel}',
dutyCycle='{self.dutyCycle}',
order='{self.order}',
enabled='{self.enabled}',
measurementID='{self.measurementID}')>"""
