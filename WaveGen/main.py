# Native imports
import os
import logging
import tempfile

# 3rd Party
import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
from PyQt5 import Qt

# Custom
from WaveGen.GUI import MainWindowController
from WaveGen.DB.DBConnection import DBConnection
from WaveGen.Helpers.SBThread import SBThread


# Fix modern windows potatoes
if hasattr(Qt, "AA_EnableHighDpiScaling"):
    QtW.QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)

if hasattr(Qt, "AA_UseHighDpiPixmaps"):
    QtW.QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)


class Main:
    def __init__(self):
        # Start a new app
        app = QtW.QApplication([])

        # Init the custom thread magic
        SBThread()

        # Update the style
        app.setStyle("Fusion")
        palette = Qt.QPalette()
        palette.setColor(Qt.QPalette.Window, Qt.QColor(53, 53, 53))
        palette.setColor(Qt.QPalette.WindowText, QtC.Qt.white)
        palette.setColor(Qt.QPalette.Base, Qt.QColor(15, 15, 15))
        palette.setColor(Qt.QPalette.AlternateBase, Qt.QColor(53, 53, 53))
        palette.setColor(Qt.QPalette.ToolTipBase, QtC.Qt.white)
        palette.setColor(Qt.QPalette.ToolTipText, QtC.Qt.white)
        palette.setColor(Qt.QPalette.Text, QtC.Qt.white)
        palette.setColor(Qt.QPalette.Button, Qt.QColor(53, 53, 53))
        palette.setColor(Qt.QPalette.ButtonText, QtC.Qt.white)
        palette.setColor(Qt.QPalette.BrightText, QtC.Qt.red)
        palette.setColor(Qt.QPalette.Highlight, QtC.Qt.lightGray)
        palette.setColor(Qt.QPalette.HighlightedText, QtC.Qt.black)

        palette.setColor(Qt.QPalette.Disabled, Qt.QPalette.Button, QtC.Qt.lightGray)
        palette.setColor(Qt.QPalette.Disabled, Qt.QPalette.ButtonText, QtC.Qt.lightGray)
        palette.setColor(Qt.QPalette.Disabled, Qt.QPalette.Text, QtC.Qt.lightGray)

        app.setPalette(palette)

        tempfile.TemporaryFile()

        # Get the main controller & launch the show
        mwc = MainWindowController()
        mwc.showMainWindow()

        # Exec
        app.exec_()


if __name__ == "__main__":
    # setup logging
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )

    logger = logging.getLogger("WaveGenLogger")
    logger.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    logging.basicConfig(level=logging.INFO)

    # Run
    main = Main()
