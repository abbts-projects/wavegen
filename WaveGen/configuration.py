# Standard imports
from typing import Optional
from enum import Enum, auto
from AMASignal import WaveForms

# The current app version
version = "1.0.0"

# Waveform instance
waveforms: WaveForms = None

# Project path
projectPath: Optional[str] = None

# Possible waveforms to generate
class WaveTypes(Enum):
    Sinus = auto()
    Cosine = auto()
    Triangle = auto()
    Sawtooth = auto()
    Square = auto()
    PWM = auto()
    DC = auto()


class ADOutputChannels(Enum):
    W1 = 0
    W2 = 1


class ADInputChannels(Enum):
    CH1 = 0
    CH2 = 1


class ChannelPlotColours(Enum):
    CH1 = "#00a518"
    CH2 = "#ff4500"


class RecordingMethods(Enum):
    SingleShot = auto()
    Continuous = auto()


class Windows(Enum):
    none = auto()
    hanning = auto()
    hamming = auto()
    blackman = auto()
    bartlett = auto()
