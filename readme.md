# WaveGen

![WaveGen Window](./img/window2.png)

## Features
- Save / load measurements via SQLite (Detects empty projects)
- Export as png, jpg, pdf, csv
- Sinus, Cosine, Triangle, Sawtooth, PWM signals
- Measurement history
- Two programmable channels for the Analog Discovery
- Fully modular windows
- Measure on both Oscilloscope channels of the Analog discovery
- FFT
- Phase shift calculation
- Warning if connection to AD can't be established
- Displays pyqtgraph plots in the UI (speed) and saved Matplotlib plots for reports
- Keeps window layout of project
- AD USB/Aux Voltage & current, internal temperature display
- Accelerated spin boxes
- Custom base-n-spinbox
- Zip compression on measured results (2 Measurements = 381kB instead of 2.7MB!)
- Signal input triggering
- Signal output sync
- Reset plot when starting measurement
- Change rec button to stop/start based on state
- X-Y view
- Live data recording


## TODO
### Important
- Export to word / latex / pdf / excel

### Optional
- Measured plots have no labels :(

### Very optional
- Settings window
- FFT: Window applied => lower amplitude?
- Handle unplugging a device
- When re-measuring a historic item, some values may be recorded differently to what is set
